﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antick.SignalsProvider.AutoTester.Mvp.Presenters;
using Castle.Windsor;

namespace Antick.SignalsProvider.AutoTester
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var windsorContainer = new WindsorContainer();
            windsorContainer.Install(new WindsorInstaller());

            windsorContainer.Resolve<ApplicationPresenter>().Run();
        }
    }
}
