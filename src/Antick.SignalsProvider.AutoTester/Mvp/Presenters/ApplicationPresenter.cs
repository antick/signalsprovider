﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Antick.Infractructure.Components.Mvp.Presenters;
using Antick.SignalsProvider.AutoTester.Mvp.Models;
using Antick.SignalsProvider.AutoTester.Mvp.Views;
using Castle.Core.Logging;
using Castle.MicroKernel;

namespace Antick.SignalsProvider.AutoTester.Mvp.Presenters
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ApplicationPresenter : Presenter<IApplicationView,ApplicationModel>
    {
        private readonly ILogger m_Logger;

        public ApplicationPresenter(IKernel kernel, IApplicationView view, ApplicationModel model, ILogger logger) : base(kernel, view, model)
        {
            m_Logger = logger;


            View.ViewStarted += View_ViewStarted;
            View.Starting += View_Starting;
        }

        private void View_Starting(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                var tickets = Model.Start();
                View.RenderResult(tickets);
            });
        }

        private void View_ViewStarted(object sender, EventArgs e)
        {
            //var tickets =  Model.Start();
            //View.RenderResult(tickets);
        }

        public override void Run()
        {
            View.Show();
        }
    }
}
