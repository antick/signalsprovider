﻿using System;
using System.Collections.Generic;
using Antick.Infractructure.Components.Mvp.Views;
using Antick.SignalsProvider.AutoTester.Domian;

namespace Antick.SignalsProvider.AutoTester.Mvp.Views
{
    public interface IApplicationView : IView
    {
        /// <summary>
        /// Генерируется после первой отрисовки приложения
        /// </summary>
        event EventHandler<EventArgs> ViewStarted;

        event EventHandler<EventArgs> Starting;

        void RenderResult(ApplicationResult appResult);
    }
}
