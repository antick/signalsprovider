﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Antick.Infractructure.Extensions;
using Antick.SignalsProvider.AutoTester.Domian;

namespace Antick.SignalsProvider.AutoTester.Mvp.Views
{
    public partial class ApplicationView : Form, IApplicationView
    {
        private readonly ApplicationContext m_Context;
        public event EventHandler<EventArgs> ViewStarted;
        public event EventHandler<EventArgs> Starting;


        public ApplicationView(ApplicationContext context)
        {
            m_Context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            m_Context.MainForm = this;
            Application.Run(m_Context);
        }

        private void ApplicationView_Load(object sender, EventArgs e)
        {
            ViewStarted.Raise(this, new EventArgs());
        }

        public void RenderResult(ApplicationResult appResult)
        {
            this.InvokeAsync(c =>
                {
                    foreach (var series in chart1.Series)
                    {
                        series.Points.Clear();
                    }

                    double totalPoints = 0;

                    for (int i = 0; i < appResult.Tickets.Count; i++)
                    {
                        totalPoints += appResult.Tickets[i].Points;
                        chart1.Series[0].Points.Add(new DataPoint(i, totalPoints));
                    }

                    labelPfValue.Text = appResult.Pf.ToString("0.00");
                }
            );
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Starting.Raise(this, new EventArgs());
        }
    }
}
