﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Apps.SingalsProvider;
using Antick.Contracts.Domain;
using Antick.Contracts.Msgs.SignalsProvider;
using Antick.Infractructure.Components.Mvp.Models;
using Antick.Infractructure.Extensions;
using Antick.RateManager.Client;
using Antick.SignalsProvider.AutoTester.Domian;
using Antick.SignalsProvider.Components;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers;
using Antick.SignalsProvider.Providers.MaProvider;
using Antick.SignalsProvider.Providers.TestsProviders;
using Newtonsoft.Json;

namespace Antick.SignalsProvider.AutoTester.Mvp.Models
{
    public class ApplicationModel : Model
    {
        private readonly IRateManagerClient m_RateManagerClient;
        private readonly ISigalPublisherFactory m_PublisherFactory;
        private readonly ISignalProviderFactory m_SignalProviderFactory;

        private DateTime m_StartTime;
        private DateTime m_EndTime;
        
        private readonly List<SignaledTicket> m_Tickets;
        private readonly List<SignaledTicket> m_ClosedTickets;

        public ApplicationModel(IRateManagerClient rateManagerClient, ISigalPublisherFactory publisherFactory, ISignalProviderFactory signalProviderFactory)
        {
            m_RateManagerClient = rateManagerClient;
            m_PublisherFactory = publisherFactory;
            m_SignalProviderFactory = signalProviderFactory;
            m_StartTime = DateTime.Now.AddDays(-365);
            m_EndTime = DateTime.Now;
            m_Tickets = new List<SignaledTicket>();
            m_ClosedTickets = new List<SignaledTicket>();
        }

        public ApplicationResult Start()
        {
            m_Tickets.Clear();
            m_ClosedTickets.Clear();

            var tf = new TimeFrameComplex {Type = TimeFrameType.Renko, Value = "5"};

            var signalPublisher = m_PublisherFactory.Create(signalCallback);

            var parameters =
                new MaProviderParameters {MaFastPeriod = 13, MaSlowPeriod = 208, MaVerySlowPeriod = 838, Tf = tf};

            var model = new SignalProviderModel(0, "Test", "Ma",
                new InstrumentComplex {Name = "EUR_USD"}, JsonConvert.SerializeObject(parameters), true);

            var signalProvider = m_SignalProviderFactory.Create(model, signalPublisher);

            var rates = m_RateManagerClient.GetRates("EUR_USD", tf.Type, tf.Value, m_StartTime, m_EndTime)
                .GetSynchronousResult();


            for (int i = 0; i < rates.Count; i++)
            {
                var rate = rates[i];

                signalProvider.Consume(new List<Rate> { rate });

                Trade(rate);
            }

            if (m_Tickets.Any())
            {
                var lastRate = rates.Last();
                Trade(lastRate,true);
            }

            var pf = m_ClosedTickets.Where(p => p.Points > 0).Sum(p => p.Points) /
                     m_ClosedTickets.Where(p => p.Points <= 0).Sum(p => p.Points);

            pf = Math.Abs(pf);

            return new ApplicationResult
            {
                Tickets = m_ClosedTickets,
                Pf = pf
            };
        }


        private void Trade(Rate rate, bool hardClose = false)
        {
            List<int> idsClosed = new List<int>();

            for (var i = 0; i < m_Tickets.Count; i++)
            {
                var ticket = m_Tickets[i];

                if (rate.Index <= ticket.OpenedIndex)
                    continue;

                switch (ticket.Direction)
                {
                    case SignalDirection.Buy:
                        if (rate.Close <= ticket.Stoploss || rate.Close >= ticket.TakeProfit || hardClose)
                        {
                            ticket.Close(rate.Close);
                            idsClosed.Add(i);
                        }
                        break;

                    case SignalDirection.Sell:
                        if (rate.Close >= ticket.Stoploss || rate.Close <= ticket.TakeProfit || hardClose)
                        {
                            ticket.Close(rate.Close);
                            idsClosed.Add(i);
                        }
                        break;
                }
            }

            if (idsClosed.Any())
            {
                foreach (var i in idsClosed)
                {
                    var ticket = m_Tickets[i];
                    m_ClosedTickets.Add(ticket);
                    m_Tickets.Remove(ticket);
                }
            }
        }

        private void signalCallback(SignalCreated signal)
        {

            if (!m_Tickets.Any())
            {
                var ticket = new SignaledTicket(signal.Instrument, signal.LastPrice, signal.LastRateIndex,
                    signal.Direction, signal.TakeProfit, signal.StopLoss);
                m_Tickets.Add(ticket);
            }
        }
    }
}

