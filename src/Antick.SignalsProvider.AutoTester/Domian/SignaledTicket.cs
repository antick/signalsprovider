﻿using System;
using Antick.Contracts.Apps.SingalsProvider;
using Antick.Contracts.Domain;
using Antick.Contracts.Specs;

namespace Antick.SignalsProvider.AutoTester.Domian
{
    public class SignaledTicket
    {
        public Instrument Instrument { get; private set; }

        /// <summary>
        /// Цена открытия
        /// </summary>
        public double OpenedPrice { get; private set; }

        /// <summary>
        /// Индекс бара, на котором было открытие
        /// </summary>
        public long OpenedIndex { get; private set; }


        public SignalDirection Direction { get; private set; }

        /// <summary>
        /// Тейкпрофит
        /// </summary>
        public double TakeProfit { get; private set; }

        /// <summary>
        /// СтопЛосс
        /// </summary>
        public double Stoploss { get; private set; }

        /// <summary>
        /// Открыта или закрыта сделка
        /// </summary>
        public bool Active { get; private set; }

        /// <summary>
        /// Результат сделки в пунктах
        /// </summary>
        public double Points { get; private set; }

        public SignaledTicket(Instrument instrument, double openedPrice, long openedIndex, SignalDirection direction,
            double takeProfit, double stoploss)
        {
            Instrument = instrument;
            OpenedPrice = openedPrice;
            OpenedIndex = openedIndex;
            Direction = direction;
            TakeProfit = takeProfit;
            Stoploss = stoploss;
            Active = true;
        }

        /// <summary>
        /// Закрытие сделки по указанной цене - возвращает профит в пунктах
        /// </summary>
        public double Close(double closePrice)
        {
            double priceDiff = 0;

            switch (Direction)
            {
                case SignalDirection.Buy:
                    priceDiff = closePrice - OpenedPrice;
                    break;

                case SignalDirection.Sell:
                    priceDiff = OpenedPrice - closePrice;

                    break;
            }

            var spec = InstrumentSpec.Get(Instrument);

            Points = Math.Round(priceDiff * Math.Pow(10, spec.Digits), 1);

            Active = false;

            return Points;
        }
    }
}
