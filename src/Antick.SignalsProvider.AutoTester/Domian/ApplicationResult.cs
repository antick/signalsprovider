﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.AutoTester.Domian
{
    public class ApplicationResult
    {
        public List<SignaledTicket> Tickets { get; set; }

        public double Pf { get; set; }
    }
}
