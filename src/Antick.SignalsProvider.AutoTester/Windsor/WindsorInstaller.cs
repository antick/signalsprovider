﻿using System.Windows.Forms;
using Antick.Cqrs;
using Antick.RateManager.Client;
using Antick.SignalsProvider.AutoTester.Mvp.Models;
using Antick.SignalsProvider.AutoTester.Mvp.Presenters;
using Antick.SignalsProvider.AutoTester.Mvp.Views;
using Antick.SignalsProvider.Components;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Providers;
using Antick.SignalsProvider.Providers.MaProvider;
using Antick.SignalsProvider.Providers.TestsProviders;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Windsor;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Antick.SignalsProvider.AutoTester
{
    public class WindsorInstaller  : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<LoggingFacility>();
            container.AddFacility<TypedFactoryFacility>();

            // View MPV
            container.Register(Component.For<ApplicationContext>()); // базовый компонент WindowForms
            container.Register(Component.For<IApplicationView>().ImplementedBy<ApplicationView>());

            // Presendter MVP
            container.Register(Component.For<ApplicationPresenter>());

            // Model MVP
            container.Register(Component.For<ApplicationModel>().ImplementedBy<ApplicationModel>());

            container.Register(Component.For<IRateManagerClient>()
                .UsingFactoryMethod((kernel, context) => new RateManagerClient("http://localhost:2002")));

            container.Install(new CqrsInstaller(Types.FromAssemblyInThisApplication()));

            container.Register(Component.For<IWaveFileStorage>().ImplementedBy<StubWaveFileStorage>());
            container.Register(Component.For<ISignalProviderCache>().ImplementedBy<SignalProviderCache>());

            container.Register(Component.For<ISignalProvider>().ImplementedBy<WaveV1Provider>()
                .Named(SignalProviders.WaveV1).LifestyleTransient());
            container.Register(Component.For<ISignalProvider>().ImplementedBy<StupidProvider>()
                .Named(SignalProviders.TestStupid).LifestyleTransient());
            container.Register(Component.For<ISignalProvider>().ImplementedBy<MaProvider>()
                .Named(SignalProviders.Ma).LifestyleTransient());

            container.Register(Component.For<ISignalProviderFactory>().ImplementedBy<SignalProviderFactory>());
            container.Register(Component.For<ISignalProviderAutomateFactory>().AsFactory(c => c.SelectedWith(new NamedTypedFactoryComponentSelector())));

            container.Register(Component.For<ISignalPublisher>().ImplementedBy<TesterSignalPublisher>());
            container.Register(Component.For<ISigalPublisherFactory>().AsFactory());
        }
    }
}
