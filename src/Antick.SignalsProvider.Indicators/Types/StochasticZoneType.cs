﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public enum StochasticZoneType
    {
        None = 0,
        U80ToL50 = 1,
        L50ToL20 = 2,
        L20ToU50 = 3,
        U50ToU80 = 4,
        L50ToU50 = 5,
        U50ToL50 = 6,
        UnknowDown = 7,
        UnknowUp = 8
    }
}
