﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public static class StochasticZoneTypeExtensions
    {
        /// <summary>
        /// Определяет направление по значение зоны переходов стохастика.
        /// </summary>
        /// <returns></returns>
        public static StochasticZoneTrend Direction(this StochasticZoneType stochasticZone)
        {
            switch (stochasticZone)
            {
                case StochasticZoneType.U50ToU80:
                case StochasticZoneType.UnknowUp:
                case StochasticZoneType.L50ToU50:
                case StochasticZoneType.L20ToU50:
                    return StochasticZoneTrend.Up;
                case StochasticZoneType.U80ToL50:
                case StochasticZoneType.L50ToL20:
                case StochasticZoneType.UnknowDown:
                case StochasticZoneType.U50ToL50:
                    return StochasticZoneTrend.Down;
            }

            return StochasticZoneTrend.None;
        }
    }
}
