﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class StochasticZoneDynamicValues
    {
        public long LastIndex { get; set; }
        public StochasticZoneType LastType { get; set; }
    }
}
