﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.Indicators.Types
{
    public enum StochasticZoneMode
    {
        /// <summary>
        /// Переходы в зону 80 и 20
        /// </summary>
        Default,
        // То что по дефолту + переходы в зону 50 
        With50
    }
}
