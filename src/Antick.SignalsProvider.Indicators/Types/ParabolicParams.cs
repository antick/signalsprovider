﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class ParabolicParams
    {
        public double Af { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public bool Flag { get; set; }
    }
}
