﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public enum StochasticZoneTrend
    {
        None,
        Up,
        Down
    }
}
