﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.Indicators.Types
{
    public enum StochasticZoneTypeV2
    {
        None,
        Zone80,
        Zone20
    }
}
