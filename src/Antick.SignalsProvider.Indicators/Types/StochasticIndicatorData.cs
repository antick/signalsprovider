﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class StochasticIndicatorData
    {
        public double Value { get; set; }
        public double Signal { get; set; }
     }
}
