﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class StochasticZoneContext
    {
        public List<Rate> Candles { get; set; }

        public IndexedList<StochasticIndicatorData> StochasticData { get; set; }

        public StochasticZoneMode? Mode { get; set; }
    }
}
