﻿using System.Collections.Generic;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class FractIndicatorResult
    {
        public List<bool> Upper { get; set; }
        public List<bool> Lower { get; set; }
    }
}
