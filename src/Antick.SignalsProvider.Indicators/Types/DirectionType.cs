using System.ComponentModel;

namespace Antick.SignalsProvider.Indicators.Types
{
    /// <summary>
    /// ��� ������, ����������� ��������/����, ��� �����������/��� ����� 
    /// </summary>
    public enum DirectionType : long 
    {
        /// <summary>
        /// ���������� �����, ��������/����� �����
        /// </summary>
        [Description("�����")]
        Up = 1,

        /// <summary>
        /// ���������� �����, ��������/����� ����
        /// </summary>
        [Description("����")]
        Down = 2,

        /// <summary>
        /// ��� ����������
        /// </summary>
        [Description("����������")]
        None = 3,
    }

    public static class DirectionTypeExt
    {
        public static DirectionType Reverse(this DirectionType direction)
        {
            if (direction == DirectionType.Up) return DirectionType.Down;
            if (direction == DirectionType.Down) return DirectionType.Up;

            return DirectionType.None;
        }
    }
}