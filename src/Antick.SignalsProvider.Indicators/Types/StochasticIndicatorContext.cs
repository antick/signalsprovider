﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class StochasticIndicatorContext
    {
        /// <summary>
        /// Исходные данные
        /// </summary>
        public List<Rate> Rates { get; set; }

        /// <summary>
        /// Период %K (Pk). Это число единичных периодов, используемых для расчета %K. По умолчанию равен 20 (В МТ4 дефолт это 5)
        /// </summary>
        public int Pk = 20;

        /// <summary>
        /// Период замедления %K (Sk). Эта величина определяет степень внутренней сглаженности линии %K.
        /// Значение 1 дает быстрый стохастический осциллятор, а значение 3 - медленный. По умолчанию равен 3
        /// </summary>
        public int Sk = 3;

        /// <summary>
        /// Период %D (Pd). Это число единичных периодов, используемых для расчета скользящего среднего линии %K. По умолчанию равен 2(в МТ4 дефолт это 3)
        /// </summary>
        public int Pd = 2;
    }
}
