﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class StochasticZone
    {
        /// <summary>
        /// Индекс, где зона найдена(и Value и Signal линия
        /// </summary>
        public long Index = -1;

        /// <summary>
        /// Индекс, где найден зона только по Value линии
        /// </summary>
        public long ValueIndex = -1;

        /// <summary>
        /// Индекс, где найдена зона только по Signal линии
        /// </summary>
        public long SignalIndex = -1;

        /// <summary>
        /// Тип перехода
        /// </summary>
        public StochasticZoneType Type = StochasticZoneType.None;

        /// <summary>
        /// Найден ли переход в Значении и Сигнале
        /// </summary>
        public bool Declared()
        {
            return ValueIndex != -1 && SignalIndex != -1;
        }

        public void Clear()
        {
            Index = -1;
            ValueIndex = -1;
            SignalIndex = -1;
            Type = StochasticZoneType.None;
        }

        public StochasticZone Clone()
        {
            return new StochasticZone
            {
                Index = Index,
                ValueIndex = ValueIndex,
                SignalIndex = SignalIndex,
                Type = Type
            };
        }

        public void Set(StochasticZoneType newZone, long index)
        {
            Type = newZone;
            Index = index;
        }

        /// <summary>
        /// Установка неизвестного типа 
        /// </summary>
        /// <param name="direction">Предыдущее направление</param>
        /// <param name="index"></param>
        public void SetUnknow(StochasticZoneTrend direction, long index)
        {
            switch (direction)
            {
                case StochasticZoneTrend.Up:
                    Type = StochasticZoneType.UnknowDown;
                    break;
                case StochasticZoneTrend.Down:
                    Type = StochasticZoneType.UnknowUp;
                    break;
            }
            Index = index;
        }
    }
}
