﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class IndexValue<T>
    {
        public long Index { get; set; }
        public T Value { get; set; }
    }
}
