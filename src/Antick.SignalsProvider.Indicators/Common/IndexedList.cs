﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class IndexedList<T> 
    {
        public int Count => m_List.Count;

        private readonly List<IndexValue<T>> m_List;

        public IndexedList()
        {
            m_List = new List<IndexValue<T>>();
        }

        public IndexedList(List<IndexValue<T>> list)
        {
            m_List = list;
        }

        public IndexedList(IndexValue<T> item)
        {
            m_List = new List<IndexValue<T>> {item};
        }
        
        public IndexValue<T> this[int i]
        {
            get => m_List[i];
            set
            {
                if (i < m_List.Count)
                    m_List[i] = value;
                else if (i == m_List.Count)
                {
                    m_List.Add(value);
                }
                else throw new NotSupportedException("Out range indexer");
            }
        }

        public List<IndexValue<T>> ToList()
        {
            return m_List;
        }

        public IndexValue<T> FirstOrDefault(Func<IndexValue<T>, bool> predicate)
        {
            return m_List.FirstOrDefault(predicate);

        }

        public IndexValue<T> First(Func<IndexValue<T>, bool> predicate)
        {
            return m_List.First(predicate);
        }

        public void Add(IndexValue<T> item)
        {
            m_List.Add(item);
        }

        public void AddRange(IndexedList<T> items)
        {
            m_List.AddRange(items.ToList());
        }

        public IEnumerable<IndexValue<T>> Skip(int count)
        {
            return m_List.Skip(count);
        }

        public IEnumerable<IndexValue<T>> Where(Func<IndexValue<T>, bool> predicate)
        {
            return m_List.Where(predicate);
        }

        public bool Any()
        {
            return m_List.Any();
        }

        public IndexValue<T> Last()
        {
            return m_List.Last();
        }

        public IndexValue<T> Prev()
        {
            return m_List.First(p => p.Index == m_List.Last().Index - 1);
        }
    }

    public static class IEnumerableIndexValue
    {
        public static IndexedList<T> ToIndexList<T>(this IEnumerable<IndexValue<T>> enumerable)
        {
            return new IndexedList<T>(enumerable.ToList());
        }
    }
}
