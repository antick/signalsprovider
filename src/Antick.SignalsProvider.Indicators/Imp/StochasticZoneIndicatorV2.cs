﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class StochasticZoneIndicatorV2
    {
        private readonly StochasticZoneContext m_Context;

        private StochasticZoneMode m_Mode;

        /// <summary>
        /// Важные переменные для всего цикла
        /// </summary>
        private StochasticZone _lastZone;
        private StochasticZone _currentZone = new StochasticZone();

        public long LastIndex { get; private set; }

        public StochasticZoneIndicatorV2(StochasticZoneContext context, StochasticZoneDynamicValues @params = null)
        {
            m_Context = context;
            _lastZone = @params != null
                ? new StochasticZone { Type = @params.LastType, Index = @params.LastIndex }
                : new StochasticZone();

            m_Mode = context.Mode ?? StochasticZoneMode.Default;
        }

        public StochasticZoneDynamicValues GetDynamicValues()
        {
            return new StochasticZoneDynamicValues { LastIndex = _lastZone.Index, LastType = _lastZone.Type };
        }

        /// <summary>
        /// Пересчет на указанном баре
        /// </summary>
        public StochasticZoneType CalculateLogic(int i)
        {
            var candles = m_Context.Candles;
            var stochasticData = m_Context.StochasticData;
            var index = candles[i].Index;

            Rate lastBar = _lastZone.Index == -1 ? candles.First() : candles.First(p => p.Index == _lastZone.Index);

            var nextStockValues = stochasticData.Where(p => p.Index >= _lastZone.Index + 1 && p.Index <= index).ToList();
            var curStockValue = stochasticData.First(p => p.Index == lastBar.Index);

            bool? trendDownStochValue = nextStockValues.Any()
                ? nextStockValues.Min(p => p.Value.Value) < curStockValue.Value.Value
                : (bool?)null;

            bool? trendUpStochValue = nextStockValues.Any()
                ? nextStockValues.Max(p => p.Value.Value) > curStockValue.Value.Value
                : (bool?)null;

            bool? trendDownStochSignal = nextStockValues.Any()
                ? nextStockValues.Min(p => p.Value.Signal) < curStockValue.Value.Signal
                : (bool?)null;

            bool? trendUpStochSignal = nextStockValues.Any()
                ? nextStockValues.Max(p => p.Value.Signal) > curStockValue.Value.Signal
                : (bool?)null;

          
            

            if (trendDownStochValue.HasValue && trendDownStochValue.Value && StochVal(index) <= 20.0 &&
                (_lastZone.Index == -1 || getDirection(_lastZone.Type) == DirectionType.Up))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.ValueIndex = index;

                if (_currentZone.SignalIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.L50ToL20;
                }
            }

            if (trendDownStochSignal.HasValue && trendDownStochSignal.Value && StochSygn(index) <= 20.0 &&
                (_lastZone.Index == -1 || getDirection(_lastZone.Type) == DirectionType.Up))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.SignalIndex = index;

                if (_currentZone.ValueIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.L50ToL20;
                }
            }

            if (trendUpStochValue.HasValue && trendUpStochValue.Value && StochVal(index) >= 80.0 &&
                (_lastZone.Index == -1 || getDirection(_lastZone.Type) == DirectionType.Down))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.ValueIndex = index;

                if (_currentZone.SignalIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.U50ToU80;
                }
            }

            if (trendUpStochSignal.HasValue && trendUpStochSignal.Value && StochSygn(index) >= 80.0 &&
                (_lastZone.Index == -1 || getDirection(_lastZone.Type) == DirectionType.Down))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.SignalIndex = index;

                if (_currentZone.ValueIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.U50ToU80;
                }
            }

            var result = StochasticZoneType.None;

            if (_currentZone.Declared() && (_lastZone.Index == -1 || _currentZone.Type != StochasticZoneType.None))
            {
                _currentZone.Index = (_currentZone.ValueIndex >= _currentZone.SignalIndex)
                    ? _currentZone.ValueIndex
                    : _currentZone.SignalIndex;

                _lastZone = _currentZone.Clone();
                result = _lastZone.Type;

            }

            _currentZone.Clear();
            LastIndex = index;

            return result;
        }

        DirectionType getDirection(StochasticZoneType type)
        {
            if (type == StochasticZoneType.U50ToU80 || type == StochasticZoneType.UnknowUp)
                return DirectionType.Up;

            return DirectionType.Down;
        }

        double StochVal(long ind)
        {
            return m_Context.StochasticData.First(p => p.Index == ind).Value.Value;
        }

        double StochSygn(long ind)
        {
            return m_Context.StochasticData.First(p => p.Index == ind).Value.Signal;
        } 
    }
}
