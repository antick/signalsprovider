﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp.MovementImp
{
    public class MovementIndicatorUpdateEventArgsV2 : EventArgs
    {
        /// <summary>
        /// Индекс бара, на котором случилось событие
        /// </summary>
        public long Index { get; set; }

        public Movement Movement { get; set; }
    }
}
