﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementIndicatorStochasticPivotEventArgs : EventArgs
    {
        /// <summary>
        /// Индекс бара, который исопльзовался для построение движения
        /// </summary>
        public long Index { get; set; }

        public StochasticZoneType ZoneType { get; set; }
    }
}
