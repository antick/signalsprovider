﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementDynamicValuesV2
    {
        /// <summary>
        /// Слом движения через пробитие точки старта предыдущего
        /// </summary>
        public bool IsBroken { get; set; }

        public bool IsMaCross { get; set; }

        public double LastMovementStartPrice { get; set; }
        public long LastMovementIndex { get; set; }
        public long LastMovementEndBarIndex { get; set; }
        public double LastMovementEndPrice { get; set; }

        public double PrevMaValue { get; set; }
        public double LastMaValue { get; set; }

        public DirectionType LastMovementDirection()
        {
            if (LastMovementStartPrice == 0)
                return DirectionType.None;
            return LastMovementEndPrice > LastMovementStartPrice ? DirectionType.Up : DirectionType.Down;
        }
    }
}
