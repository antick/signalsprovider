﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class Movement
    {
        public long Id { get; set; }

        public double StartPrice { get; set; }
        public double EndPrice { get; set; }
        public bool IsCompleted { get; set; }
        public long Index { get; set; }

        public long StartBarIndex { get; set; }
        public long EndBarIndex { get; set; }

        /// <summary>
        /// Определяет направление движения
        /// </summary>
        public DirectionType Direction()
        {
            return StartPrice < EndPrice ? DirectionType.Up : DirectionType.Down;
        }
    }
}
