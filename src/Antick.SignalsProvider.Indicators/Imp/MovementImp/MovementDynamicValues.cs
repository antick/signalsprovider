﻿namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementDynamicValues
    {
        /// <summary>
        /// Последняя зафиксированная зона Стохастика(которая использовалась для создания нового движения), создается из _newPossibleZone
        /// </summary>
        public StochasticZone LastCheckedZone { get; set; }

        /// <summary>
        /// Новая возможная зона для стохастика(Профильтрованная уже), создается из _lastActiveZone
        /// </summary>
        public StochasticZone NewPossibleZone { get; set; }

        /// <summary>
        /// Просто последняя зона стохастика (Из индикатора)
        /// </summary>
        public StochasticZone LastActiveZone { get; set; }

        /// <summary>
        /// Достижение зоны 20/80 стохастиком
        /// </summary>
        public bool Stochasctic { get; set; }

        /// <summary>
        /// Слом движения через пробитие точки старта предыдущего
        /// </summary>
        public bool IsBroken { get; set; }

        /// <summary>
        /// Пересечение МА периода 13
        /// </summary>
        public bool Ma13Cross { get; set; }

        public double LastMovementStartPrice { get; set; }
        public long LastMovementIndex { get; set; }
        public long LastMovementEndBarIndex { get; set; }
        public double LastMovementEndPrice { get; set; }

        /// <summary>
        /// Кешированное последнее движение
        /// </summary>
        //public Movement LastMovement { get; set; }

        public MovementDynamicValues()
        {
            LastCheckedZone = new StochasticZone();
            NewPossibleZone = new StochasticZone();
            LastActiveZone = new StochasticZone();
        }
    }
}
