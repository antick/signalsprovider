﻿using System;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Infractructure.Extensions;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class MovementIndicator
    {
        public EventHandler<MovementIndicatorUpdateEventArgs> Updated;
        public EventHandler<MovementIndicatorAddedEventArgs> Added;
        public EventHandler<MovementIndicatorStochasticPivotEventArgs> StochasticPivot;
        
        private readonly MovementContext m_Context;

        private readonly MovementDynamicValues m_Values;

        public MovementIndicator(MovementContext context, MovementDynamicValues values = null)
        {
            m_Values = values ?? new MovementDynamicValues();
            m_Context = context;
        }

        public MovementDynamicValues GetDynamicValues()
        {
            return m_Values;
        }

        private void Clear()
        {
            m_Values.LastCheckedZone.Clear();
            m_Values.LastActiveZone.Clear();
            m_Values.NewPossibleZone.Clear();
            m_Values.Stochasctic = false;
            m_Values.IsBroken = false;
            m_Values.Ma13Cross = false;
        }

        private void clearTmpVariable()
        {
            m_Values.NewPossibleZone.Clear();
            m_Values.Stochasctic = false;
            m_Values.IsBroken = false;
            m_Values.Ma13Cross = false;
        }

        /// <summary>
        /// Проверка на пробитие МА208
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool isMa13Cross(long index)
        {
            var candle = m_Context.Candles.FirstOrDefault(p => p.Index == index);
            var ma13 = m_Context.MaValues.FirstOrDefault(p => p.Index == index).Value;

            switch (m_Values.LastCheckedZone.Type)
            {
                case StochasticZoneType.U50ToU80:
                case StochasticZoneType.L20ToU50:
                case StochasticZoneType.UnknowUp:
                case StochasticZoneType.L50ToU50:
                    if (candle.Close < ma13)
                        return true;
                    break;

                case StochasticZoneType.U80ToL50:
                case StochasticZoneType.L50ToL20:
                case StochasticZoneType.UnknowDown:
                case StochasticZoneType.U50ToL50:
                    if (candle.Close > ma13)
                        return true;
                    break;
            }
            return false;
        }

        /// <summary>
        /// Обновление конца текущего движения
        /// </summary>
        /// <param name="index"></param>
        private void updateEndMovement(long index)
        {
            bool condition;
            var bar = m_Context.Candles.FirstOrDefault(p => p.Index == index);

            switch (m_Values.LastCheckedZone.Type)
            {
                case StochasticZoneType.U50ToU80:
                case StochasticZoneType.UnknowUp:
                case StochasticZoneType.L20ToU50:
                    condition = bar.High > m_Values.LastMovementEndPrice;

                    if (condition)
                    {
                        var value = bar.High;

                        m_Values.LastMovementEndBarIndex = index;
                        m_Values.LastMovementEndPrice = value;

                        clearTmpVariable();

                        Updated.Raise(this, new MovementIndicatorUpdateEventArgs
                        {
                            Index = index,
                            MovementEndBarIndex = m_Values.LastMovementEndBarIndex,
                            MovementEndPrice = m_Values.LastMovementEndPrice,
                            MovementIndex = m_Values.LastMovementIndex
                        });
                    }
                    break;
                case StochasticZoneType.U80ToL50:
                case StochasticZoneType.L50ToL20:
                case StochasticZoneType.UnknowDown:
                case StochasticZoneType.U50ToL50:
                    condition = bar.Low < m_Values.LastMovementEndPrice;
                    if (condition)
                    {
                        var value = bar.Low;

                        m_Values.LastMovementEndBarIndex = index;
                        m_Values.LastMovementEndPrice = value;
                        clearTmpVariable();
                        Updated.Raise(this, new MovementIndicatorUpdateEventArgs
                        {
                            Index = index,
                            MovementEndBarIndex = m_Values.LastMovementEndBarIndex,
                            MovementEndPrice = m_Values.LastMovementEndPrice,
                            MovementIndex = m_Values.LastMovementIndex
                        });
                    }
                    break;
            }
        }

        private void tryToCompleteMovementLine(long index)
        {
            double endPrice = 0;
            var direction = m_Values.LastCheckedZone.Type.Direction();
            var commonCondition = m_Values.Stochasctic && m_Values.Ma13Cross;

            if (commonCondition || m_Values.IsBroken)
            {
                long endIndex = index;

                switch (direction)
                {
                    case StochasticZoneTrend.Up:
                        endPrice = m_Context.Candles.First(p => p.Index == index).Low;
                        break;

                    case StochasticZoneTrend.Down:
                        endPrice = m_Context.Candles.First(p => p.Index == index).High;
                        break;
                }

                var movement = new Movement
                {
                    StartBarIndex = m_Values.LastMovementEndBarIndex,
                    StartPrice = m_Values.LastMovementEndPrice,
                    EndBarIndex = endIndex,
                    EndPrice = endPrice,
                    Index = m_Values.LastMovementIndex + 1
                };
                
                m_Values.LastMovementIndex = movement.Index;
                m_Values.LastMovementEndBarIndex = movement.EndBarIndex;
                m_Values.LastMovementEndPrice = movement.EndPrice;
                m_Values.LastMovementStartPrice = movement.StartPrice;

                Added.Raise(this, new MovementIndicatorAddedEventArgs { Index = index, Movement = movement});

                if (commonCondition)
                {
                    m_Values.LastCheckedZone.Set(m_Values.NewPossibleZone.Type, index);
                   
                }
                else
                {
                    m_Values.LastCheckedZone.SetUnknow(direction, index);
                }

                StochasticPivot.Raise(this,
                    new MovementIndicatorStochasticPivotEventArgs
                    {
                        Index = index,
                        ZoneType = m_Values.LastCheckedZone.Type
                    });

                clearTmpVariable();
            }

        }

        private Rate barByIndex(long index)
        {
            return m_Context.Candles.First(p => p.Index == index);
        }

        private void isBroken(long index)
        {
            var bar = barByIndex(index);

            switch (m_Values.LastCheckedZone.Type)
            {
                case StochasticZoneType.U50ToU80:
                case StochasticZoneType.UnknowUp:
                case StochasticZoneType.L20ToU50:
                case StochasticZoneType.L50ToU50:
                    m_Values.IsBroken = bar.Low < m_Values.LastMovementStartPrice;
                    break;

                case StochasticZoneType.U80ToL50:
                case StochasticZoneType.L50ToL20:
                case StochasticZoneType.UnknowDown:
                case StochasticZoneType.U50ToL50:
                    m_Values.IsBroken = bar.High > m_Values.LastMovementStartPrice;
                    break;
            }
        }

        private StochasticZoneType stochasticZone(long index)
        {
            var val = m_Context.StochZones.First(p => p.Index == index).Value;
            return val;
        }

        private void analizeStoch(long index)
        {
            if (stochasticZone(index) != StochasticZoneType.None)
            {
                var stochPivot = stochasticZone(index);

                m_Values.LastActiveZone.Set(stochPivot, index);

                switch (m_Values.LastCheckedZone.Type)
                {
                    case StochasticZoneType.U50ToU80:
                        switch (stochPivot)
                        {
                            case StochasticZoneType.U80ToL50:
                            case StochasticZoneType.U50ToL50:
                                //_stochasctic50 = true;
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                break;
                            case StochasticZoneType.L50ToL20:
                                m_Values.Stochasctic = true;
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                break;
                        }
                        break;
                    case StochasticZoneType.U80ToL50:
                        switch (stochPivot)
                        {
                            case StochasticZoneType.U50ToU80:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                m_Values.Stochasctic = true;
                                break;

                            // Вариант вниз(обновление по тренду)
                            case StochasticZoneType.L50ToL20:
                                // Но переход фиксируются
                                m_Values.LastCheckedZone.Set(stochPivot, index);
                                updateEndMovement(index);
                                clearTmpVariable();
                                return;
                        }
                        break;
                    case StochasticZoneType.L50ToL20:
                        switch (stochPivot)
                        {
                            // Вариант вверх
                            case StochasticZoneType.L20ToU50:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                //_stochasctic50 = true;
                                break;
                            case StochasticZoneType.U50ToU80:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                m_Values.Stochasctic = true;
                                break;
                        }
                        break;
                    case StochasticZoneType.L20ToU50:
                        switch (stochPivot)
                        {
                            case StochasticZoneType.U50ToU80:
                                // Обновление лоу только если ниже
                                m_Values.LastCheckedZone.Set(stochPivot, index);
                                updateEndMovement(index);
                                // Но переход фиксируются
                                clearTmpVariable();
                                return;
                            case StochasticZoneType.L50ToL20:
                                m_Values.Stochasctic = true;
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                break;
                            case StochasticZoneType.U50ToL50:
                                //_stochasctic50 = true;
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                break;
                        }
                        break;
                    case StochasticZoneType.UnknowUp:
                        switch (stochPivot)
                        {
                            // Вариант вверх
                            case StochasticZoneType.L20ToU50:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                //_stochasctic50 = true;
                                break;
                            case StochasticZoneType.U50ToU80:
                                m_Values.LastCheckedZone.Set(stochPivot, index);
                                break;
                            case StochasticZoneType.L50ToL20:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                m_Values.Stochasctic = true;
                                break;
                        }
                        break;
                    case StochasticZoneType.UnknowDown:
                        switch (stochPivot)
                        {
                            case StochasticZoneType.L50ToL20:
                                m_Values.LastCheckedZone.Set(stochPivot, index);
                                break;
                            case StochasticZoneType.L50ToU50:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                //_stochasctic50 = true;
                                break;
                            case StochasticZoneType.U50ToU80:
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                m_Values.Stochasctic = true;
                                break;
                            default:
                                //MessageBox.Show(StochasticPivotPointList[View[index].Time].ToString());
                                break;
                        }
                        break;
                    case StochasticZoneType.L50ToU50:
                        switch (stochPivot)
                        {
                            case StochasticZoneType.L50ToL20:
                                m_Values.Stochasctic = true;
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                break;
                            case StochasticZoneType.U50ToU80:
                                m_Values.LastCheckedZone.Set(stochPivot, index);
                                break;
                        }
                        break;
                    case StochasticZoneType.U50ToL50:
                        switch (stochPivot)
                        {
                            case StochasticZoneType.U50ToU80:
                                m_Values.Stochasctic = true;
                                m_Values.NewPossibleZone.Set(stochPivot, index);
                                break;
                            case StochasticZoneType.L50ToL20:
                                m_Values.LastCheckedZone.Set(stochPivot, index);
                                updateEndMovement(index);
                                clearTmpVariable();
                                return;
                        }
                        break;
                }
            }
            return;
        }

        /// <summary>
        /// Анализатор на прошлом баре(на том баре который 100% был уже заверщшен)
        /// </summary>
        /// <param name="index"></param>
        public void CalculateLogic(long index)
        {
            //Отдельно расматривает случай первого движения
            if (m_Values.LastCheckedZone.Type == StochasticZoneType.None)
            {
                // Изначально за нач. точки берутся точки по стохастику 80 или 20
                if (stochasticZone(index) != StochasticZoneType.None)
                {
                    Rate x1;
                    Rate x2;
                    double y1;
                    double y2;

                    var bars = m_Context.Candles.Where(p => p.Index <= index);

                    switch (stochasticZone(index))
                    {
                        case StochasticZoneType.L50ToL20:
                            y1 = bars.Max(p => p.High);
                            x1 = bars.First(p => p.High == y1);
                            x2 = barByIndex(index);
                            y2 = barByIndex(index).Low;
                            break;
                        case StochasticZoneType.U50ToU80:
                            y1 = bars.Min(p => p.Low);
                            x1 = bars.First(p => p.Low == y1);
                            x2 = barByIndex(index);
                            y2 = barByIndex(index).High;
                            break;

                        default:
                            return;
                    }

                    var movement = new Movement
                    {
                        StartBarIndex = x1.Index,
                        EndBarIndex = x2.Index,
                        StartPrice = y1,
                        EndPrice = y2,
                        Index = 0,
                    };
                    
                    m_Values.LastMovementIndex = movement.Index;
                    m_Values.LastMovementEndBarIndex = movement.EndBarIndex;
                    m_Values.LastMovementEndPrice = movement.EndPrice;
                    m_Values.LastMovementStartPrice = movement.StartPrice;

                    Added.Raise(this, new MovementIndicatorAddedEventArgs { Index = index, Movement = movement });

                    m_Values.LastCheckedZone.Set(stochasticZone(index), index);

                }
            }
            // Вариации последнего движения расматриваются подробно в каждом подметоде ниже
            else
            {
                // Слом вниз когда пробивает точку старта текущего движения
                isBroken(index);

                // Переход МА13 вниз/вверх
                if (!m_Values.Ma13Cross && isMa13Cross(index))
                {
                    m_Values.Ma13Cross = true;
                }

                // Анализ переходов по стохастику
                analizeStoch(index);

                // Проверка условия на начало нового движения в противоположную сторону.
                // Отрисовка при выполнении условий
                tryToCompleteMovementLine(index);

                // Обновление хая/лоу движения
                updateEndMovement(index);
            }
        }
    }
}
