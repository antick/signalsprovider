﻿using System;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Infractructure.Extensions;
using Antick.SignalsProvider.Indicators.Imp.MovementImp;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    /// <summary>
    /// Реализация MovementIndicator - без стохастика, движения это пересечения быстрой МА более медленной МА
    /// </summary>
    public class MovementIndicatorV2
    {
        public EventHandler<MovementIndicatorUpdateEventArgs> Updated;
        public EventHandler<MovementIndicatorAddedEventArgs> Added;
        public EventHandler<MovementIndicatorStochasticPivotEventArgs> StochasticPivot;

        private readonly MovementContextV2 m_Context;

        private readonly MovementDynamicValuesV2 m_Values;

        public MovementIndicatorV2(MovementContextV2 context, MovementDynamicValuesV2 values = null)
        {
            m_Values = values ?? new MovementDynamicValuesV2();
            m_Context = context;
        }

        public MovementDynamicValuesV2 GetDynamicValues()
        {
            return m_Values;
        }

        private void Clear()
        {
            m_Values.IsBroken = false;
            m_Values.IsMaCross = false;
        }

        private void clearTmpVariable()
        {
            m_Values.IsBroken = false;
            m_Values.IsMaCross = false;
        }


        /// <summary>
        /// Проверка на пробитие МА208
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool isMa13Cross(long index)
        {
            var candle = m_Context.Candles.FirstOrDefault(p => p.Index == index);
            var ma13 = m_Context.MaFastValues.FirstOrDefault(p => p.Index == index).Value;

            switch (m_Values.LastMovementDirection())
            {
                case DirectionType.Up:
                    if (candle.Close < ma13)
                        return true;
                    break;

                case DirectionType.Down:
                    if (candle.Close > ma13)
                        return true;
                    break;
            }
            return false;
        }


        /// <summary>
        /// Обновление конца текущего движения
        /// </summary>
        /// <param name="index"></param>
        private void updateEndMovement(long index)
        {
            bool condition;
            var bar = m_Context.Candles.FirstOrDefault(p => p.Index == index);

            if (m_Values.LastMovementDirection() == DirectionType.None)
                return;
            
            switch (m_Values.LastMovementDirection())
            {
                case DirectionType.Up:
                    condition = bar.High > m_Values.LastMovementEndPrice;

                    if (condition)
                    {
                        var value = bar.High;

                        m_Values.LastMovementEndBarIndex = index;
                        m_Values.LastMovementEndPrice = value;
                        clearTmpVariable();
                        Updated.Raise(this,
                            new MovementIndicatorUpdateEventArgs
                            {
                                Index = index,
                                MovementEndPrice = m_Values.LastMovementEndPrice,
                                MovementIndex = m_Values.LastMovementIndex,
                                MovementEndBarIndex = m_Values.LastMovementEndBarIndex
                            });
                    }

                    break;
                case DirectionType.Down:
                    condition = bar.Low < m_Values.LastMovementEndPrice;
                    if (condition)
                    {
                        var value = bar.Low;

                        m_Values.LastMovementEndBarIndex = index;
                        m_Values.LastMovementEndPrice = value;
                        clearTmpVariable();
                        Updated.Raise(this,
                            new MovementIndicatorUpdateEventArgs
                            {
                                Index = index,
                                MovementEndPrice = m_Values.LastMovementEndPrice,
                                MovementIndex = m_Values.LastMovementIndex,
                                MovementEndBarIndex = m_Values.LastMovementEndBarIndex
                            });
                    }
                    break;
            }
        }

        private void tryToCompleteMovementLine(long index)
        {
            double endPrice = 0;
            var direction = m_Values.LastMovementDirection();

            if (direction == DirectionType.None)
                return;

            var commonCondition = false;
            var movementCrossMa = false;

            if (m_Values.IsBroken)
            {
                commonCondition = true;
            }
            else
            {
                switch (direction)
                {
                    case DirectionType.Up:
                        commonCondition = m_Values.LastMaValue < m_Values.PrevMaValue && m_Values.IsMaCross;
                        break;
                    case DirectionType.Down:
                        commonCondition = m_Values.LastMaValue > m_Values.PrevMaValue && m_Values.IsMaCross;
                        break;
                }
            }

            if (commonCondition)
            {
                long endIndex = index;

                switch (direction)
                {
                    case DirectionType.Up:
                        endPrice = m_Context.Candles.First(p => p.Index == index).Low;
                        break;

                    case DirectionType.Down:
                        endPrice = m_Context.Candles.First(p => p.Index == index).High;
                        break;
                }

                var movement = new Movement
                {
                    StartBarIndex = m_Values.LastMovementEndBarIndex,
                    StartPrice = m_Values.LastMovementEndPrice,
                    EndBarIndex = endIndex,
                    EndPrice = endPrice,
                    Index = m_Values.LastMovementIndex + 1
                };

                m_Values.LastMovementIndex = movement.Index;
                m_Values.LastMovementEndBarIndex = movement.EndBarIndex;
                m_Values.LastMovementEndPrice = movement.EndPrice;
                m_Values.LastMovementStartPrice = movement.StartPrice;

                Added.Raise(this, new MovementIndicatorAddedEventArgs { Index = index, Movement = movement });
                
                StochasticPivot.Raise(this,
                    new MovementIndicatorStochasticPivotEventArgs
                    {
                        Index = index,
                        ZoneType = direction == DirectionType.Up ? StochasticZoneType.U50ToU80 : StochasticZoneType.L50ToL20
                    });

                clearTmpVariable();
            }

        }

        private Rate barByIndex(long index)
        {
            return m_Context.Candles.First(p => p.Index == index);
        }

        private void isBroken(long index)
        {
            if (m_Values.LastMovementDirection() == DirectionType.None)
                return;

            var bar = barByIndex(index);
            
            switch (m_Values.LastMovementDirection())
            {
                case DirectionType.Up:
                    m_Values.IsBroken = bar.Low < m_Values.LastMovementStartPrice;
                    break;

                case DirectionType.Down:
                    m_Values.IsBroken = bar.High > m_Values.LastMovementStartPrice;
                    break;
            }
        }
        
        /// <summary>
        /// Анализатор на прошлом баре(на том баре который 100% был уже заверщшен)
        /// </summary>
        /// <param name="index"></param>
        public void CalculateLogic(long index)
        {
            //Отдельно расматривает случай первого движения
            if (m_Values.LastMovementDirection() == DirectionType.None)
            {
                Rate x1;
                Rate x2;
                double y1;
                double y2;

                var bar = m_Context.Candles.First(p => p.Index == index);

                if (bar.Close > bar.Open)
                {
                    y1 = bar.Low;
                    x1 = bar;
                    x2 = bar;
                    y2 = bar.High;
                }
                else
                {
                    y1 = bar.High;
                    x1 = bar;
                    x2 = bar;
                    y2 = bar.Low;
                }

                var movement = new Movement
                {
                    StartBarIndex = x1.Index,
                    EndBarIndex = x2.Index,
                    StartPrice = y1,
                    EndPrice = y2,
                    Index = 0
                };

                m_Values.LastMovementIndex = movement.Index;
                m_Values.LastMovementEndBarIndex = movement.EndBarIndex;
                m_Values.LastMovementEndPrice = movement.EndPrice;
                m_Values.LastMovementStartPrice = movement.StartPrice;

                Added.Raise(this, new MovementIndicatorAddedEventArgs { Index = index, Movement = movement });
            }
            // Вариации последнего движения расматриваются подробно в каждом подметоде ниже
            else
            {
                m_Values.PrevMaValue = m_Values.LastMaValue;
                m_Values.LastMaValue = m_Context.MaFastValues.First(x => x.Index == index).Value;

                // Переход МА13 вниз/вверх
                if (!m_Values.IsMaCross && isMa13Cross(index))
                {
                    m_Values.IsMaCross = true;
                }

                // Слом вниз когда пробивает точку старта текущего движения
                isBroken(index);
                
                // Проверка условия на начало нового движения в противоположную сторону.
                // Отрисовка при выполнении условий
                tryToCompleteMovementLine(index);

                // Обновление хая/лоу движения
                updateEndMovement(index);
            }
        }
    }
}
