﻿using System;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementIndicatorAddedEventArgs : EventArgs
    {
        /// <summary>
        /// Индекс бара, на котором случилось событие
        /// </summary>
        public long Index { get; set; }
        
        /// <summary>
        /// Добавленный Movement
        /// </summary>
        public Movement Movement { get; set; }
    }
}
