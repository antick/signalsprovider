﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementContextV2
    {
        public List<Rate> Candles { get; set; }
        public List<IndexValue<double>> MaFastValues { get; set; }
    }
}
