﻿using System;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementIndicatorUpdateEventArgs : EventArgs
    {
        /// <summary>
        /// Индекс бара, на котором случилось событие
        /// </summary>
        public long Index { get; set; }

        /// <summary>
        /// Индекс движения, которое обновилось
        /// </summary>
        public long MovementIndex { get; set; }

        /// <summary>
        /// Новый индекс бара конца движения
        /// </summary>
        public long MovementEndBarIndex { get; set; }

        /// <summary>
        /// Новая цена окончания движения
        /// </summary>
        public double MovementEndPrice { get; set; }
    }
}
