﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class MovementContext
    {
        public List<Rate> Candles { get; set; }
        public IndexedList<double> MaValues { get; set; }
        public IndexedList<StochasticZoneType> StochZones { get; set; }
    }
}
