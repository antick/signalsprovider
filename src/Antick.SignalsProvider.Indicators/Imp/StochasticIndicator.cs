﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class StochasticIndicator
    {

        public static IndexedList<StochasticIndicatorData> Calculate(StochasticIndicatorContext context)
        {
            List<double> values= new List<double>();

            for (int i = 0; i < context.Rates.Count; i++)
            {
                values.Add(calcValue(i, context.Rates, context.Pk, context.Sk));
            }

            IndexedList <StochasticIndicatorData> res = new IndexedList<StochasticIndicatorData>();
            for (int i = 0; i < context.Rates.Count; i++)
            {
                var signal = calcSignal(i, values, context.Pd);

                res.Add(
                    new IndexValue<StochasticIndicatorData>
                    {
                        Value = new StochasticIndicatorData {Value = values[i], Signal = signal},
                        Index = context.Rates[i].Index
                    }
                );
            }

            return res;
        }

        /// <summary>
        /// Вычисление стохастика на заданном диапозоне
        /// </summary>
        public static IndexValue<StochasticIndicatorData> Calculate(StochasticIndicatorContext context, int i)
        {
            var value = calcValue(i, context.Rates, context.Pk, context.Sk);
            var signal = calcSignal(i, context.Rates, context.Pk, context.Sk, context.Pd);

            return new IndexValue<StochasticIndicatorData>
            {
                Value = new StochasticIndicatorData {Value = value, Signal = signal},
                Index = context.Rates[i].Index
            };
        }

        private static double calcValue(int i, List<Rate> candles, int pk, int sk)
        {
            double FnMinLow(int period, long endIndex) => getRangeMinLow(endIndex - period + 1, endIndex, candles);
            double FnMaxHigh(int period, long endIndex) => getRangeMaxHigh(endIndex - period + 1, endIndex, candles);

            var sum1 = 0.0;

            Dictionary<long,double> cacheFnMinLow = new Dictionary<long, double>();

            for (int j = i; j > i - sk && j >= 0; j--)
            {
                var fnMinLow = FnMinLow(pk, candles[j].Index);
                sum1 += candles[j].Close - fnMinLow;

                cacheFnMinLow[candles[j].Index] = fnMinLow;
            }

            var sum2 = 0.0;

            for (int j = i; j > i - sk && j >= 0; j--)
            {
                var fnMinLow = cacheFnMinLow.ContainsKey(candles[j].Index)
                    ? cacheFnMinLow[candles[j].Index]
                    : FnMinLow(pk, candles[j].Index);


                sum2 += FnMaxHigh(pk, candles[j].Index) - fnMinLow;
            }

            var value = 0.0;
            if (sum2 == 0.0)
                value = 100.0;
            else value = 100 * sum1 / sum2;

            return value;
        }

        private static double calcSignal(int i, List<Rate> candles, int pk, int sk, int pd)
        {
            double sum = 0.0;
            for (int k = 0; k < pd && i - k >= 0; k++)
                sum += calcValue(i - k, candles, pk, sk);

            var result = sum / pd;

            return result;
        }

        private static double calcSignal(int i, List<double> values, int pd)
        {
            double sum = 0.0;
            for (int k = 0; k < pd && i - k >= 0; k++)
                sum += values[i - k];

            var result = sum / pd;

            return result;
        }


        private static double getRangeMinLow(long startIndex, long endIndex, List<Rate> candles)
        {
            return candles.Where(p => p.Index >= startIndex && p.Index <= endIndex).Min(p => p.Low);
        }

        private static double getRangeMaxHigh(long startIndex, long endIndex, List<Rate> candles)
        {
            return candles.Where(p => p.Index >= startIndex && p.Index <= endIndex).Max(p => p.High);
        }
    }
}
