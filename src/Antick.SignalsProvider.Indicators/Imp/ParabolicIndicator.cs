﻿using System;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class ParabolicIndicator
    {
        private readonly double m_Step;
        private readonly double m_AfMax;

        /// <summary>
        /// the acceleration factor
        /// </summary>
        private double m_Af;

        /// <summary>
        /// the maximum value of the price of the previous period
        /// </summary>
        private double m_High;

        /// <summary>
        /// the minimum value of the price of the previous period
        /// </summary>
        private double m_Low;

        private bool m_Flag = true;

        private bool m_Loaded = false;

        private ParabolicIndicator(ParabolicParams @params, double step, double afMax)
        {
            if (@params != null)
            {
                m_Af = @params.Af;
                m_High = @params.High;
                m_Low = @params.Low;
                m_Flag = @params.Flag;
                m_Loaded = true;
            }
            m_Step = step;
            m_AfMax = afMax;
        }

        private void calculate(int i, Rate[] candles, double[] values, int[] sars)
        {
            if (i == 0)
            {
                values[i] = 0;
                sars[i] = 0;
                return;
            }

            // Расчет первоночального значение
            if (!m_Loaded)
            {
                // Вариант вычисления в оригинальном алгоритме первого значения : (candles[i].Low - 10.0 * SymbolVars.MajorPoint);

                // Так лучше работает, хотя и косяки первые 20.
                values[i] = candles[i].Low;
                m_Flag = true;
                m_Af = m_Step;
                m_High = candles[i].High;
                m_Low = candles[i].Low;

                m_Loaded = true;
                return;
            }

            var prevBar = candles[i - 1];
            var curBar = candles[i];

            if (curBar.Close > m_High)
            {
                m_High = curBar.Close;
                if (m_Flag && m_Af <= m_AfMax - m_Step)
                    m_Af += m_Step;
            }

            if (curBar.Close < m_Low)
            {
                m_Low = curBar.Close;
                if (!m_Flag && m_Af <= m_AfMax - m_Step)
                    m_Af += m_Step;
            }

            var prevValue = values[i - 1];
            double curValue;

            if (m_Flag)
            {
                curValue = prevValue + m_Af * (m_High - prevValue);
                values[i] = curValue;
            }
            else
            {
                curValue = prevValue + m_Af * (m_Low - prevValue);
                values[i] = curValue;
            }

            if ((prevValue < prevBar.Close && curValue > curBar.Close) || (prevValue > prevBar.Close && curValue < curBar.Close))
            {
                m_Af = m_Step;
                values[i] = m_Flag ? m_High : m_Low;
                sars[i] = m_Flag ? -1 : 1;
                m_Low = curBar.Close;
                m_High = curBar.Close;
                m_Flag = !m_Flag;
            }
        }

        private ParabolicParams saveParams()
        {
            return new ParabolicParams
            {
                Af = m_Af,
                High = m_High,
                Low = m_Low,
                Flag = m_Flag
            };
        }

        public static Tuple<double[], int[]> Calculate(Rate[] candles, ParabolicParams @params, double step = 0.02,
            double afMax = 0.2)
        {
            double[] values = new double[candles.Length];
            int[] sars = new int[candles.Length];

            var parabolic = new ParabolicIndicator(@params, step, afMax);

            for (int i = 0; i < candles.Length; i++)
            {
                parabolic.calculate(i, candles, values, sars);
            }

            return new Tuple<double[], int[]>(values, sars);
        }
    }
}
