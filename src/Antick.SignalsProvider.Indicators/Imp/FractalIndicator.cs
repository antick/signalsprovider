﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class FractalIndicator
    {
        public const int DELTA_CONST = 3;

        /// <summary>
        /// Проверка сущестования указанного типа фрактала на указанном индексе в массиве
        /// </summary>
        /// <param name="rates">Исходные данные</param>
        /// <param name="mode">Тип фрактала</param>
        /// <param name="i">индекс</param>
        /// <returns></returns>
        private static bool isValid(List<Rate> rates, FractalType mode, int i)
        {
            var startIndex = rates.First().Index;
            var maxIndex = rates.Last().Index + 1;

            if (i + DELTA_CONST >= maxIndex || i - DELTA_CONST < startIndex)
                return false;

            //----
            bool fr = true;
            int a, b, count = 0;
            bool res = false;

            switch (mode)
            {

                case FractalType.Upper: //поиск верхних фракталов
                    //справа от фрактала должно быть 2 бара с более низкими максимумами
                    //слева от фрактала пожет быть группа баров, которую завершат 2 бара с более низкими максимумами 
                    //максимум любого бара из группы не должен превысить максимум фрактального бара    

                    for (b = i - 1; b > i - 3; b--)
                    {
                        if (rates[i].High < rates[b].High)
                        {
                            fr = false;
                            break;
                        }
                    }

                    a = i + 1;
                    while (count < 2)
                    {
                        if (rates[i].High <= rates[a].High)
                        {
                            fr = false;
                            break;
                        }
                        else
                        {
                            if (rates[i].High > rates[a].High) count++;
                            else count = 0;
                        }

                        a++;
                    }

                    if (fr) res = true;
                    break;


                case FractalType.Lower: //поиск нижних фракталов
                    //справа от фрактала должно быть 2 бара с более высокими минимумами
                    //слева от фрактала может быть группа баров, которую завершат 2 бара с более высокими минимумами 
                    //минимум любого бара из группы не должен быть ниже минимума фрактального бара 

                    for (b = i - 1; b > i - 3; b--)
                    {
                        if (rates[i].Low > rates[b].Low)
                        {
                            fr = false;
                            break;
                        }
                    }

                    a = i + 1;
                    while (count < 2)
                    {
                        if (rates[i].Low >= rates[a].Low)
                        {
                            fr = false;
                            break;
                        }

                        if (rates[i].Low < rates[a].Low) count++;
                        else count = 0;
                        a++;
                    }

                    if (fr) res = true;
                    break;
            }

            //----
            return res;
        }

        public static  FractIndicatorResult Calculate(List<Rate> rates)
        {
            FractIndicatorResult result = new FractIndicatorResult {Upper = new List<bool>(), Lower = new List<bool>()};

            for (int i = 0; i < rates.Count; i++)
            {
                result.Upper[i] = isValid(rates, FractalType.Upper, i);
                result.Lower[i] = isValid(rates, FractalType.Lower, i);
            }

            return result;
        }
    }
}

