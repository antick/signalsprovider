﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class SmaIndicator
    {
        public static List<IndexValue<double>> Calculate(List<Rate> candles, int period)
        {
            IndexValue<double>[] result = new IndexValue<double>[candles.Count];

            for (var i = 0; i < candles.Count; i++)
            {
                var value =  i < period
                    ? 0
                    : candles.Where(p => p.Index > candles[i - period].Index && p.Index <= candles[i].Index)
                          .Sum(p => p.Close) / period;

                result[i] = new IndexValue<double> {Value = value, Index = candles[i].Index};
            }
            return result.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="candles">данные</param>
        /// <param name="period">Период Средней</param>
        /// <param name="points">Точность данных(в знаках после запятой)</param>
        /// <param name="step">Шаг в пунктах(Ренко тип таймфрейма)</param>
        /// <returns></returns>
        public static List<IndexValue<double>> Calculate(List<Rate> candles, int period, int points, int step)
        {
            IndexValue<double>[] result = new IndexValue<double>[candles.Count];

            var pow = Math.Pow(10, points);

            for (var i = 0; i < candles.Count; i++)
            {
                var value = i < period
                    ? 0
                    : candles.Where(p => p.Index > candles[i - period].Index && p.Index <= candles[i].Index)
                          .Sum(p => p.Close) / period;

                result[i] = new IndexValue<double> { Value = value, Index = candles[i].Index };
            }

            return result.Select(x => new IndexValue<double>
            {
                Value = ((int) (x.Value * pow / step)) / pow * step,
                Index = x.Index
            }).ToList();
        }


        public static double Calculate(List<Rate> candles, int period, int i, int points, int step)
        {
            var pow = Math.Pow(10, points);

            var result = i < period
                ? 0
                : candles.Where(p => p.Index > candles[i - period].Index && p.Index <= candles[i].Index)
                      .Sum(p => p.Close) / period;

            result = ((int) (result * pow / step)) / pow * step;

            return result;
        }

        public static List<IndexValue<double>> CalculateAo(List<Rate> candles)
        {
            var slow = Calculate(candles, 34);
            var fast = Calculate(candles, 5);

            IndexValue<double>[] result = new IndexValue<double>[candles.Count];

            for (int i = 0; i < slow.Count; i++)
            {
                result[i] = new IndexValue<double> {Index = slow[i].Index, Value = slow[i].Value - fast[i].Value};
            }

            return result.ToList();
        }
    }
}
