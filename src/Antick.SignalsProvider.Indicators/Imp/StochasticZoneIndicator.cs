﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators.Imp
{
    public class StochasticZoneIndicator
    {
        private readonly StochasticZoneContext m_Context;

        private StochasticZoneMode m_Mode;

        /// <summary>
        /// Важные переменные для всего цикла
        /// </summary>
        private StochasticZone _lastZone;
        private StochasticZone _currentZone = new StochasticZone();

        public long LastIndex { get; private set; }

        public StochasticZoneIndicator(StochasticZoneContext context, StochasticZoneDynamicValues @params = null)
        {
            m_Context = context;
            _lastZone = @params != null
                ? new StochasticZone { Type = @params.LastType, Index = @params.LastIndex }
                : new StochasticZone();

            m_Mode = context.Mode ?? StochasticZoneMode.Default;
        }

        public StochasticZoneDynamicValues GetDynamicValues()
        {
            return new StochasticZoneDynamicValues {LastIndex = _lastZone.Index, LastType = _lastZone.Type};
        }

        /// <summary>
        /// Пересчет на указанном баре
        /// </summary>
        public StochasticZoneType CalculateLogic(int i)
        {
            var candles = m_Context.Candles;
            var stochasticData = m_Context.StochasticData;
            var index = candles[i].Index;

            Rate lastBar = _lastZone.Index == -1 ? candles.First() : candles.First(p => p.Index == _lastZone.Index);
            
            var nextStockValues = stochasticData.Where(p => p.Index >= _lastZone.Index + 1 && p.Index <= index).ToList();
            var curStockValue = stochasticData.First(p => p.Index == lastBar.Index);

            bool? trendDownStochValue = nextStockValues.Any()
                ? nextStockValues.Min(p => p.Value.Value) < curStockValue.Value.Value
                : (bool?)null;

            bool? trendUpStochValue = nextStockValues.Any()
                ? nextStockValues.Max(p => p.Value.Value) > curStockValue.Value.Value
                : (bool?)null;
            
            bool? trendDownStochSignal = nextStockValues.Any()
                ? nextStockValues.Min(p => p.Value.Signal) < curStockValue.Value.Signal
                : (bool?)null;

            bool? trendUpStochSignal = nextStockValues.Any()
                ? nextStockValues.Max(p => p.Value.Signal) > curStockValue.Value.Signal
                : (bool?)null;

            double StochVal(long ind) => stochasticData.First(p => p.Index == ind).Value.Value;
            double StochSygn(long ind) => stochasticData.First(p => p.Index == ind).Value.Signal;

            if (trendDownStochValue.HasValue && trendDownStochValue.Value && StochVal(index) < 50.0 &&
                (_lastZone.Index == -1 ||
                 (_lastZone.Type == StochasticZoneType.U50ToU80 ||
                  _lastZone.Type == StochasticZoneType.L50ToU50 ||
                  _lastZone.Type == StochasticZoneType.L20ToU50)))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.ValueIndex = index;

                if (_currentZone.SignalIndex != -1)
                {
                    switch (_lastZone.Type)
                    {
                        case StochasticZoneType.U50ToU80:
                            _currentZone.Type = StochasticZoneType.U80ToL50;
                            break;
                        case StochasticZoneType.L20ToU50:
                        case StochasticZoneType.L50ToU50:
                            _currentZone.Type = StochasticZoneType.U50ToL50;
                            break;
                    }
                    if (_lastZone.Index == -1)
                    {
                        _currentZone.Type = StochasticZoneType.U80ToL50;
                    }
                }
            }

            if (trendDownStochSignal.HasValue && trendDownStochSignal.Value && StochSygn(index) < 50.0 &&
                (_lastZone.Index == -1 ||
                 _lastZone.Type == StochasticZoneType.U50ToU80 ||
                 _lastZone.Type == StochasticZoneType.L50ToU50 ||
                 _lastZone.Type == StochasticZoneType.L20ToU50))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochSignal
                _currentZone.SignalIndex = index;

                if (_currentZone.ValueIndex != -1)
                {
                    switch (_lastZone.Type)
                    {
                        case StochasticZoneType.U50ToU80:
                            _currentZone.Type = StochasticZoneType.U80ToL50;
                            break;
                        case StochasticZoneType.L20ToU50:
                        case StochasticZoneType.L50ToU50:
                            _currentZone.Type = StochasticZoneType.U50ToL50;
                            break;
                    }
                    if (_lastZone.Index == -1)
                    {
                        _currentZone.Type = StochasticZoneType.U80ToL50;
                    }
                }
            }

            if (trendUpStochValue.HasValue && trendUpStochValue.Value && StochVal(index) > 50.0 &&
                (_lastZone.Index == -1 ||
                 _lastZone.Type == StochasticZoneType.L50ToL20 ||
                 _lastZone.Type == StochasticZoneType.U80ToL50 ||
                 _lastZone.Type == StochasticZoneType.U50ToL50))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.ValueIndex = index;

                if (_currentZone.SignalIndex != -1)
                {
                    switch (_lastZone.Type)
                    {
                        case StochasticZoneType.L50ToL20:
                            _currentZone.Type = StochasticZoneType.L20ToU50;
                            break;
                        case StochasticZoneType.U50ToL50:
                        case StochasticZoneType.U80ToL50:
                            _currentZone.Type = StochasticZoneType.L50ToU50;
                            break;
                    }
                    if (_lastZone.Index == -1)
                    {
                        _currentZone.Type = StochasticZoneType.L20ToU50;
                    }
                }
            }

            if (trendUpStochSignal.HasValue && trendUpStochSignal.Value && StochSygn(index) > 50.0 &&
                (_lastZone.Index == -1 ||
                 _lastZone.Type == StochasticZoneType.L50ToL20 ||
                 _lastZone.Type == StochasticZoneType.U80ToL50 ||
                 _lastZone.Type == StochasticZoneType.U50ToL50))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.SignalIndex = index;

                if (_currentZone.ValueIndex != -1)
                {
                    switch (_lastZone.Type)
                    {
                        case StochasticZoneType.L50ToL20:
                            _currentZone.Type = StochasticZoneType.L20ToU50;
                            break;
                        case StochasticZoneType.U50ToL50:
                        case StochasticZoneType.U80ToL50:
                            _currentZone.Type = StochasticZoneType.L50ToU50;
                            break;
                    }
                    if (_lastZone.Index == -1)
                    {
                        _currentZone.Type = StochasticZoneType.L20ToU50;
                    }
                }
            }

            if (trendDownStochValue.HasValue && trendDownStochValue.Value && StochVal(index) <= 20.0 &&
                (_lastZone.Index == -1 ||
                 (_lastZone.Type == StochasticZoneType.U80ToL50 ||
                  _lastZone.Type == StochasticZoneType.U50ToL50)))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.ValueIndex = index;

                if (_currentZone.SignalIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.L50ToL20;
                }
            }

            if (trendDownStochSignal.HasValue && trendDownStochSignal.Value && StochSygn(index) <= 20.0 &&
                (_lastZone.Index == -1 || (_lastZone.Type == StochasticZoneType.U80ToL50 || _lastZone.Type == StochasticZoneType.U50ToL50)))
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.SignalIndex = index;

                if (_currentZone.ValueIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.L50ToL20;
                }
            }

            if (trendUpStochValue.HasValue && trendUpStochValue.Value && StochVal(index) >= 80.0)
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.ValueIndex = index;

                if (_currentZone.SignalIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.U50ToU80;
                }
            }

            if (trendUpStochSignal.HasValue && trendUpStochSignal.Value && StochSygn(index) >= 80.0)
            {
                // когда тренд вниз, переход вниз за 50 и это не последний бар - был переход по StochValue
                _currentZone.SignalIndex = index;

                if (_currentZone.ValueIndex != -1)
                {
                    _currentZone.Type = StochasticZoneType.U50ToU80;
                }
            }

            var result = StochasticZoneType.None;

            if (_currentZone.Declared() && (_lastZone.Index == -1 || anyTurn()))
            {
                _currentZone.Index = (_currentZone.ValueIndex >= _currentZone.SignalIndex)
                    ? _currentZone.ValueIndex
                    : _currentZone.SignalIndex;

                _lastZone = _currentZone.Clone();
                result = _lastZone.Type;

            }

            _currentZone.Clear();
            LastIndex = index;

            return result;
        }

        /// <summary>
        /// Условия любого перехода в любую сторону
        /// </summary>
        private bool anyTurn()
        {
            return
                // Зоны "Выше 80 - Ниже 50"
                turn(StochasticZoneType.U80ToL50, StochasticZoneType.U50ToU80) ||
                turn(StochasticZoneType.U80ToL50, StochasticZoneType.L50ToL20) ||
                turn(StochasticZoneType.U80ToL50, StochasticZoneType.L50ToU50) ||

                turn(StochasticZoneType.L50ToL20, StochasticZoneType.L20ToU50) ||
                turn(StochasticZoneType.L50ToL20, StochasticZoneType.U50ToU80) ||

                // Зоны "Выше 50 - Выше 50"
                turn(StochasticZoneType.U50ToU80, StochasticZoneType.U80ToL50) ||
                turn(StochasticZoneType.U50ToU80, StochasticZoneType.L50ToL20) ||

                turn(StochasticZoneType.L20ToU50, StochasticZoneType.U50ToU80) ||
                turn(StochasticZoneType.L20ToU50, StochasticZoneType.U50ToL50) ||

                turn(StochasticZoneType.L50ToU50, StochasticZoneType.U50ToU80) ||
                turn(StochasticZoneType.L50ToU50, StochasticZoneType.L50ToL20) ||
                turn(StochasticZoneType.L50ToU50, StochasticZoneType.U50ToL50) ||

                turn(StochasticZoneType.U50ToL50, StochasticZoneType.L50ToL20) ||
                turn(StochasticZoneType.U50ToL50, StochasticZoneType.L50ToU50);
        }

        /// <summary>
        /// Переход из одной зоны в другую
        /// </summary>
        private bool turn(StochasticZoneType from, StochasticZoneType to)
        {
            return _lastZone.Type == from && _currentZone.Type == to;
        }
    }
}
