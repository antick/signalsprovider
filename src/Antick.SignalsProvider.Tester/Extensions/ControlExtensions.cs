﻿using System;
using System.Windows.Forms;

namespace Antick.SignalsProvider.Tester.Extensions
{
    public static class ControlExtensions
    {
        public static void InvokeAsync<T>(this T c, Action<T> action)
            where T : Control
        {
            if (c.InvokeRequired)
            {
                c.Invoke(new Action(() => action(c)));
            }
            else
            {
                action(c);
            }
        }
    }
}
