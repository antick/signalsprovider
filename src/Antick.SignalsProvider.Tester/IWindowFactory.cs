﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Mvp.Models;

namespace Antick.SignalsProvider.Tester
{
    public interface IWindowFactory
    {
        WindowModel Create(WindowType windowType);
        void Release(WindowModel component);
    }
}
