﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers;

namespace Antick.SignalsProvider.Tester
{
    public interface ISignalProviderFactory
    {
        ISignalProvider Create(SignalProviderModel model);
        void Release(IServiceProvider component);
    }
}
