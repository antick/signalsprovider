﻿using System.Windows.Forms;
using Antick.Cqrs;
using Antick.RateManager.Client;
using Antick.SignalsProvider.Components;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.IndicatorProviders;
using Antick.SignalsProvider.IndicatorProviders.MovementProvider;
using Antick.SignalsProvider.IndicatorProviders.SmaProvider;
using Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider;
using Antick.SignalsProvider.Indicators;
using Antick.SignalsProvider.Providers;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Mvp.Models;
using Antick.SignalsProvider.Tester.Mvp.Presenters;
using Antick.SignalsProvider.Tester.Mvp.Views;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Inceptum.AppServer.Configuration;

namespace Antick.SignalsProvider.Tester
{
    public class WindsorInstaller  : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<LoggingFacility>();
            container.AddFacility<TypedFactoryFacility>();

            // View MPV
            container.Register(Component.For<ApplicationContext>()); // базовый компонент WindowForms
            container.Register(Component.For<ITerminalView>().ImplementedBy<TerminalView>());
            container.Register(Component.For<WindowControl>().LifestyleTransient());
            container.Register(Component.For<IWindowControlFactory>().AsFactory());

            // Presendter MVP
            container.Register(Component.For<TerminalPresenter>());

            // Model MVP
            container.Register(Component.For<WindowModel>().LifestyleTransient());
            container.Register(Component.For<IWindowFactory>().AsFactory());
            container.Register(Component.For<TerminalModel>().UsingFactoryMethod((kernel, context) =>
            {
                var windowFactory = kernel.Resolve<IWindowFactory>();
                return new TerminalModel(windowFactory.Create(WindowType.High));
            }));

            container.Register(Component.For<IRateManagerClient>()
                .UsingFactoryMethod((kernel, context) => new RateManagerClient("http://localhost:2002")));

            container.Install(new CqrsInstaller(Types.FromAssemblyInThisApplication()));

            container.Register(Component.For<IWaveFileStorage>().ImplementedBy<StubWaveFileStorage>());
            container.Register(Component.For<ISignalProviderCache>().ImplementedBy<SignalProviderCache>());

            // Reginster Indicators
            container.Register(Component.For<IIndicatorProvider>().ImplementedBy<IndicatorProvider>());
            container.Register(Component.For<ISmaIndicatorProvider>().ImplementedBy<SmaIndicatorProvider>());
            container.Register(Component.For<IStochasticIndicatorProvider>().ImplementedBy<StochasticIndicatorProvider>());
            container.Register(Component.For<IStochasticZoneIndicatorProvider>().ImplementedBy<StochasticZoneIndicatorProvider>());
            container.Register(Component.For<IMovementIndicatorProvider>().ImplementedBy<MovementIndicatorProvider>());

            container.Register(Component.For<ISignalProvider>().ImplementedBy<WaveV1Provider>().LifestyleTransient());
            container.Register(Component.For<ISignalProviderFactory>().AsFactory());
        }
    }
}
