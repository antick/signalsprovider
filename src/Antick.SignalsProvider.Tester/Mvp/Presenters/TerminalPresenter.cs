﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Antick.Infractructure.Components.Mvp.Presenters;
using Antick.SignalsProvider.Tester.Contracts.Args;
using Antick.SignalsProvider.Tester.Mvp.Models;
using Antick.SignalsProvider.Tester.Mvp.Views;
using Castle.Core.Logging;
using Castle.MicroKernel;

namespace Antick.SignalsProvider.Tester.Mvp.Presenters
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class TerminalPresenter : Presenter<ITerminalView,TerminalModel>
    {
        private readonly ILogger m_Logger;

        public TerminalPresenter(IKernel kernel, ITerminalView view, TerminalModel model, ILogger logger) : base(kernel, view, model)
        {
            m_Logger = logger;

            View.ViewStarted += View_ViewStarted;
            View.InstrumentChanging += View_InstrumentChanging;
            View.TimeFrameValueChanging += View_TimeFrameValueChanging;
            View.StartTimeChanging += View_StartTimeChanging;
            View.EndTimeChanging += View_EndTimeChanging;
            View.RefreshStarting += View_RefreshStarting;
            View.StochasticParamsChanged += View_StochasticParamsChanged;
        }
        private void View_StochasticParamsChanged(object sender, StochasticParamsChangedEventArgs e)
        {
            Model.ChangeStochasticParams(e);
        }

        private void View_TimeFrameValueChanging(object sender, TimeFrameValueChangingEventArgs e)
        {
            Model.ChangeTimeFrame(e.Value);
        }

        private void View_StartTimeChanging(object sender, DateChangingEventArgs e)
        {
            Model.ChangeStartTime(e.Time);
        }

        private void View_EndTimeChanging(object sender, DateChangingEventArgs e)
        {
            Model.ChangeEndTime(e.Time);
        }

        private void View_InstrumentChanging(object sender, InstrumentChangingEventArgs e)
        {
            Model.ChangeInstrument(e.Instrument);
        }

        private void View_RefreshStarting(object sender, RefreshStartingEventArgs e)
        {
            render();
        }

        private void View_ViewStarted(object sender, EventArgs e)
        {
            render();
        }

        public override void Run()
        {
            View.Show();
        }

        private void render()
        {
            var render = Model.GenerateRenderData();
            View.Render(render);
        }
    }
}
