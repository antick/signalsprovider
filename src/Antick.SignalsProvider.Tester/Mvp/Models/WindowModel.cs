﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.RateManager.Client;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Cqrs.WaveAlg;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Antick.SignalsProvider.Tester.Components.Indicators;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Contracts.Render;
using Newtonsoft.Json;

namespace Antick.SignalsProvider.Tester.Mvp.Models
{
    public class WindowModel
    {
        public Instrument Instrument { get; set; }

        public int TimeFrameValue { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }
        
        public int Pk = 20;
        public int Sk = 2;
        public int Pd = 3;

        public IndicatorType IndicatorType { get; set; }

        private readonly WindowType m_WindowType;

        /// <summary>
        /// Кол-во пропускаемых баров и данных в начале(идет о самого тяжелого расчетого, 208 - это период тяжелой МА )
        /// </summary>
        private const int skipVal = 832;

        private readonly IRateManagerClient m_RateManagerClient;
        private readonly ISignalProviderCache m_SignalProviderCache;
        private readonly ISignalProviderFactory m_SignalProviderFactory;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IQueryBuilder m_QueryBuilder;
       
        public WindowModel(IRateManagerClient rateManagerClient, ISignalProviderCache signalProviderCache, ISignalProviderFactory signalProviderFactory, ICommandBuilder commandBuilder, IQueryBuilder queryBuilder, WindowType windowType)
        {
            m_RateManagerClient = rateManagerClient;
            m_SignalProviderCache = signalProviderCache;
            m_SignalProviderFactory = signalProviderFactory;
            m_CommandBuilder = commandBuilder;
            m_QueryBuilder = queryBuilder;
            m_WindowType = windowType;
            Instrument = Instrument.EUR_USD;
            TimeFrameValue = 5;
            EndTime = DateTime.Now;
            StartTime = EndTime.AddDays(-2);
            IndicatorType = IndicatorType.None;
        }

        public RenderData GenerateRenderData()
        {
            Stopwatch w = new Stopwatch();

            w.Start();
            var data = coreGenerateRenderData();
            w.Stop();

            data.GeneratedTotalTime = w.Elapsed;

            return data;
        }

        private List<Rate> getCandles()
        {
            var starttime = new DateTime(StartTime.Year, StartTime.Month, StartTime.Day, 0, 0, 0, DateTimeKind.Local);
            var endTime = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 23, 59, 59, DateTimeKind.Local);

            var candles = m_RateManagerClient.GetRates(Instrument.ToString(), TimeFrameType.Renko,
                TimeFrameValue.ToString(), starttime, endTime).Result;

            if (!candles.Any())
                return new List<Rate>();

            var firstTime = candles.First().Time;

            var firstCanles = m_RateManagerClient.GetRatesByLastTime(Instrument.ToString(), TimeFrameType.Renko,
                TimeFrameValue.ToString(), firstTime, skipVal).Result;

            candles.AddRange(firstCanles);
            candles = candles.OrderBy(p => p.Index).ToList();

            return candles;
        }



        private RenderData coreGenerateRenderData()
        {

            var waveAlgParameters = new WaveV1Parameters
            {
                MaFastPeriod = 26,
                MaSlowPeriod = 208,
                MaVerySlowPeriod = 832,
                StochasticPk = Pk,
                StochasticSk = Sk,
                StochasticPd = Pd,
                Tf = new TimeFrameComplex { Type = TimeFrameType.Renko, Value = TimeFrameValue.ToString() }
            };

            var waveParams = waveAlgParameters;

            var model = new SignalProviderModel(0, "test",
                "WaveV1",
                new InstrumentComplex
                {
                    Group = InstrumentGroup.Fx,
                    Name = "EUR_USD",
                    Provider = InstrumentProvider.Oanda
                }, JsonConvert.SerializeObject(waveParams), true);

            var waveProvider = m_SignalProviderFactory.Create(model);
            waveProvider.Configure();

            m_SignalProviderCache.AddOrUpdate(waveProvider);

            var ptf = new Ptf(model.Id, model.Instrument,
                new TimeFrameComplex { Type = TimeFrameType.Renko, Value = TimeFrameValue.ToString() });

            var candles = getCandles();

            m_CommandBuilder.Execute(new ProccessingWaveV1Context(ptf, candles) {UseWaveAlg = false});


            var waveTfData = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var renderData = new RenderData();
            var count = 0;

            var allCandles = waveTfData.Rates.Select(x => new RenderCandle
            {
                Time = x.Time.ToLocalTime(),
                Open = x.Open,
                Close = x.Close,
                High = x.High,
                Low = x.Low,
                Index = x.Index,
                Order = count++
            }).ToList();

            renderData.Candles =
                allCandles.Skip(skipVal).ToList();
            renderData.Spec = Utils.InstrumentSpec.Get(Instrument.ToString());

            renderData.MaFastValues = waveTfData.MaFast.Skip(skipVal).Select(x => x.Value).ToList();
            renderData.MaSlowValues = waveTfData.MaSlow.Skip(skipVal).Select(x => x.Value).ToList();
            renderData.MaVerySlowValues = waveTfData.MaVerySlow.Skip(skipVal).Select(x => x.Value).ToList();
            
            renderData.StockValue = waveTfData.Stochastic.Skip(skipVal).Select(x => x.Value.Value).ToList();
            renderData.StockSignal = waveTfData.Stochastic.Skip(skipVal).Select(x => x.Value.Signal).ToList();

            IndexedList<StochasticZoneType> StochasticZone = new IndexedList<StochasticZoneType>();
            for (int i = 0; i < renderData.Candles.Count; i++)
            {
                StochasticZone.Add(new IndexValue<StochasticZoneType>
                {
                    Value = StochasticZoneType.None,
                    Index = renderData.Candles[i].Index
                });
            }

            for (int i = 0; i < waveTfData.PivotZones.Count; i++)
            {
                var item = StochasticZone.FirstOrDefault(p => p.Index == waveTfData.PivotZones[i].Index);
                if (item != null)
                {
                    item.Value = waveTfData.PivotZones[i].Value;
                }
            }
            renderData.StochasticZone = StochasticZone.ToList().Select(p => p.Value).ToList();

            renderData.Movements = waveTfData.Movements.Select(x => new Zigzag
            {
                Index = x.Index,
                StartPrice = x.StartPrice,
                EndPrice = x.EndPrice,
                StartBar = allCandles.FirstOrDefault(p => p.Index == x.StartBarIndex),
                EndBar = allCandles.FirstOrDefault(p => p.Index == x.EndBarIndex)
            }).ToList();

            renderData.TimeFrameValue = TimeFrameValue;
            renderData.StartTime = StartTime;
            renderData.EndTime = EndTime;
            renderData.Pk = Pk;
            renderData.Sk = Sk;
            renderData.Pd = Pd;





            renderData.Tmp = CalculateAveragePrice(renderData.Candles, 10).ToList();
            renderData.Tmp2 = CalculateAveragePrice(renderData.Candles, 40).ToList();



            return renderData;

        }

        private double[] CalculateAveragePrice(List<RenderCandle> input, int period)
        {

            double[] buff = new double[input.Count];
            int maxCount = period;



            for (int i = 0; i < input.Count; i += maxCount)
            {
                var curIndex = input[i].Index;
                var maxIndex = curIndex + maxCount;

                var avr = input.Where(p => p.Index >= curIndex && p.Index < maxIndex)
                              .Sum(p => (p.High + p.Low) / 2.0) / maxCount;

                for (int j = i; j < i + maxCount && j < input.Count; j++)
                {
                    buff[j] = avr;
                }
            }

            return buff;
        }
    }
}
