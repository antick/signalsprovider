﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.Infractructure.Components.Mvp.Models;
using Antick.RateManager.Client;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Cqrs.WaveAlg;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Tester.Components.Indicators;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Contracts.Args;
using Antick.SignalsProvider.Tester.Contracts.Render;
using Newtonsoft.Json;

namespace Antick.SignalsProvider.Tester.Mvp.Models
{
    public class TerminalModel : Model
    {
        private readonly WindowModel m_WindowModel;
        

        public TerminalModel(WindowModel windowModel)
        {
            m_WindowModel = windowModel;
        }

        public void ChangeTimeFrame(int value)
        {
            m_WindowModel.TimeFrameValue = value;
        }

        public void ChangeStartTime(DateTime time)
        {
            m_WindowModel.StartTime = time;
        }

        public void ChangeEndTime(DateTime time)
        {
            m_WindowModel.EndTime = time;
        }

        public void ChangeInstrument(Instrument instrument)
        {
            m_WindowModel.Instrument = instrument;
        }

        public void ChangeStochasticParams(StochasticParamsChangedEventArgs args)
        {
            if (args.Pk.HasValue)
                m_WindowModel.Pk = args.Pk.Value;
            if (args.Sk.HasValue)
                m_WindowModel.Sk = args.Sk.Value;
            if (args.Pd.HasValue)
                m_WindowModel.Pd = args.Pd.Value;
        }

        public RenderWindowParams GetWindowParams()
        {
            var window = m_WindowModel;

            return new RenderWindowParams
            {
                TimeFrameValue = window.TimeFrameValue,
                StartTime = window.StartTime,
                EndTime = window.EndTime,
                Pk = window.Pk,
                Sk = window.Sk,
                Pd = window.Pd
            };
        }

        public RenderData GenerateRenderData()
        {
            return m_WindowModel.GenerateRenderData();
        }
    }
}

