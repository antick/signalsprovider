﻿using System;
using Antick.Infractructure.Components.Mvp.Views;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Contracts.Args;
using Antick.SignalsProvider.Tester.Contracts.Render;

namespace Antick.SignalsProvider.Tester.Mvp.Views
{
    public interface ITerminalView : IView
    {
        /// <summary>
        /// Генерируется после первой отрисовки приложения
        /// </summary>
        event EventHandler<EventArgs> ViewStarted;

        event EventHandler<TimeFrameValueChangingEventArgs> TimeFrameValueChanging;
        event EventHandler<DateChangingEventArgs> StartTimeChanging;
        event EventHandler<DateChangingEventArgs> EndTimeChanging;
        event EventHandler<InstrumentChangingEventArgs> InstrumentChanging;

        /// <summary>
        /// Старт полной отрисовки с получением данных
        /// </summary>
        event EventHandler<RefreshStartingEventArgs> RefreshStarting;
        
        /// <summary>
        /// Нажатие кнопки выхода в меню
        /// </summary>
        event EventHandler<EventArgs> ExitingApplication;
        
        event EventHandler<StochasticParamsChangedEventArgs> StochasticParamsChanged;

        /// <summary>
        /// Отрисовка
        /// </summary>
        /// <param name="renderData"></param>
        void Render(RenderData renderData);

        void RenderWindowParams(RenderWindowParams windowParams);
    }
}
