﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Antick.Contracts.Domain;
using Antick.Infractructure.Extensions;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Tester.Components.Indicators;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Contracts.Args;
using Antick.SignalsProvider.Tester.Contracts.Render;
using Antick.SignalsProvider.Tester.Mvp.Views.Series;
using Antick.SignalsProvider.Tester.Resources;

namespace Antick.SignalsProvider.Tester.Mvp.Views
{
    public partial class TerminalView : Form, ITerminalView
    {
        private readonly ApplicationContext m_Context;
        private readonly IWindowControlFactory m_WindowControlFactory;

        public event EventHandler<EventArgs> ViewStarted;
        public event EventHandler<TimeFrameValueChangingEventArgs> TimeFrameValueChanging;
        public event EventHandler<DateChangingEventArgs> StartTimeChanging;
        public event EventHandler<DateChangingEventArgs> EndTimeChanging;
        public event EventHandler<InstrumentChangingEventArgs> InstrumentChanging;
        public event EventHandler<RefreshStartingEventArgs> RefreshStarting;
        public event EventHandler<EventArgs> ExitingApplication;
        public event EventHandler<StochasticParamsChangedEventArgs> StochasticParamsChanged;


        private WindowControl m_LeftWindow;
        private AreaSelection m_AreaSelection;

        public TerminalView(ApplicationContext context, IWindowControlFactory windowControlFactory)
        {
            m_Context = context;
            m_WindowControlFactory = windowControlFactory;
            InitializeComponent();

        }

        public new void Show()
        {
            m_Context.MainForm = this;
            Application.Run(m_Context);
            Application.ThreadException += Application_ThreadException;
        }

        #region Combobox and textbox convertation
        

        private int TimeFrameValue
        {
            get
            {
                try
                {
                    return Convert.ToInt32(textBoxTimeFrameValue.Text);
                }
                catch
                {
                    return 0;
                }
            }
        }


        private int? Pk
        {
            get
            {
                try
                {
                    return Convert.ToInt32(textBoxPk.Text);
                }
                catch
                {
                    return null;
                }
            }
        }

        private int? Sk
        {
            get
            {
                try
                {
                    return Convert.ToInt32(textBoxSk.Text);
                }
                catch
                {
                    return null;
                }
            }
        }

        private int? Pd
        {
            get
            {
                try
                {
                    return Convert.ToInt32(textBoxPd.Text);
                }
                catch
                {
                    return null;
                }
            }
        }

        


        private DateTime StartTime => dateTimePickerStartTime.Value;
        private DateTime EndTime => dateTimePickerEndTime.Value;

        #endregion

        private void textBoxTimeFrameValue_TextChanged(object sender, EventArgs e)
        {
            TimeFrameValueChanging.Raise(this, new TimeFrameValueChangingEventArgs{Value = TimeFrameValue});
        }
        
        private void dateTimePickerStartTime_ValueChanged(object sender, EventArgs e)
        {
            StartTimeChanging.Raise(this, new DateChangingEventArgs {Time = StartTime});
        }
        
        private void dateTimePickerEndTime_ValueChanged(object sender, EventArgs e)
        {
            EndTimeChanging.Raise(this, new DateChangingEventArgs {Time = EndTime});
        }
        
        #region toolStripMenu Items 

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitingApplication.Raise(this, new EventArgs());
        }

        #endregion

        #region misc EventHandlers

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            RefreshStarting.Raise(this, new RefreshStartingEventArgs());
        }

      

        private void mfbToolsForm_Load(object sender, EventArgs e)
        {
            m_LeftWindow = m_WindowControlFactory.Create();
            m_LeftWindow.Size = new Size(panelCharts.Width, panelCharts.Height);
            m_LeftWindow.Left = 0;
            m_LeftWindow.Top = 0;
            m_LeftWindow.CursorDataCatching += window_CursorDataCatching;
            m_LeftWindow.AreaSelectionChanged += M_LeftWindow_AreaSelectionChanged;
            panelCharts.Controls.Add(m_LeftWindow);
            
            MfbToolsForm_SizeChanged(sender, e);

            ViewStarted.Raise(this, new EventArgs());
        }

        private void M_LeftWindow_AreaSelectionChanged(object sender, AreaSelectionChangedEventArgs e)
        {
            m_AreaSelection = e.AreaSelection;
        }

        private void window_CursorDataCatching(object sender, CursorDataCatchingEventArgs e)
        {
            this.InvokeAsync(c =>
            {
                labelCurrentTime.Text = e.Date.ToString("yyyy.MM.dd hh:mm:ss");
                labelCandleData.Text = e.ChartData;
                labelIndicatorData.Text = e.IndicatorData;
            });
        }

        #endregion

       
        public void Render(RenderData renderData)
        {
            this.InvokeAsync(c =>
            {
                Stopwatch w = new Stopwatch();
                w.Start();
                m_LeftWindow.RenderData(renderData);
                w.Stop();

                var currentData = renderData;

                textBoxTimeFrameValue.Text = currentData.TimeFrameValue.ToString();
                dateTimePickerStartTime.Value = currentData.StartTime;
                dateTimePickerEndTime.Value = currentData.EndTime;
                textBoxPk.Text = currentData.Pk.ToString();
                textBoxSk.Text = currentData.Sk.ToString();
                textBoxPd.Text = currentData.Pd.ToString();

                labelSpendTimeBack.Text = currentData.GeneratedTotalTime.TotalSeconds.ToString("0.0");
                labelSpendTimeFront.Text = w.Elapsed.TotalSeconds.ToString("0.0");

            });
        }

        public void Render(RenderData renderData, WindowType type)
        {
            m_LeftWindow.RenderData(renderData);
        }

        public void RenderWindowParams(RenderWindowParams windowParams)
        {
            this.InvokeAsync(c =>
            {
                textBoxTimeFrameValue.Text = windowParams.TimeFrameValue.ToString();
                dateTimePickerStartTime.Value = windowParams.StartTime;
                dateTimePickerEndTime.Value = windowParams.EndTime;
                textBoxPk.Text = windowParams.Pk.ToString();
                textBoxSk.Text = windowParams.Sk.ToString();
                textBoxPd.Text = windowParams.Pd.ToString();
            });
        }

        private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            this.InvokeAsync(c =>
            {
                MessageBox.Show(this, Localization.ErrorApplication);
            });
        }

        private void MfbToolsForm_SizeChanged(object sender, EventArgs e)
        {
            this.SuspendLayout();

            var width = this.Size.Width - panelLeft.Width;
            var height = this.Size.Height - panelUpper.Top - panelUpper.Height - panelChartDetails.Height;

            panelCharts.Width = width;
            panelCharts.Height = height;
            
            var windowWidth = panelCharts.Width;
            var windowHeight = panelCharts.Height - panelChartDetails.Height;

            m_LeftWindow.Size = new Size(windowWidth, windowHeight);
            m_LeftWindow.Visible = true;

            panelChartDetails.Width = width;
            panelChartDetails.Left = 0;
            panelChartDetails.Top = m_LeftWindow.Height;
            

            this.ResumeLayout();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBoxPk_TextChanged(object sender, EventArgs e)
        {
            StochasticParamsChanged.Raise(this, new StochasticParamsChangedEventArgs{Pk = Pk, Sk = Sk, Pd = Pd});
        }

        private void textBoxSk_TextChanged(object sender, EventArgs e)
        {
            StochasticParamsChanged.Raise(this, new StochasticParamsChangedEventArgs { Pk = Pk, Sk = Sk, Pd = Pd });
        }

        private void textBoxPd_TextChanged(object sender, EventArgs e)
        {
            StochasticParamsChanged.Raise(this, new StochasticParamsChangedEventArgs { Pk = Pk, Sk = Sk, Pd = Pd });
        }

        private void TerminalView_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void checkBoxSecodAreaVisible_CheckedChanged(object sender, EventArgs e)
        {
            m_LeftWindow.IsSecondAreaVisible = checkBoxSecodAreaVisible.Checked;
        }
        
        private void buttonSubDays_Click(object sender, EventArgs e)
        {
            var days = Convert.ToInt32(textBoxCountChangeDays.Text);

            dateTimePickerStartTime.Value = dateTimePickerStartTime.Value.AddDays(-days);
            dateTimePickerEndTime.Value = dateTimePickerEndTime.Value.AddDays(-days);
        }

        private void buttonAddDays_Click(object sender, EventArgs e)
        {
            var days = Convert.ToInt32(textBoxCountChangeDays.Text);

            dateTimePickerStartTime.Value = dateTimePickerStartTime.Value.AddDays(days);
            dateTimePickerEndTime.Value = dateTimePickerEndTime.Value.AddDays(days);
        }
    }
}
