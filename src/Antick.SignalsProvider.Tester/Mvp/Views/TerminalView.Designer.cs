﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Antick.SignalsProvider.Tester.Mvp.Views
{
    partial class TerminalView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SettingstoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxCountChangeDays = new System.Windows.Forms.TextBox();
            this.buttonSubDays = new System.Windows.Forms.Button();
            this.buttonAddDays = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerEndTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTimeFrameValue = new System.Windows.Forms.TextBox();
            this.checkBoxSecodAreaVisible = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.textBoxPk = new System.Windows.Forms.TextBox();
            this.textBoxSk = new System.Windows.Forms.TextBox();
            this.textBoxPd = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelSpendTimeBack = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelSpendTimeFront = new System.Windows.Forms.Label();
            this.panelCharts = new System.Windows.Forms.Panel();
            this.panelChartDetails = new System.Windows.Forms.Panel();
            this.labelCurrentTime = new System.Windows.Forms.Label();
            this.labelCandleData = new System.Windows.Forms.Label();
            this.labelIndicatorData = new System.Windows.Forms.Label();
            this.panelUpper = new System.Windows.Forms.Panel();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelCharts.SuspendLayout();
            this.panelChartDetails.SuspendLayout();
            this.panelUpper.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(3, 11);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(200, 64);
            this.buttonRefresh.TabIndex = 69;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.SettingstoolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1382, 28);
            this.menuStrip1.TabIndex = 79;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(45, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // SettingstoolStripMenuItem
            // 
            this.SettingstoolStripMenuItem.Name = "SettingstoolStripMenuItem";
            this.SettingstoolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.SettingstoolStripMenuItem.Text = "Settings";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxCountChangeDays);
            this.groupBox1.Controls.Add(this.buttonSubDays);
            this.groupBox1.Controls.Add(this.buttonAddDays);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateTimePickerEndTime);
            this.groupBox1.Controls.Add(this.dateTimePickerStartTime);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxTimeFrameValue);
            this.groupBox1.Location = new System.Drawing.Point(17, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(773, 83);
            this.groupBox1.TabIndex = 84;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // textBoxCountChangeDays
            // 
            this.textBoxCountChangeDays.Location = new System.Drawing.Point(479, 58);
            this.textBoxCountChangeDays.Name = "textBoxCountChangeDays";
            this.textBoxCountChangeDays.Size = new System.Drawing.Size(59, 22);
            this.textBoxCountChangeDays.TabIndex = 97;
            this.textBoxCountChangeDays.Text = "2";
            // 
            // buttonSubDays
            // 
            this.buttonSubDays.Location = new System.Drawing.Point(398, 58);
            this.buttonSubDays.Name = "buttonSubDays";
            this.buttonSubDays.Size = new System.Drawing.Size(75, 23);
            this.buttonSubDays.TabIndex = 96;
            this.buttonSubDays.Text = "-";
            this.buttonSubDays.UseVisualStyleBackColor = true;
            this.buttonSubDays.Click += new System.EventHandler(this.buttonSubDays_Click);
            // 
            // buttonAddDays
            // 
            this.buttonAddDays.Location = new System.Drawing.Point(555, 57);
            this.buttonAddDays.Name = "buttonAddDays";
            this.buttonAddDays.Size = new System.Drawing.Size(75, 23);
            this.buttonAddDays.TabIndex = 95;
            this.buttonAddDays.Text = "+";
            this.buttonAddDays.UseVisualStyleBackColor = true;
            this.buttonAddDays.Click += new System.EventHandler(this.buttonAddDays_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(566, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "EndTime";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "StartTime";
            // 
            // dateTimePickerEndTime
            // 
            this.dateTimePickerEndTime.Location = new System.Drawing.Point(518, 32);
            this.dateTimePickerEndTime.Name = "dateTimePickerEndTime";
            this.dateTimePickerEndTime.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerEndTime.TabIndex = 3;
            this.dateTimePickerEndTime.ValueChanged += new System.EventHandler(this.dateTimePickerEndTime_ValueChanged);
            // 
            // dateTimePickerStartTime
            // 
            this.dateTimePickerStartTime.Location = new System.Drawing.Point(295, 32);
            this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
            this.dateTimePickerStartTime.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerStartTime.TabIndex = 3;
            this.dateTimePickerStartTime.ValueChanged += new System.EventHandler(this.dateTimePickerStartTime_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "RANGE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "TimeFrameType";
            this.label2.Click += new System.EventHandler(this.label1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(175, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "TimeFrameValue";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxTimeFrameValue
            // 
            this.textBoxTimeFrameValue.Location = new System.Drawing.Point(178, 30);
            this.textBoxTimeFrameValue.Name = "textBoxTimeFrameValue";
            this.textBoxTimeFrameValue.Size = new System.Drawing.Size(100, 22);
            this.textBoxTimeFrameValue.TabIndex = 0;
            this.textBoxTimeFrameValue.TextChanged += new System.EventHandler(this.textBoxTimeFrameValue_TextChanged);
            // 
            // checkBoxSecodAreaVisible
            // 
            this.checkBoxSecodAreaVisible.AutoSize = true;
            this.checkBoxSecodAreaVisible.Checked = true;
            this.checkBoxSecodAreaVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSecodAreaVisible.Location = new System.Drawing.Point(20, 250);
            this.checkBoxSecodAreaVisible.Name = "checkBoxSecodAreaVisible";
            this.checkBoxSecodAreaVisible.Size = new System.Drawing.Size(183, 21);
            this.checkBoxSecodAreaVisible.TabIndex = 85;
            this.checkBoxSecodAreaVisible.Text = "Видимость индикатора";
            this.checkBoxSecodAreaVisible.UseVisualStyleBackColor = true;
            this.checkBoxSecodAreaVisible.CheckedChanged += new System.EventHandler(this.checkBoxSecodAreaVisible_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(20, 288);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(68, 21);
            this.checkBox2.TabIndex = 86;
            this.checkBox2.Text = "Area3";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // textBoxPk
            // 
            this.textBoxPk.Location = new System.Drawing.Point(15, 101);
            this.textBoxPk.Name = "textBoxPk";
            this.textBoxPk.Size = new System.Drawing.Size(100, 22);
            this.textBoxPk.TabIndex = 87;
            this.textBoxPk.Text = "20";
            this.textBoxPk.TextChanged += new System.EventHandler(this.textBoxPk_TextChanged);
            // 
            // textBoxSk
            // 
            this.textBoxSk.Location = new System.Drawing.Point(15, 146);
            this.textBoxSk.Name = "textBoxSk";
            this.textBoxSk.Size = new System.Drawing.Size(100, 22);
            this.textBoxSk.TabIndex = 87;
            this.textBoxSk.Text = "2";
            this.textBoxSk.TextChanged += new System.EventHandler(this.textBoxSk_TextChanged);
            // 
            // textBoxPd
            // 
            this.textBoxPd.Location = new System.Drawing.Point(15, 188);
            this.textBoxPd.Name = "textBoxPd";
            this.textBoxPd.Size = new System.Drawing.Size(100, 22);
            this.textBoxPd.TabIndex = 87;
            this.textBoxPd.Text = "3";
            this.textBoxPd.TextChanged += new System.EventHandler(this.textBoxPd_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 17);
            this.label6.TabIndex = 88;
            this.label6.Text = "Pk";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 17);
            this.label7.TabIndex = 89;
            this.label7.Text = "Sk";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 17);
            this.label8.TabIndex = 90;
            this.label8.Text = "Pd";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(800, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 17);
            this.label9.TabIndex = 92;
            this.label9.Text = "SpendTimeBack";
            // 
            // labelSpendTimeBack
            // 
            this.labelSpendTimeBack.AutoSize = true;
            this.labelSpendTimeBack.Location = new System.Drawing.Point(800, 51);
            this.labelSpendTimeBack.Name = "labelSpendTimeBack";
            this.labelSpendTimeBack.Size = new System.Drawing.Size(28, 17);
            this.labelSpendTimeBack.TabIndex = 92;
            this.labelSpendTimeBack.Text = "0.0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(918, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 17);
            this.label10.TabIndex = 92;
            this.label10.Text = "SpendTimeFront";
            // 
            // labelSpendTimeFront
            // 
            this.labelSpendTimeFront.AutoSize = true;
            this.labelSpendTimeFront.Location = new System.Drawing.Point(918, 56);
            this.labelSpendTimeFront.Name = "labelSpendTimeFront";
            this.labelSpendTimeFront.Size = new System.Drawing.Size(28, 17);
            this.labelSpendTimeFront.TabIndex = 92;
            this.labelSpendTimeFront.Text = "0.0";
            // 
            // panelCharts
            // 
            this.panelCharts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelCharts.Controls.Add(this.panelChartDetails);
            this.panelCharts.Location = new System.Drawing.Point(220, 151);
            this.panelCharts.Name = "panelCharts";
            this.panelCharts.Size = new System.Drawing.Size(1126, 542);
            this.panelCharts.TabIndex = 93;
            // 
            // panelChartDetails
            // 
            this.panelChartDetails.Controls.Add(this.labelCurrentTime);
            this.panelChartDetails.Controls.Add(this.labelCandleData);
            this.panelChartDetails.Controls.Add(this.labelIndicatorData);
            this.panelChartDetails.Location = new System.Drawing.Point(5, 398);
            this.panelChartDetails.Name = "panelChartDetails";
            this.panelChartDetails.Size = new System.Drawing.Size(1116, 50);
            this.panelChartDetails.TabIndex = 85;
            // 
            // labelCurrentTime
            // 
            this.labelCurrentTime.AutoSize = true;
            this.labelCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCurrentTime.Location = new System.Drawing.Point(10, 13);
            this.labelCurrentTime.Name = "labelCurrentTime";
            this.labelCurrentTime.Size = new System.Drawing.Size(89, 20);
            this.labelCurrentTime.TabIndex = 72;
            this.labelCurrentTime.Text = "10.10.2016";
            // 
            // labelCandleData
            // 
            this.labelCandleData.AutoSize = true;
            this.labelCandleData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCandleData.Location = new System.Drawing.Point(232, 13);
            this.labelCandleData.Name = "labelCandleData";
            this.labelCandleData.Size = new System.Drawing.Size(97, 20);
            this.labelCandleData.TabIndex = 73;
            this.labelCandleData.Text = "CandleData";
            // 
            // labelIndicatorData
            // 
            this.labelIndicatorData.AutoSize = true;
            this.labelIndicatorData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIndicatorData.Location = new System.Drawing.Point(810, 13);
            this.labelIndicatorData.Name = "labelIndicatorData";
            this.labelIndicatorData.Size = new System.Drawing.Size(109, 20);
            this.labelIndicatorData.TabIndex = 74;
            this.labelIndicatorData.Text = "IndicatorData";
            // 
            // panelUpper
            // 
            this.panelUpper.Controls.Add(this.groupBox1);
            this.panelUpper.Controls.Add(this.labelSpendTimeFront);
            this.panelUpper.Controls.Add(this.labelSpendTimeBack);
            this.panelUpper.Controls.Add(this.label10);
            this.panelUpper.Controls.Add(this.label9);
            this.panelUpper.Location = new System.Drawing.Point(220, 40);
            this.panelUpper.Name = "panelUpper";
            this.panelUpper.Size = new System.Drawing.Size(1126, 105);
            this.panelUpper.TabIndex = 94;
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.textBoxPk);
            this.panelLeft.Controls.Add(this.textBoxSk);
            this.panelLeft.Controls.Add(this.textBoxPd);
            this.panelLeft.Controls.Add(this.buttonRefresh);
            this.panelLeft.Controls.Add(this.checkBox2);
            this.panelLeft.Controls.Add(this.label8);
            this.panelLeft.Controls.Add(this.checkBoxSecodAreaVisible);
            this.panelLeft.Controls.Add(this.label6);
            this.panelLeft.Controls.Add(this.label7);
            this.panelLeft.Location = new System.Drawing.Point(0, 40);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(220, 650);
            this.panelLeft.TabIndex = 95;
            // 
            // TerminalView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 721);
            this.Controls.Add(this.panelCharts);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelUpper);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TerminalView";
            this.Text = "[SharkFx.ru] MfbTool v. 1.0.0.0";
            this.Load += new System.EventHandler(this.mfbToolsForm_Load);
            this.SizeChanged += new System.EventHandler(this.MfbToolsForm_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TerminalView_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelCharts.ResumeLayout(false);
            this.panelChartDetails.ResumeLayout(false);
            this.panelChartDetails.PerformLayout();
            this.panelUpper.ResumeLayout(false);
            this.panelUpper.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button buttonRefresh;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem SettingstoolStripMenuItem;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private GroupBox groupBox1;
        private Label label1;
        private TextBox textBoxTimeFrameValue;
        private Label label5;
        private Label label4;
        private DateTimePicker dateTimePickerEndTime;
        private DateTimePicker dateTimePickerStartTime;
        private Label label3;
        private Label label2;
        private CheckBox checkBoxSecodAreaVisible;
        private CheckBox checkBox2;
        private TextBox textBoxPk;
        private TextBox textBoxSk;
        private TextBox textBoxPd;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label labelSpendTimeBack;
        private Label label10;
        private Label labelSpendTimeFront;
        private Panel panelCharts;
        private Panel panelUpper;
        private Panel panelLeft;
        private Panel panelChartDetails;
        private Label labelCurrentTime;
        private Label labelCandleData;
        private Label labelIndicatorData;
        private TextBox textBoxCountChangeDays;
        private Button buttonSubDays;
        private Button buttonAddDays;
    }
}