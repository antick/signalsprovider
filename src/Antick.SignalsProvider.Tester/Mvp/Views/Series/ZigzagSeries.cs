﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;
using Antick.SignalsProvider.Tester.Components.Indicators;
using Antick.SignalsProvider.Tester.Contracts.Render;
using Antick.SignalsProvider.Tester.Utils;

namespace Antick.SignalsProvider.Tester.Mvp.Views.Series
{
    public class ZigzagSeries
    {
        //Используется для прорировки движений
        private readonly LnKoef m_LineKoef;

        /// <summary>
        /// индекс последнего отрисованного движения
        /// </summary>
        protected long Index = 0;

        /// <summary>
        /// Индекс бара, с которого начинается рисование последнго отрезка зигзага
        /// </summary>
        protected long StartIndex = 0;

        /// <summary>
        /// Индекс бара, на котором закончилось рисование последнго отрезка зигзага
        /// </summary>
        protected long EndIndex = 0;


        private IZigzag m_Zigzag;

        protected List<Zigzag> ZigzagList { get; set; }

        private DataPoint[] m_Result;
        private RenderCandle[] m_Candles;

        public ZigzagSeries()
        {
            m_LineKoef = new LnKoef();
        }

        public DataPoint[] Calculate(RenderCandle[] candles,
            List<Zigzag> movements)
        {
            m_Result = new DataPoint[candles.Length];
            m_Candles = candles;
            ZigzagList = movements;

            m_Zigzag = ZigzagList.FirstOrDefault(p => p.Index == Index);
            while (m_Zigzag.EndBar.Index < candles[0].Index)
            {
                Index++;
                m_Zigzag = ZigzagList.FirstOrDefault(p => p.Index == Index);
            }

            for (int i = 0;  i < candles.Length; i++)
            {
                RenderPoint(i);
            }

            return m_Result;
        }

        /// <summary>
        /// Рисует итерацию зигзага
        /// </summary>
        public void RenderPoint(int i)
        {
            if (!ZigzagList.Any())
            {
                RenderStub(i);
                return;
            }

            var bar = m_Candles[i];
            var barIndex = bar.Index;
            var time = bar.Time;

            m_Zigzag = ZigzagList.FirstOrDefault(p => p.Index == Index);
           
            if (Index > ZigzagList.Last().Index)
            {
                RenderStub(i);
                return;
            }

          
            if (barIndex == m_Zigzag.StartBar.Index)
            {
                Point(i, m_Zigzag.StartBar.Time,
                    m_Zigzag.StartPrice);

                StartIndex = barIndex;
            }
            else if (barIndex > m_Zigzag.StartBar.Index &&
                     barIndex < m_Zigzag.EndBar.Index)
            {
                m_LineKoef.Calculate(m_Zigzag);

                long x1 = barIndex;
                double y1 = m_LineKoef.Y(x1);
                Point(i, bar.Time, y1);
            }
            else if (bar.Index == m_Zigzag.EndBar.Index)
            {
                // Итарация случится только в самом конце
                Point(i, m_Zigzag.EndBar.Time,
                    m_Zigzag.EndPrice);
                EndIndex = barIndex;

                Index++;
            }
            else
            {
                RenderStub(i);
            }

        }

        public void Clear()
        {
            m_LineKoef.Clear();
            Index = 0;
            StartIndex = 0;
            EndIndex = 0;
        }

        public void RenderStub(int i)
        {
            m_Result[i] = new DataPoint(m_Candles[i].Time.ToOADate(), 0) {IsEmpty = true};
        }

        public void Point(int i, DateTime time, double value)
        {
            m_Result[i] = new DataPoint(time.ToOADate(), value);
        }
    }
}
