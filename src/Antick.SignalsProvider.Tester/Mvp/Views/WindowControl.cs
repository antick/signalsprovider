﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Antick.Infractructure.Extensions;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Tester.Contracts;
using Antick.SignalsProvider.Tester.Contracts.Args;
using Antick.SignalsProvider.Tester.Contracts.Render;
using Antick.SignalsProvider.Tester.Mvp.Views.Series;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Tester.Mvp.Views
{
    public partial class WindowControl : UserControl
    {
        public event EventHandler<CursorDataCatchingEventArgs> CursorDataCatching;
        public event EventHandler<AreaSelectionChangedEventArgs> AreaSelectionChanged; 

        public bool IsAreaSelectionEnabled = true;

        private bool m_IsSecondAreaVisible = true;
        public bool IsSecondAreaVisible
        {
            get => m_IsSecondAreaVisible;
            set
            {
                m_IsSecondAreaVisible = value;
                chart.ChartAreas[1].Visible = m_IsSecondAreaVisible;
            }
        }

        private readonly ILogger m_Logger;


        private int curPointIndex = 0;

        private RenderData m_RenderData;

        private int? m_FirstStripLineIndex = null;
        private int? m_SecondStripLineIndex = null;
        
        public WindowControl(ILogger logger)
        {
            m_Logger = logger;
            InitializeComponent();

            chart.MouseClick += chart_MouseClick;

        }


        private void chart_MouseClick(object sender, MouseEventArgs e)
        {
            addNewStripLine();
        }

        private void addNewStripLine()
        {
            this.InvokeAsync((c) =>
            {
                if (!IsAreaSelectionEnabled)
                    return;

                var newStripLineIndex = curPointIndex;
                if (!m_FirstStripLineIndex.HasValue)
                    m_FirstStripLineIndex = newStripLineIndex;
                else if (!m_SecondStripLineIndex.HasValue)
                {
                    m_SecondStripLineIndex = newStripLineIndex;
                }
                else
                {
                    m_FirstStripLineIndex = m_SecondStripLineIndex;
                    m_SecondStripLineIndex = newStripLineIndex;
                }

                chart.ChartAreas[0].AxisX.StripLines
                    .Add(new StripLine
                    {
                        BorderColor = Color.Orange,
                        BorderDashStyle = ChartDashStyle.Solid,
                        BorderWidth = 2,
                        IntervalOffset = curPointIndex
                    });

                if (chart.ChartAreas[0].AxisX.StripLines.Count > 2)
                {
                    chart.ChartAreas[0].AxisX.StripLines.RemoveAt(0);
                }

                AreaSelectionChanged.Raise(this, new AreaSelectionChangedEventArgs
                {
                    AreaSelection = new AreaSelection
                    {
                        StartTime = m_RenderData.Candles[m_FirstStripLineIndex.Value].Time,
                        EndTime = m_SecondStripLineIndex != null
                            ? m_RenderData.Candles[m_SecondStripLineIndex.Value].Time
                            : (DateTime?) null
                    }
                });
            });
        }

        public void RenderData(RenderData renderData)
        {
            try
            {
                labelTitle.Text = $"{renderData.TimeFrameValue} Renko";

                m_RenderData = renderData;

                foreach (var s in chart.Series)
                {
                    s.Points.Clear();
                }

                if (renderData.Candles == null)
                    return;

                var count = renderData.Candles.Count;

                chart.ChartAreas[0].AxisX.ScaleView.Size = count + 1;
                chart.ChartAreas[1].AxisX.ScaleView.Size = count + 1;
                chart.ChartAreas[0].AxisX.ScaleView.Position = 0;
                chart.ChartAreas[0].AxisY.LabelStyle.Format = renderData.Spec.Format;

                var movementData = new ZigzagSeries().Calculate(renderData.Candles.ToArray(), renderData.Movements);

                #region Draw Bars

                var minPrice = renderData.Candles.Min(p => p.Low);
                var maxPrice = renderData.Candles.Min(p => p.High);

                for (int index = 0; index < renderData.Candles.Count; index++)
                {
                    var data = renderData.Candles[index];

                    chart.Series["BarsStub"].Points.Add(new DataPoint(data.Time.ToOADate(), 1000));
                    chart.Series["Bars"].Points.Add(new DataPoint(data.Time.ToOADate(),
                        new[] {data.High, data.Low, data.Open, data.Close}));

                    //chart.Series["MAFast"].Points.Add(renderData.MaFastValues[index] > 0
                    //    ? new DataPoint(data.Time.ToOADate(), renderData.Tmp[index])
                    //    : new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });

                    //chart.Series["MAVerySlow"].Points.Add(renderData.MaFastValues[index] > 0
                    //    ? new DataPoint(data.Time.ToOADate(), renderData.Tmp2[index])
                    //    : new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });

                    chart.Series["MAFast"].Points.Add(renderData.MaFastValues[index] > 0
                        ? new DataPoint(data.Time.ToOADate(), renderData.MaFastValues[index])
                        : new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });

                    chart.Series["MASlow"].Points.Add(renderData.MaSlowValues[index] > 0
                        ? new DataPoint(data.Time.ToOADate(), renderData.MaSlowValues[index])
                        : new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });

                    chart.Series["MAVerySlow"].Points.Add(renderData.MaVerySlowValues[index] > 0
                        ? new DataPoint(data.Time.ToOADate(), renderData.MaVerySlowValues[index])
                        : new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });

                    chart.Series["StochSygnal"].Points.Add(new DataPoint(data.Time.ToOADate(), renderData.StockSignal[index]));
                    chart.Series["StochValue"].Points.Add(new DataPoint(data.Time.ToOADate(), renderData.StockValue[index]));

                    if (renderData.StochasticZone[index] == StochasticZoneType.None)
                    {
                        chart.Series["StochasticZone"].Points.Add(new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });
                        chart.Series["MovementChange"].Points.Add(new DataPoint(data.Time.ToOADate(), 0) { IsEmpty = true });
                    }
                    else
                    {
                        double val = 0;

                        if (renderData.StochasticZone[index] == StochasticZoneType.U50ToU80 ||
                            renderData.StochasticZone[index] == StochasticZoneType.UnknowUp)
                        {
                            val = new[] { renderData.StockValue[index], renderData.StockSignal[index] }.Min();
                        }
                        else
                        {
                            val = new[] { renderData.StockValue[index], renderData.StockSignal[index] }.Max();
                        }

                        chart.Series["StochasticZone"].Points
                            .Add(new DataPoint(data.Time.ToOADate(), val));

                        chart.Series["MovementChange"].Points
                            .Add(new DataPoint(data.Time.ToOADate(), minPrice));


                    }


                    //chart.Series["Ao"].Points.Add(new DataPoint(data.Time.ToOADate(), renderData.TimeData[index]));


                    chart.Series["Movement"].Points
                        .Add(new DataPoint(data.Time.ToOADate(), movementData[index].YValues)
                        {
                            IsEmpty = movementData[index].IsEmpty
                        });
                }
                //chart.ChartAreas[1].Visible = checkBox1.Checked;
                //chart.ChartAreas[2].Visible = checkBox2.Checked;


                //chart1.ChartAreas[1].Visible = false;



                #endregion

                var max = new[] {renderData.Candles.Max(p => p.High)}.Max();
                var min = new[] {renderData.Candles.Min(p => p.Low)}.Min();

                setMinMaxForArea(0, min, max);

                onCursorDataCatching(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка рендеринга WindowControl");
            }
        }

        private void setMinMaxForArea(int areaIndex, double min, double max)
        {
            if (min == 0)
                min = double.NaN;
            if (max == 0)
                max = double.NaN;

            var _5P = (max - min) * 0.10;
            chart.ChartAreas[areaIndex].AxisY.Minimum = min - _5P;
            chart.ChartAreas[areaIndex].AxisY.Maximum = max + _5P;
        }

        private void chart_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                var hitTestResult = chart.HitTest(e.Location.X, e.Location.Y, ChartElementType.DataPoint);
                if (hitTestResult != null && hitTestResult.ChartElementType != ChartElementType.Nothing)
                {
                    var point = hitTestResult.PointIndex;
                    curPointIndex = point;
                    onCursorDataCatching(point);
                }
            }
            catch (Exception ex)
            {
               // m_Logger.ErrorFormat("windowType[{0}]: Error on mouseMove. DetailError: {1}", m_WindowType, ex.ToString());
            }
        }

        private void onCursorDataCatching(int pointIndex)
        {
            this.InvokeAsync(c =>
            {
                try
                {
                    chart.ChartAreas[0].CursorX.Position = pointIndex + 1;

                    var candle = m_RenderData.Candles[pointIndex];

                    var chartData =
                        $"O:{candle.Open:0.00000},C:{candle.Close:0.00000},H:{candle.High:0.00000},L:{candle.Low:0.00000}, I:{candle.Index}; MaF: {m_RenderData.MaFastValues[pointIndex]}";

                    var indicatorData =
                        $"StockValue: {m_RenderData.StockValue[pointIndex]:0.00}; StochSignal: {m_RenderData.StockSignal[pointIndex]:0.00}";

                    CursorDataCatching.Raise(this, new CursorDataCatchingEventArgs
                    {
                        Date = candle.Time,
                        ChartData = chartData,
                        IndicatorData = indicatorData
                    });
                }
                catch
                {
                }
            });
        }

        private void WindowControl_SizeChanged(object sender, EventArgs e)
        {
            chart.Width = this.Size.Width;
            chart.Height = this.Size.Height;
            labelTitle.Left = this.Size.Width / 2;
        }
    }
}
