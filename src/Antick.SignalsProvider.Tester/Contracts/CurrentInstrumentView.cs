﻿using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Tester.Contracts
{
    public class CurrentInstrumentView
    {
        public Instrument Instrument { get; set; }

        public TimeFrame TimeFrame { get; set; }
        
        public int? CountCandles { get; set; }
    }
}
