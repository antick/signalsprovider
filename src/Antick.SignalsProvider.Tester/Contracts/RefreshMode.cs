﻿namespace Antick.SignalsProvider.Tester.Contracts
{
    public enum RefreshMode
    {
        All,
        Current
    }
}
