﻿using System;
using System.Collections.Generic;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Tester.Components.Indicators;

namespace Antick.SignalsProvider.Tester.Contracts.Render
{
    public class RenderData
    {
        public List<RenderCandle> Candles { get; set; }

        public List<double> MaFastValues { get; set; }

        public List<double> MaSlowValues { get; set; }

        public List<double> MaVerySlowValues { get; set; }
        
        public List<double> StockValue { get; set; }

        public List<double> StockSignal { get; set; }

        public List<double> ParabolicValues { get; set; }

        public List<StochasticZoneType> StochasticZone { get; set; }

        public List<double> TimeSpend { get; set; }

        public List<Zigzag> Movements { get; set; }

        public Utils.InstrumentSpec Spec { get; set; }

        public string IndicatorDataFormat { get; set; }


        public List<double> Tmp { get; set; }
        public List<double> Tmp2 { get; set; }

        public TimeSpan GeneratedTotalTime { get; set; }


        /// Данные отрисовки входных данных
        public int TimeFrameValue { get; set; }
        public int Pk { get; set; }
        public int Sk { get; set; }
        public int Pd { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
