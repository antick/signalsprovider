﻿namespace Antick.SignalsProvider.Tester.Contracts.Render
{
    public class RenderIndicator
    {
        public double Buy { get; set; }

        public double Sell { get; set; }
    }
}
