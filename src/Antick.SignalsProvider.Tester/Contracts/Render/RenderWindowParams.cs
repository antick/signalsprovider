﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Render
{
    public class RenderWindowParams
    {
        /// Данные отрисовки входных данных
        public int TimeFrameValue { get; set; }
        public int Pk { get; set; }
        public int Sk { get; set; }
        public int Pd { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
