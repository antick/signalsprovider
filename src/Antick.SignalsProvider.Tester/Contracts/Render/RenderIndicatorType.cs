﻿namespace Antick.SignalsProvider.Tester.Contracts.Render
{
    public enum RenderIndicatorType
    {
        Line,
        Line2,
        Delta,
        Delta2,
    }
}
