﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.Tester.Contracts
{
    public enum IndicatorType
    {
        None,
        StochasticWithZone,
        MovementTime
    }
}
