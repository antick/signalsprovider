﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts
{
    public class AreaSelection
    {
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
