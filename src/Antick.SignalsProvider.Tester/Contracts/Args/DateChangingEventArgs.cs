﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class DateChangingEventArgs : EventArgs
    {
        public DateTime Time { get; set; }
    }
}
