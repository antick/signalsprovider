﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class CurrentWindowChangedEventArgs : EventArgs
    {
        public WindowType Window { get; set; }
    }
}
