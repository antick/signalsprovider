﻿using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class InstrumentChangingEventArgs : System.EventArgs
    {
        public Instrument Instrument { get; set; }
    }
}
