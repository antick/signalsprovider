﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class CursorDataCatchingEventArgs : EventArgs
    {
        public DateTime Date { get; set; }
        public string ChartData { get; set; }
        public string IndicatorData { get; set; }
    }
}
