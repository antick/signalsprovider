﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class AreaSelectionChangedEventArgs : EventArgs
    {
        public AreaSelection AreaSelection { get; set; }
    }
}
