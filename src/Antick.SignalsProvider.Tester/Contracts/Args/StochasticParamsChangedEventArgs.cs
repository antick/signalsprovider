﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class StochasticParamsChangedEventArgs : EventArgs
    {
        public int? Pk { get; set; }
        public int? Sk { get; set; }
        public int? Pd { get; set; }
    }
}
