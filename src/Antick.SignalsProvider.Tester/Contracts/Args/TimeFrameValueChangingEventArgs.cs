﻿namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class TimeFrameValueChangingEventArgs : System.EventArgs
    {
        public int Value { get; set; }
    }
}
