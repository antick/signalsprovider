﻿using System;

namespace Antick.SignalsProvider.Tester.Contracts.Args
{
    public class TimeFrameValueLowerChangedEventArgs : EventArgs
    {
        public int? Value { get; set; }
    }
}
