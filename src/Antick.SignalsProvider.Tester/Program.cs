﻿using System;
using Antick.SignalsProvider.Tester.Mvp.Presenters;
using Castle.Windsor;

namespace Antick.SignalsProvider.Tester
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var windsorContainer = new WindsorContainer();
            windsorContainer.Install(new WindsorInstaller());

           windsorContainer.Resolve<TerminalPresenter>().Run();
        }
    }
}
