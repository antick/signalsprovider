﻿using Antick.SignalsProvider.Tester.Contracts.Render;

namespace Antick.SignalsProvider.Tester.Components.Indicators
{
    public interface IZigzag
    {
        /// <summary>
        /// Начальный бар для отрезка
        /// </summary>
        RenderCandle StartBar { get; set; }

        /// <summary>
        /// Конечный бар отрезка
        /// </summary>
        RenderCandle EndBar { get; set; }

        /// <summary>
        /// Цена начала отрезка
        /// </summary>
        double StartPrice { get; set; }

        /// <summary>
        /// Цена конча отрезка
        /// </summary>
        double EndPrice { get; set; }

        /// <summary>
        /// Индекс движения, волны или тренда
        /// </summary>
        long Index { get; set; }
    }
}
