﻿using Antick.SignalsProvider.Tester.Contracts.Render;

namespace Antick.SignalsProvider.Tester.Components.Indicators
{
    public class Zigzag : IZigzag
    {
        public RenderCandle StartBar { get; set; }
        public RenderCandle EndBar { get; set; }
        public double StartPrice { get; set; }
        public double EndPrice { get; set; }
        public long Index { get; set; }
    }
}
