﻿using System;
using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Tester.Utils
{
    /// <summary>
    /// Спецификация по каждому инструменту в контексте книги Оанды
    /// </summary>
    public class InstrumentSpec
    {
        /// <summary>
        /// Формат вывода котировки, для мажоров это 4 знака
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// Стандартный шаг цены в книге заказов оанды
        /// </summary>
        public double Step { get; set; }

        /// <summary>
        /// Шаг сетки по инструменту
        /// </summary>
        public double StepInterval { get; set; }

        public int Digits { get; set; }

        public static InstrumentSpec Get(Instrument instrument)
        {
            return Get(instrument.ToString());
        }

        public static InstrumentSpec Get(string instrument)
        {
            switch (instrument)
            {
                case "GBP_USD":
                case "EUR_USD":
                case "AUD_USD":
                case "NZD_USD":
                case "USD_CAD":
                case "USD_CHF":
                case "EUR_AUD":
                case "EUR_CHF":
                case "EUR_GBP":
                case "GBP_CHF":
                    return new InstrumentSpec
                    {
                        Format = "0.0000",
                        Step = 0.0005,
                        StepInterval = 0.0050,
                        Digits = 4,
                    };
                case "USD_JPY":
                case "EUR_JPY":
                case "GBP_JPY":
                case "AUD_JPY":
                    return new InstrumentSpec
                    {
                        Format = "0.00",
                        Step = 0.05,
                        StepInterval = 0.50,
                        Digits = 2
                    };
                case "XAU_USD":
                    return new InstrumentSpec
                    {
                        Format = "0.0",
                        Step = 0.5,
                        StepInterval = 5.00,
                        Digits = 1
                    };
                case "XAG_USD":
                    return new InstrumentSpec
                    {
                        Format = "0.0",
                        Step = 1,
                        StepInterval = 10.00,
                        Digits = 1
                    };
                default: throw new Exception("Неизвестный символ");
            }
        }
    }
}
