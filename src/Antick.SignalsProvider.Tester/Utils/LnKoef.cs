﻿using Antick.SignalsProvider.Tester.Components.Indicators;

namespace Antick.SignalsProvider.Tester.Utils
{
    public class LnKoef
    {
        private double m_K;
        private double m_B;

        public void Calculate(double y1, long x1, double y2, long x2)
        {
            m_K = (y2 - y1) / (x2 - x1);
            m_B = y1 - m_K * x1;
        }

        /// <summary>
        /// Строит по зигзагу
        /// </summary>
        /// <param name="zigzag"></param>
        /// <returns></returns>
        public void Calculate(IZigzag zigzag)
        {
            Calculate(zigzag.StartPrice, zigzag.StartBar.Index, zigzag.EndPrice, zigzag.EndBar.Index);
        }

        public void Clear()
        {
            m_K = 0;
            m_B = 0;
        }

        public bool IsEmpty()
        {
            return m_K == 0 && m_B == 0;
        }

        public double Y(long x)
        {
            return m_K * x + m_B;
        }
    }
}
