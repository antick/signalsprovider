﻿using System;

namespace Antick.SignalsProvider.Tester.Utils
{
    public static class DateUtils
    {
        /// <summary>
        /// Начало последнего рабочего дня
        /// </summary>
        /// <returns></returns>
        public static DateTime WorkDayStart()
        {
            var day = DateTime.Now;
            var d = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0);
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                d = d.AddDays(-1);
            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                d = d.AddDays(-2);
            }
            return d;
        }

        /// <summary>
        /// Начало рабочего дня в формате UTF, Это 3.00 по МСК !!!
        /// </summary>
        public static DateTime WorkDayStartUtf()
        {
            return DateTime.SpecifyKind(WorkDayStart(), DateTimeKind.Utc);
        }

        public static DateTime Start(DateTime time)
        {
            return new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
        }

        public static DateTime Start()
        {
            return Start(DateTime.Now);
        }

        public static DateTime End(DateTime time)
        {
            return new DateTime(time.Year, time.Month, time.Day, 23, 59, 59);
        }

        public static DateTime End()
        {
            return End(DateTime.Now);
        }

        /// <summary>
        /// Вычитает из указанной даты ровное колличество рабочих дней.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="appendDays"></param>
        /// <returns></returns>
        public static DateTime SubWorkDays(DateTime t, int appendDays)
        {
            DateTime time = t;

            var tmpDays = appendDays;
            while (tmpDays > 0)
            {
                time = time.AddDays(-1);
                if (!(time.DayOfWeek == DayOfWeek.Saturday || time.DayOfWeek == DayOfWeek.Sunday))
                {
                    tmpDays--;
                }
                else
                {
                    if (time.DayOfWeek == DayOfWeek.Sunday)
                    {
                        time = time.AddDays(-2);
                        tmpDays--;
                    }
                    else
                    {
                        time = time.AddDays(-1);
                        tmpDays--;
                    }
                }
            }
            return time;
        }

        /// <summary>
        /// Считает колличество рабочих дней
        /// </summary>
        /// <param name="end"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        public static int CountWorkDays(DateTime end, DateTime start)
        {
            DateTime time = end;

            int countDays = 0;

            while (true)
            {
                if (time.Year == start.Year && time.DayOfYear + 1 == start.DayOfYear)
                    break;

                time = time.AddDays(-1);
                if (!(time.DayOfWeek == DayOfWeek.Saturday || time.DayOfWeek == DayOfWeek.Sunday))
                {
                    countDays++;
                }
            }

            return countDays;
        }

        public static DateTime Concat(this DateTime date, TimeSpan span)
        {
            return new DateTime(date.Year, date.Month, date.Day, span.Hours, span.Minutes, span.Seconds);
        }

        public static DateTime Concat(this DateTime date, TimeSpan? span)
        {
            return date.Concat(span.Value);
        }

        public static string ToStr(this TimeSpan span)
        {
            return string.Format("{0:00}:{1:00}", span.Hours, span.Minutes);
        }

        public static string ToStr(this TimeSpan? span)
        {
            return span.Value.ToStr();
        }
    }
}
