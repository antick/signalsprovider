﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Tester.Mvp.Views;

namespace Antick.SignalsProvider.Tester
{
    public interface IWindowControlFactory
    {
        WindowControl Create();
        void Release(WindowControl component);
    }
}
