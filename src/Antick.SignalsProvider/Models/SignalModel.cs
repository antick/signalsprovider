﻿using System;

namespace Antick.SignalsProvider.Models
{
    internal class SignalModel
    {
        public long Id { get; set; }

        public DateTime CreatedDate { get; set; }

        //public TicketTypeEnum TicketTypeId { get; set; }

        public double Price { get; set; }

        public double TakeProfit { get; set; }

        public double StopLoss { get; set; }

        public bool IsActive { get; set; }

        public DmsEnum Dms { get; set; }

        public SignalStatusEnum Status { get; set; }

        public FmaSarSignalType? FmaSarSignalType { get; set; }

        public long? ExecutedBarId { get; set; }

        public long InitBarId { get; set; }
    }
}
