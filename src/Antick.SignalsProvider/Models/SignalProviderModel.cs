﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Domain;
using Antick.SignalsProvider.Db;

namespace Antick.SignalsProvider.Models
{
    /// <summary>
    /// Общая модель поставщика сигналов
    /// </summary>
    public class SignalProviderModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Имя провайдера
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип провайдера
        /// </summary>
        public string Type { get; set; }

        public InstrumentComplex Instrument { get; set; }

        public string Parameters { get; set; }

        /// <summary>
        /// Включенность поставщика
        /// </summary>
        public bool Enabled { get;  set; }

        public SignalProviderModel(int id, string name, string type, InstrumentComplex instrument, string parameters, bool enabled)
        {
            Id = id;
            Name = name;
            Type = type;
            Instrument = instrument;
            Parameters = parameters;
            Enabled = enabled;
        }
    }
}
