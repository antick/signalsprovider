﻿namespace Antick.SignalsProvider.Models
{
    public enum SignalStatusEnum
    {
        // ПО ТС-ФМА Сар:
        /* изначально сигнал принимается с типом New (работаем с отложенниками ж)
         * Если последний бар цепляет сигнал - переходит в Executed
         * Если находясь в статусе New дотянулся до уровня Stoploss сигнала - переходит в Canceled
         * Если по сигналу открывается сделка - переодит в Accepted
         * Если находясь в статусе Executed так и не был исполнен сделкой, то на следующем переходит в статус Skipped
         */

        /// <summary>
        /// Неизвестно
        /// </summary>
        None = 0,
        /// <summary>
        /// Создан,но еще не исполнен(чаще всего отложенник)
        /// </summary>
        New = 1,
        /// <summary>
        /// Исполнен (рынок выполнил цели по сигналу, но входа по этому сигналу еще нет)
        /// </summary>
        Executed = 2,
        /// <summary>
        /// Отменен
        /// </summary>
        Canceled = 3,

        /// <summary>
        /// Исполнен сделкой(был отработан на рынке)
        /// </summary>
        Accepted = 4,
        
        /// <summary>
        /// Был пропущен
        /// </summary>
        Skipped = 5,

    }
}
