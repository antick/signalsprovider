﻿using System.ComponentModel;

namespace Antick.SignalsProvider.Models
{
    /// <summary>
    /// Тип сигнала по ТС ФМА-САР
    /// </summary>
    public enum FmaSarSignalType
    {
        /// <summary>
        /// Неизвестно
        /// </summary>
        None = 0,
        /// <summary>
        /// ПО тренду
        /// </summary>
        [Description("По тренду")]
        Trend = 1,
        
        /// <summary>
        /// Флэт
        /// </summary>
        [Description("Во флэте")]
        Flat = 2,
        
        /// <summary>
        /// На разворот
        /// </summary>
        [Description("На разворот")]
        Reverse = 3,
    }
}
