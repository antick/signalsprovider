﻿namespace Antick.SignalsProvider.Models
{
    /// <summary>
    /// Система принятия решений
    /// </summary>
    public enum DmsEnum
    {
        None = 0,
        /// <summary>
        /// Оригинальная стратегия по ТС ФМА Сар(как я ее вижу)
        /// </summary>
        FmaSarOrigin = 1
    }
}
