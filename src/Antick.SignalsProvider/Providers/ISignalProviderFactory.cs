﻿using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Models;

namespace Antick.SignalsProvider.Providers
{
    public interface ISignalProviderFactory
    {
        ISignalProvider Create(SignalProviderModel model, ISignalPublisher signalPublisher);
        void Release(ISignalProvider signalProvider);
    }
}
