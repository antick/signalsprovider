﻿using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Models;

namespace Antick.SignalsProvider.Providers
{
    public interface ISignalProviderAutomateFactory
    {
        ISignalProvider Create(string componentName, SignalProviderModel model, ISignalPublisher signalPublisher);
        void Release(ISignalProvider signalProvider);
    }
}
