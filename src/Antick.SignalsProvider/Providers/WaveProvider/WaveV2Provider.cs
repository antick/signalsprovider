﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Contracts.Specs;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.IndicatorProviders;
using Antick.SignalsProvider.IndicatorProviders.MovementProvider;
using Antick.SignalsProvider.IndicatorProviders.SmaProvider;
using Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers.MaProvider;
using Antick.SignalsProvider.Providers.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Internal;
using Newtonsoft.Json;

namespace Antick.SignalsProvider.Providers.WaveProvider
{
    public class WaveV2Provider : SignalProvider
    {
        private WaveV2Parameters ParametersObj => (WaveV2Parameters)Parameters;
        private WaveV2DataModel DataModelObj => (WaveV2DataModel)DataModel;
        private InstrumentSpecItem m_Spec;
        private readonly IIndicatorProvider m_IndicatorProvider;
        private readonly ISignalPublisher m_SignalPublisher;

        public WaveV2Provider(SignalProviderModel model, IIndicatorProvider indicatorProvider, ISignalPublisher signalPublisher) : base(model)
        {
            m_IndicatorProvider = indicatorProvider;
            m_SignalPublisher = signalPublisher;
        }

        public override void Configure()
        {
            Parameters = !Model.Parameters.IsNullOrEmpty()
                ? JsonConvert.DeserializeObject<WaveV2Parameters>(Model.Parameters)
                : null;

            if (Parameters == null)
                throw new Exception("Параметры не заданы");

            // Грузить из източника
            DataModel = new WaveV2DataModel();

            Instrument instrument = (Instrument)Enum.Parse(typeof(Instrument), Instrument.Name);
            m_Spec = InstrumentSpec.Get(instrument);

        }

        public override TimeFrameComplex GetTimeFrame()
        {
            throw new NotImplementedException();
        }

        public override TimeFrameState GetTimeframeState()
        {
            throw new NotImplementedException();
        }

        public override void Consume(List<Rate> rates)
        {
            // Добавление новый данных по рейтам, расчеты индикаторв и прочей логики
            appendData(rates);

            publish();

            cleadData();
        }

        private void appendData(List<Rate> rates)
        {
            var dataModel = DataModelObj;

            var nextRateIndex = dataModel.Rates.Count;

            dataModel.Rates.AddRange(rates);

            for (int k = 0; k < rates.Count; k++)
            {
                var i = nextRateIndex + k;
                var rate = dataModel.Rates[k];

                dataModel.MaFast.AddRange(ma(ParametersObj.MaFastPeriod, i));
                dataModel.MaSlow.AddRange(ma(ParametersObj.MaSlowPeriod, i));
                dataModel.MaVerySlow.AddRange(ma(ParametersObj.MaVerySlowPeriod, i));
                dataModel.Stochastic.AddRange(stochastic(i));

                var stochasticZoneResult = stochasticZone(i);
                dataModel.StochasticZone.AddRange(stochasticZoneResult.Values);
                dataModel.StochasticZoneDynamicValues = stochasticZoneResult.DynamicValues;

                var movementResultFast = movement(rate.Index, MovementType.Low);
                dataModel.StochasticZoneUsedLow.AddRange(movementResultFast.PivotZones);
                dataModel.MovementFastDynamicParams = movementResultFast.DynamicValues;

                var movementResultSlow = movement(rate.Index, MovementType.High);
                dataModel.StochasticZoneUsedHigh.AddRange(movementResultSlow.PivotZones);
                dataModel.MovementSlowDynamicParams = movementResultSlow.DynamicValues;
            }
        }

        private void cleadData()
        {
            var dataModel = DataModelObj;

            if (DataModelObj.Rates.Count > 1000)
            {
                dataModel.Rates = dataModel.Rates.Skip(DataModelObj.Rates.Count - 1000).ToList();
                dataModel.MaFast = dataModel.MaFast.Skip(DataModelObj.Rates.Count - 1000).ToIndexList();
                dataModel.MaSlow = dataModel.MaSlow.Skip(DataModelObj.Rates.Count - 1000).ToIndexList();
                dataModel.MaVerySlow = dataModel.MaVerySlow.Skip(DataModelObj.Rates.Count - 1000).ToIndexList();
            }
        }

        private void publish()
        {

        }

        private IndexedList<double> ma(int period, int? i = null)
        {
            return m_IndicatorProvider.Sma(new SmaIndicatorProviderContext
            {
                Rates = DataModelObj.Rates,
                TimeFrameValue = Convert.ToInt32(ParametersObj.Tf.Value),
                Digits = m_Spec.Digits,
                Period = period,
                I = i
            });
        }

        private IndexedList<StochasticIndicatorData> stochastic(int? i = null)
        {
            return m_IndicatorProvider.Stochastic(new StochasticIndicatorProviderContext
            {
                Rates = DataModelObj.Rates,
                Pk = ParametersObj.StochasticPk,
                Sk = ParametersObj.StochasticSk,
                Pd = ParametersObj.StochasticPd,
                I = i
            });
        }

        private StochasticZoneResult stochasticZone(int? i = null)
        {
            return m_IndicatorProvider.StochasticZone(new StochasticZoneIndicatorProviderContext
            {
                Rates = DataModelObj.Rates,
                Stochastic = DataModelObj.Stochastic,
                StochasticParams = DataModelObj.StochasticZoneDynamicValues,
                I = i
            });
        }

        private MovementResult movement(long? index, MovementType movementType)
        {
            var context = new MovementIndicatorProviderContext
            {
                Rates = DataModelObj.Rates,
                MaValues = DataModelObj.MaFast,
                StochasticZoneValues = DataModelObj.StochasticZone,
                Movemements = movementType == MovementType.Low ?  DataModelObj.MovementsFast : DataModelObj.MovementsSlow,
                MovemementParams = movementType == MovementType.Low ? DataModelObj.MovementFastDynamicParams : DataModelObj.MovementSlowDynamicParams,
                CallbackAdded = args => { execWaveAnalysis(WaveEventType.AddMovement, args.Index, movementType); },
                CallBackUpdated = args => { execWaveAnalysis(WaveEventType.UpdateMovement, args.Index, movementType); },
                Index = index
            };

            var result = m_IndicatorProvider.Movement(context);

            return result;
        }

        private void execWaveAnalysis(WaveEventType eventType, long index, MovementType movementType)
        {
            return;

            //m_CommandBuilder.Execute(new WaveAnalysisContext(m_Ptf, eventType, index));
        }

        private enum MovementType
        {
            Low,
            High
        }
    }
}
