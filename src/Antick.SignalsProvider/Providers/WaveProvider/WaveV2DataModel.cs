﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Providers.WaveProvider
{
    public class WaveV2DataModel
    {
        public List<Rate> Rates { get; set; }
        public IndexedList<double> MaSlow { get; set; }
        public IndexedList<double> MaFast { get; set; }
        public IndexedList<double> MaVerySlow { get; set; }
        public IndexedList<StochasticIndicatorData> Stochastic { get; set; }
        public IndexedList<StochasticZoneType> StochasticZone { get; set; }
        public IndexedList<StochasticZoneType> StochasticZoneUsedLow { get; set; }
        public IndexedList<StochasticZoneType> StochasticZoneUsedHigh { get; set; }
        public List<Movement> MovementsFast { get; set; }
        public List<Movement> MovementsSlow { get; set; }

        /// <summary>
        /// Динамически данные индикатора StochasticZone
        /// </summary>
        public StochasticZoneDynamicValues StochasticZoneDynamicValues { get; set; }

        /// <summary>
        /// Динамические данные индикатора Movement
        /// </summary>
        public MovementDynamicValues MovementFastDynamicParams { get; set; }

        public MovementDynamicValues MovementSlowDynamicParams { get; set; }

        public DateTime? TfLastRateDate { get; set; }
        public long? TfLastRateIndex { get; set; }

        /// <summary>
        /// Вся переменные состояния волновой логики
        /// </summary>
        public Bus Bus { get; set; }

        public WaveV2DataModel()
        {
            Rates = new List<Rate>();
            MaSlow = new IndexedList<double>();
            MaFast = new IndexedList<double>();
            Stochastic = new IndexedList<StochasticIndicatorData>();
            StochasticZone = new IndexedList<StochasticZoneType>();
            StochasticZoneUsedLow = new IndexedList<StochasticZoneType>();
            StochasticZoneUsedHigh = new IndexedList<StochasticZoneType>();
            MovementsFast = new List<Movement>();
            MovementsSlow = new List<Movement>();
            MaVerySlow = new IndexedList<double>();
        }
    }
}
