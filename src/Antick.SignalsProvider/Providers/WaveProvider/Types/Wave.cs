﻿using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public class Wave
    {
        public long Id { get; set; }

        public long? MovementId { get; set; }

        public WaveType WaveType { get; set; }

        public PatternType Pattern { get; set; }

        public long? ParentId { get; set; }

        public int Index { get; set; }

        public DirectionType TrendType { get; set; }

        public bool IsCompleted { get; set; }

        public int Depth { get; set; }
    }
}
