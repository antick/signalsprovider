﻿namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public enum WaveEventType
    {
        AddMovement,
        UpdateMovement,
        NewRate,
        /// <summary>
        /// Устарел
        /// </summary>
        NewSarBar
    }
}
