﻿using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public class Bus
    { 
        /// <summary>
        /// Активный тренд
        /// </summary>
        public DirectionType Direction = DirectionType.None;

        
        public PatternData Pattern { get; set; }
        public ReverseData Reverse { get; set; }
    }
}
