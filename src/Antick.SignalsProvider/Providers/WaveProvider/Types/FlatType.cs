using System.ComponentModel;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    /// <summary>
    /// ���� ������
    /// </summary>
    public enum FlatType : long
    {
        /// <summary>
        /// �������
        /// </summary>
        [Description("�������")]
        Simple = 1,

        /// <summary>
        /// ���� � �����
        /// </summary>
        [Description("� �����")]
        AfterIpuz = 2,
        
        /// <summary>
        /// ���� ����� ������ ������ ����-����� ��. ����� �� ������� ���/���
        /// </summary>
        [Description("����� ������� ������")]
        AfterCanceledVipuz = 3
    }
}