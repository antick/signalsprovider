﻿using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public class Flat
    {
        public long Id { get; set; }

        public long WaveMarkupId { get; set; }

        public DirectionType TrendType { get; set; }

        /// <summary>
        /// Индекс бара, на котором начинается рисоваться флэт(верхняя точка тренда при направление вверх)
        /// </summary>
        public long BarStart { get; set; }

        /// <summary>
        /// индекс бара, с которого точно узнали что начался флэт
        /// </summary>
        public long BarInit { get; set; }

        /// <summary>
        /// Индекс бара, на котором флэт закончился. 
        /// </summary>
        public long? BarEnd { get; set; }

        /// <summary>
        /// Индекс бара, конца для Наклонного Канала
        /// </summary>
        public long BarNKEnd { get; set; }

        public FlatType FlatType { get; set; }

        public bool IsCompleted { get; set; }
    }
}
