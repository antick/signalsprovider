﻿using System.ComponentModel;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    /// <summary>
    /// Паттерны
    /// </summary>
    public enum PatternType : long 
    {
        /// <summary>
        /// Нету, (Был апдейт тренда, или переход по тренду)
        /// </summary>
        [Description("Н/А")]
        None = 1,

        /// <summary>
        /// ПП ФЗР
        /// </summary>
        [Description("ПП ФЗР")]
        Ppfzr = 2,

        /// <summary>
        /// УЗ
        /// </summary>
        [Description("УЗ")]
        Uz = 3,

        /// <summary>
        /// ИПУЗ
        /// </summary>
        [Description("ИПУЗ")]
        Ipuz = 4,

        /// <summary>
        /// ВИПУЗ
        /// </summary>
        [Description("ВИПУЗ")]
        Vipuz = 5,

        /// <summary>
        /// Флэт простой
        /// </summary>
        [Description("Флэт(Простой)")]
        FlatSimple = 6,

        /// <summary>
        /// Флэт после ИПУЗа
        /// </summary>
        [Description("Флэт(к ИПУЗу)")]
        FlatAfterIpuz = 7,

        /// <summary>
        /// Флэт после отменнного ВИПУЗа
        /// </summary>
        [Description("Флэт(после отмены ВИПУЗа")]
        FlatAfterCanceledVipuz = 8,

        /// <summary>
        /// Сложный импульс, формируется когда выход флэта происходит по тренду.
        /// </summary>
        [Description("Сложный импульс")]
        ComplexImpulse = 9
    }
}