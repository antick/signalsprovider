﻿namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public enum ReverseStatus : byte
    {
        None,

        /// <summary>
        /// Стадия 1 : Волна пересекла МА208
        /// </summary>
        Stage1,

        /// <summary>
        /// Стадия 2: Сформирована первая коррекционная волна ( ФЗР и Сарок нет)
        /// </summary>
        Stage2,

        /// <summary>
        /// Стадия 3, добавлена разворотная сарка : Импульс + Коррекция + Сарка (Но нет ФЗР)
        /// </summary>
        Stage3A,

        /// <summary>
        /// Стадия 3B : Импульс + Коррекция + ФЗР (Но нет Сарки)
        /// </summary>
        Stage3B,

        /// <summary>
        /// Стадия 4: все есть, не важно в какой последовательности : ИМПУЛЬС + КОРРЕКЦИЯ + САРКА + ФЗР
        /// </summary>
        Stage4
    }
}
