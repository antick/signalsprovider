﻿using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public class Trend
    {
        public long Id { get; set; }

        public long StartWaveId { get; set; }

        public long EndWaveId { get; set; }

        public DirectionType Type { get; set; }

        public bool IsCompleted { get; set; }

        public int Index { get; set; }
    }
}
