﻿namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public class PatternData
    {
        public PatternType Up { get; set; }
        public PatternType Down { get; set; }
    }
}
