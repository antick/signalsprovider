﻿namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public class ReverseData
    {
        /*
         * План А - 1. Пересечение МА208, 2. Разворотная сар-свеча 3. ФЗР  (обычный вид разворота)
         * План Б - 1. Разворотная сар-свеча 2. ФЗР (Скрытый разворот)
         */

        /// <summary>
        /// Есть пересенчие МА208
        /// </summary>
        public bool CrossMaSlow { get; set; }

        /// <summary>
        /// Индекс разворотного сар-бара, после пересечения МА208 (План А)
        /// </summary>
        public long SarBar { get; set; }

        /// <summary>
        /// Айди MarkupWave волны, импульсной(в Плане А это волн пересекает МА208)
        /// </summary>
        public long ImpulseId { get; set; }

        public long CorrectionId { get; set; }

        /// <summary>
        /// Волна, что создала фзр(3ая волна при фзр)
        /// </summary>
        public long FzrWaveId { get; set; }

        public bool HasFzr { get; set; }

        public ReverseData()
        {
            CrossMaSlow = false;
            HasFzr = false;
            SarBar = -1;
            ImpulseId = -1;
            CorrectionId = -1;
            FzrWaveId = -1;
        }
    }
}
