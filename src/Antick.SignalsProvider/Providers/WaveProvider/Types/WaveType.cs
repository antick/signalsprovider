﻿namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    /// <summary>
    /// Тип волны
    /// </summary>
    public enum WaveType : long 
    {
        /// <summary>
        /// Импульс
        /// </summary>
        Impulse = 1,
        /// <summary>
        /// Коррекция
        /// </summary>
        Correction = 2
    }
}
