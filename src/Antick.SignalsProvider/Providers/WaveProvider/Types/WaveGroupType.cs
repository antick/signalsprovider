﻿namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    public enum WaveGroupType
    {
        None = 0,
        Wave,
        WaveMarkup,
        Trend
    }
}
