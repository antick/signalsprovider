﻿using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Providers.WaveProvider.Types
{
    /// <summary>
    /// Комбинация Id SignalProvider и Таймфрейма, 
    /// </summary>
    public class Ptf
    {
        public int ProviderId { get; set; }
        public TimeFrameComplex TimeFrame { get; set; }
        public InstrumentComplex Instrument { get; set; }


        public Ptf(int providerId, InstrumentComplex instrument, TimeFrameComplex timeFrame)
        {
            ProviderId = providerId;
            Instrument = instrument;
            TimeFrame = timeFrame;
        }

        public override string ToString()
        {
            return $"{ProviderId}_{TimeFrame}";
        }
    }
}
