﻿using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Providers.WaveProvider
{
    public class WaveV1Parameters
    {
        public TimeFrameComplex Tf { get; set; }
        
        /// <summary>
        /// Период скользящей средней старшего порядка (по дефолту это 208 обычно)
        /// </summary>
        public int MaFastPeriod { get; set; }

        /// <summary>
        /// Период скользящей средней младшего порядка (по дефолту это 13 обычно)
        /// </summary>
        public int MaSlowPeriod { get; set; }

        public int MaVerySlowPeriod { get; set; }

        /// <summary>
        /// Период %K (Pk). Это число единичных периодов, используемых для расчета %K. По умолчанию равен 20 (В МТ4 дефолт это 5)
        /// </summary>
        public int StochasticPk { get; set; }

        /// <summary>
        /// Период замедления %K (Sk). Эта величина определяет степень внутренней сглаженности линии %K.
        /// Значение 1 дает быстрый стохастический осциллятор, а значение 3 - медленный. По умолчанию равен 3
        /// </summary>
        public int StochasticSk { get; set; }

        /// <summary>
        /// Период %D (Pd). Это число единичных периодов, используемых для расчета скользящего среднего линии %K. По умолчанию равен 2(в МТ4 дефолт это 3)
        /// </summary>
        public int StochasticPd { get; set; }
    
        /// <summary>
        /// Глубина хранения исторический данных
        /// </summary>
        public int RatesCount { get; set; }
    }
}
