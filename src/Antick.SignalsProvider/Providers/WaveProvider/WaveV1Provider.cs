﻿using System;
using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Extensions;
using Antick.SignalsProvider.Components;
using Antick.SignalsProvider.Cqrs.WaveAlg;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Newtonsoft.Json;

namespace Antick.SignalsProvider.Providers.WaveProvider
{
    public class WaveV1Provider : SignalProvider
    {
        private readonly SignalProviderModel m_Model;
        
        private WaveV1Parameters ParametersObj => (WaveV1Parameters) Parameters;
        private WaveV1DataModel DataModelObj => (WaveV1DataModel) DataModel;

        private readonly IWaveFileStorage m_WaveFileStorage;
        private readonly ICommandBuilder m_CommandBuilder;
        
        public WaveV1Provider(ILogger logger, IWaveFileStorage waveFileStorage, ICommandBuilder commandBuilder,  SignalProviderModel model):base(model)
        {
            m_WaveFileStorage = waveFileStorage;
            m_CommandBuilder = commandBuilder;
            m_Model = model;
        }

        public override void Configure()
        {
            Parameters = !m_Model.Parameters.IsNullOrEmpty()
                ? JsonConvert.DeserializeObject<WaveV1Parameters>(m_Model.Parameters)
                : null;

            if (Parameters == null)
                throw new Exception("Параметры не заданы");

            DataModel = m_WaveFileStorage.Get(Id).GetSynchronousResult() ?? new WaveV1DataModel();
        }
        
        public override TimeFrameComplex GetTimeFrame()
        {
            return ParametersObj.Tf;
        }

        public override TimeFrameState GetTimeframeState()
        {

            return
                new TimeFrameState
                {
                    TimeFrame = ParametersObj.Tf,
                    LastRateDate = DataModelObj.TfLastRateDate,
                    LastRateIndex = DataModelObj.TfLastRateIndex
                };
        }

        public override void Consume(List<Rate> rates)
        {
            var ptf = new Ptf(Id, Instrument, ParametersObj.Tf);
            m_CommandBuilder.Execute(new ProccessingWaveV1Context(ptf, rates));
        }
    }
}
