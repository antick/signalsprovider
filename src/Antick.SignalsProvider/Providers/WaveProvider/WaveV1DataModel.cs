﻿using System;
using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Providers.WaveProvider
{
    public class WaveV1DataModel
    {
        public List<Rate> Rates { get; set; }
        public IndexedList<double> MaSlow { get; set; }
        public IndexedList<double> MaFast { get; set; }
        public IndexedList<double> MaVerySlow { get; set; }
        public IndexedList<StochasticIndicatorData> Stochastic { get; set; }
        public IndexedList<StochasticZoneType> StochasticZone { get; set; }
        public IndexedList<StochasticZoneType> PivotZones { get; set; }
        public List<Movement> Movements { get; set; }
        public List<Wave> Waves { get; set; }
        public List<WaveMarkup> WaveMarkups { get; set; }
        public List<Trend> Trends { get; set; }
        public List<Flat> Flats { get; set; }

        /// <summary>
        /// Динамически данные индикатора StochasticZone
        /// </summary>
        public StochasticZoneDynamicValues StochasticZoneDynamicValues { get; set; }

        /// <summary>
        /// Динамические данные индикатора Movement
        /// </summary>
        public MovementDynamicValues MovementDynamicValues { get; set; }

        public DateTime? TfLastRateDate { get; set; }
        public long? TfLastRateIndex { get; set; }

        /// <summary>
        /// Вся переменные состояния волновой логики
        /// </summary>
        public Bus Bus { get; set; }

        public WaveV1DataModel()
        {
            Rates = new List<Rate>();
            MaSlow = new IndexedList<double>();
            MaFast = new IndexedList<double>();
            Stochastic = new IndexedList<StochasticIndicatorData>();
            StochasticZone = new IndexedList<StochasticZoneType>();
            PivotZones = new IndexedList<StochasticZoneType>();
            Movements = new List<Movement>();
            Waves = new List<Wave>();
            WaveMarkups = new List<WaveMarkup>();
            Trends = new List<Trend>();
            Flats = new List<Flat>();
            MaVerySlow = new IndexedList<double>();
        }
    }
}
