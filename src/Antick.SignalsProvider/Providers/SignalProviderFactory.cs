﻿using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Models;

namespace Antick.SignalsProvider.Providers
{
    public class SignalProviderFactory : ISignalProviderFactory
    {
        private readonly ISignalProviderAutomateFactory m_AutomateFactory;

        public SignalProviderFactory(ISignalProviderAutomateFactory automateFactory)
        {
            m_AutomateFactory = automateFactory;
        }

        public ISignalProvider Create(SignalProviderModel model, ISignalPublisher signalPublisher)
        {
            var component = m_AutomateFactory.Create(model.Type.ToString(), model, signalPublisher);
            component.Configure();
            return component;
        }

        public void Release(ISignalProvider signalProvider)
        {
            m_AutomateFactory.Release(signalProvider);
        }
    }
}
