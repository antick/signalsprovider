﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.SignalsProvider.Db;

namespace Antick.SignalsProvider.Providers
{
    public interface ISignalProvider
    {
        int Id { get; }

        /// <summary>
        /// Имя провайдера
        /// </summary>
        string Name { get; }
        
        InstrumentComplex Instrument { get; }

        string Type { get; }

        /// <summary>
        /// Статус общей готовности провайдера(включенность по Модели + первичная загрузка)
        /// </summary>
        bool Active { get; }

        /// <summary>
        /// Установка готовности провайдера(проход первичного запуска)
        /// </summary>
        void SetReady();

        /// <summary>
        /// Конфигурация провайдера, должна запускаться после создания
        /// </summary>
        void Configure();

        dynamic Parameters { get; }
        dynamic DataModel { get; }

        TimeFrameComplex GetTimeFrame();
        TimeFrameState GetTimeframeState();
        
        void Consume(List<Rate> rates);
    }
}
