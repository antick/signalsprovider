﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.Providers
{
    public class TicketBillet
    {
        public Guid SignalId { get; set; }

        public double EntryPrice { get; set; }

        public double TakeProfit { get; set; }

        public double Stoploss { get; set; }

        public bool Active { get; set; }
    }
}
