﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Apps.SingalsProvider;
using Antick.Contracts.Domain;
using Antick.Contracts.Msgs.SignalsProvider;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Db;

namespace Antick.SignalsProvider.Providers.TestsProviders
{
    public class StupidProvider : SignalProvider
    {
        private readonly ISignalPublisher m_SignalPublisher;

        public override int Id => 0;
        public override string Name => "StupidSignalProvider";
        public override InstrumentComplex Instrument => new InstrumentComplex{Name = "EUR_USD", Group = InstrumentGroup.Fx, Provider = InstrumentProvider.Oanda};
        public override bool Active => true;

        public StupidProvider(ISignalPublisher signalPublisher):base(null)
        {
            m_SignalPublisher = signalPublisher;
        }

        public override TimeFrameComplex GetTimeFrame()
        {
            return new TimeFrameComplex {Type = TimeFrameType.Renko, Value = "5"};
        }

        public override TimeFrameState GetTimeframeState()
        {
            throw new NotImplementedException();
        }

        public override void Consume(List<Rate> rates)
        {
            var rate = rates.Last();

            var minStep = 0.0001;

            var direction = new Random((int) DateTime.Now.Ticks).Next() % 2 == 0
                ? SignalDirection.Buy
                : SignalDirection.Sell;

            var newSignal = new SignalCreated()
            {
                Direction = direction,
                ExecutionType = SignalExecutionType.Market,
                Instrument = Contracts.Domain.Instrument.EUR_USD,
                LastPrice = rate.Close,
                LastRateIndex = rate.Index,
                ProviderName = Name,
                TakeProfit = direction == SignalDirection.Buy ? rate.Close + minStep * 30 : rate.Close - minStep * 30,
                StopLoss = direction == SignalDirection.Buy ? rate.Close - minStep * 30 : rate.Close + minStep * 30,
            };

            m_SignalPublisher.Publish(newSignal);
        }
    }
}
