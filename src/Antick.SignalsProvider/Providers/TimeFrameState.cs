﻿using System;
using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Providers
{
    public class TimeFrameState
    {
        public TimeFrameComplex TimeFrame { get; set; }
        public DateTime? LastRateDate { get; set; }
        public long? LastRateIndex { get; set; }
    }
}
