﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Domain;

namespace Antick.SignalsProvider.Providers.MaProvider
{
    public class MaProviderParameters
    {
        public TimeFrameComplex Tf { get; set; }
        
        /// <summary>
        /// Период скользящей средней старшего порядка (по дефолту это 208 обычно)
        /// </summary>
        public int MaFastPeriod { get; set; }

        /// <summary>
        /// Период скользящей средней младшего порядка (по дефолту это 13 обычно)
        /// </summary>
        public int MaSlowPeriod { get; set; }

        public int MaVerySlowPeriod { get; set; }
    }
}
