﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Providers.MaProvider
{
    public class MaProviderDataModel
    {
        public List<Rate> Rates { get; set; }
        public IndexedList<double> MaSlow { get; set; }
        public IndexedList<double> MaFast { get; set; }
        public IndexedList<double> MaVerySlow { get; set; }

        public MaProviderDataModel()
        {
            Rates = new List<Rate>();
            MaSlow = new IndexedList<double>();
            MaFast = new IndexedList<double>();
            MaVerySlow = new IndexedList<double>();
        }
    }
}
