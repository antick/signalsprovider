﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Apps.SingalsProvider;
using Antick.Contracts.Domain;
using Antick.Contracts.Msgs.SignalsProvider;
using Antick.Contracts.Specs;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Newtonsoft.Json;
using Castle.Core.Internal;

namespace Antick.SignalsProvider.Providers.MaProvider
{
    public class MaProvider : SignalProvider
    {
        private readonly ISignalPublisher m_SignalPublisher;

        private MaProviderParameters ParametersObj => (MaProviderParameters)Parameters;
        private MaProviderDataModel DataModelObj => (MaProviderDataModel)DataModel;
        private InstrumentSpecItem m_Spec;

        public MaProvider(SignalProviderModel model, ISignalPublisher signalPublisher): base(model)
        {
            m_SignalPublisher = signalPublisher;
        }

        public override void Configure()
        {
            Parameters = !Model.Parameters.IsNullOrEmpty()
                ? JsonConvert.DeserializeObject<MaProviderParameters>(Model.Parameters)
                : null;

            if (Parameters == null)
                throw new Exception("Параметры не заданы");

            // Грузить из източника
            DataModel = new MaProviderDataModel();

            Instrument instrument = (Instrument)Enum.Parse(typeof(Instrument), Instrument.Name);
            m_Spec = InstrumentSpec.Get(instrument);

        }

        public override TimeFrameComplex GetTimeFrame()
        {
            return ParametersObj.Tf;
        }

        public override TimeFrameState GetTimeframeState()
        {
            throw new NotImplementedException();
        }

        public override void Consume(List<Rate> rates)
        {
            var dataModel = DataModelObj;

            var nextRateIndex = dataModel.Rates.Count;

            dataModel.Rates.AddRange(rates);

            for (int k = 0; k < rates.Count; k++)
            {
                var i = nextRateIndex + k;
                dataModel.MaFast.AddRange(ma(ParametersObj.MaFastPeriod, i));
                dataModel.MaSlow.AddRange(ma(ParametersObj.MaSlowPeriod, i));
                dataModel.MaVerySlow.AddRange(ma(ParametersObj.MaVerySlowPeriod, i));
            }

            publish();

            if (rates.Count > 1000)
            {
                dataModel.Rates = dataModel.Rates.Skip(rates.Count - 1000).ToList();
                dataModel.MaFast = dataModel.MaFast.Skip(rates.Count - 1000).ToIndexList();
                dataModel.MaSlow = dataModel.MaSlow.Skip(rates.Count - 1000).ToIndexList();
                dataModel.MaVerySlow = dataModel.MaVerySlow.Skip(rates.Count - 1000).ToIndexList();
            }
        }

        /// <summary>
        /// Публикация или не публикация сигнала
        /// </summary>
        private void publish()
        {
            if (DataModelObj.Rates.Count < ParametersObj.MaVerySlowPeriod)
                return;

            var needPublish = false;
            var upTrend = DataModelObj.Rates.Last().Close >= DataModelObj.MaVerySlow.Last().Value;

            var prevMaFast = DataModelObj.MaFast.Prev().Value;
            var lastMaFast = DataModelObj.MaFast.Last().Value;

            var prevMaSlow = DataModelObj.MaSlow.Prev().Value;
            var lastMaSlow = DataModelObj.MaSlow.Last().Value;

            var dslowVerySlowMa = 0.0040;

            if (upTrend)
            {
                if (lastMaFast > lastMaSlow && prevMaFast <= lastMaSlow)
                    needPublish = true;
            }
            else
            {
                if (lastMaFast < lastMaSlow && prevMaFast >= lastMaSlow)
                    needPublish = true;
            }

            if (needPublish)
            {
                var direction = upTrend ? SignalDirection.Buy : SignalDirection.Sell;
                var rate = DataModelObj.Rates.Last();
                var minStep = 0.0001;


                var newSignal = new SignalCreated()
                {
                    Direction = direction,
                    ExecutionType = SignalExecutionType.Market,
                    Instrument = Contracts.Domain.Instrument.EUR_USD,
                    LastPrice = rate.CloseFact,
                    LastRateIndex = rate.Index,
                    ProviderName = Name,
                    TakeProfit = direction == SignalDirection.Buy
                        ? rate.Close + dslowVerySlowMa
                        : rate.Close - dslowVerySlowMa,
                    StopLoss = direction == SignalDirection.Buy ? rate.Close - dslowVerySlowMa * 1 : rate.Close + dslowVerySlowMa * 1,
                };

                m_SignalPublisher.Publish(newSignal);
            }
        }

        private IndexedList<double> ma(int period, int? i = null)
        {
            var dataModel = DataModelObj;
            var tfValue = ParametersObj.Tf.Value;

            if (i == null)
            {

                var list = SmaIndicator.Calculate(
                    dataModel.Rates, period, m_Spec.Digits, Convert.ToInt32(tfValue));
                return new IndexedList<double>(list);
            }
            else
            {
                var ii = i.Value;

                var item = new IndexValue<double>
                {
                    Index = dataModel.Rates[ii].Index,
                    Value = SmaIndicator.Calculate(dataModel.Rates, period, ii, m_Spec.Digits,
                        Convert.ToInt32(tfValue))
                };

                return new IndexedList<double>(item);
            }
        }
    }
}
