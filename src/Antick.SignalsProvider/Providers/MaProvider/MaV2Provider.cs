﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Apps.SingalsProvider;
using Antick.Contracts.Domain;
using Antick.Contracts.Msgs.SignalsProvider;
using Antick.Contracts.Specs;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.IndicatorProviders;
using Antick.SignalsProvider.IndicatorProviders.SmaProvider;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Models;
using Castle.Core.Internal;
using Newtonsoft.Json;

namespace Antick.SignalsProvider.Providers.MaProvider
{
    public class MaV2Provider : SignalProvider
    {
        private readonly ISignalPublisher m_SignalPublisher;
        private readonly IIndicatorProvider m_IndicatorProvider;

        private MaProviderParameters ParametersObj => (MaProviderParameters)Parameters;
        private MaV2ProviderDataModel DataModelObj => (MaV2ProviderDataModel)DataModel;
        private InstrumentSpecItem m_Spec;

        public MaV2Provider(SignalProviderModel model, ISignalPublisher signalPublisher, IIndicatorProvider indicatorProvider): base(model)
        {
            m_SignalPublisher = signalPublisher;
            m_IndicatorProvider = indicatorProvider;
        }

        public override void Configure()
        {
            Parameters = !Model.Parameters.IsNullOrEmpty()
                ? JsonConvert.DeserializeObject<MaProviderParameters>(Model.Parameters)
                : null;

            if (Parameters == null)
                throw new Exception("Параметры не заданы");

            // Грузить из източника
            DataModel = new MaV2ProviderDataModel();

            Instrument instrument = (Instrument)Enum.Parse(typeof(Instrument), Instrument.Name);
            m_Spec = InstrumentSpec.Get(instrument);

        }

        public override TimeFrameComplex GetTimeFrame()
        {
            return ParametersObj.Tf;
        }

        public override TimeFrameState GetTimeframeState()
        {
            throw new NotImplementedException();
        }

        public override void Consume(List<Rate> rates)
        {
            var dataModel = DataModelObj;

            var nextRateIndex = dataModel.Rates.Count;

            dataModel.Rates.AddRange(rates);

            for (int k = 0; k < rates.Count; k++)
            {
                var i = nextRateIndex + k;
                dataModel.MaSlow.AddRange(ma(ParametersObj.MaSlowPeriod, i));
                dataModel.MaVerySlow.AddRange(ma(ParametersObj.MaVerySlowPeriod, i));
            }

            publish();

            if (rates.Count > 1000)
            {
                dataModel.Rates = dataModel.Rates.Skip(rates.Count - 1000).ToList();
                dataModel.MaSlow = dataModel.MaSlow.Skip(rates.Count - 1000).ToIndexList();
                dataModel.MaVerySlow = dataModel.MaVerySlow.Skip(rates.Count - 1000).ToIndexList();
            }
        }

        /// <summary>
        /// Публикация или не публикация сигнала
        /// </summary>
        private void publish()
        {
            if (DataModelObj.Rates.Count < ParametersObj.MaVerySlowPeriod)
                return;

            var needPublish = false;
            var upTrend = DataModelObj.MaSlow.Last().Value >= DataModelObj.MaVerySlow.Last().Value;

            var prevMaSlow = DataModelObj.MaSlow.Prev().Value;
            var lastMaSlow = DataModelObj.MaSlow.Last().Value;

            var prevMaVerySlow = DataModelObj.MaVerySlow.Prev().Value;
            var lastMaVerySlow = DataModelObj.MaVerySlow.Last().Value;

            if (!DataModelObj.Tickets.Any(p => p.Active))
            {
                if (upTrend)
                {
                    if (lastMaSlow > lastMaVerySlow && prevMaSlow <= lastMaVerySlow)
                        needPublish = true;
                }
                else
                {
                    if (lastMaSlow < lastMaVerySlow && prevMaSlow >= lastMaVerySlow)
                        needPublish = true;
                }

                if (needPublish)
                {
                    var direction = upTrend ? SignalDirection.Buy : SignalDirection.Sell;
                    var rate = DataModelObj.Rates.Last();

                    var dslowVerySlowMa = 0.0050;

                    var newSignal = new SignalCreated()
                    {
                        SignalId = Guid.NewGuid(),
                        Direction = direction,
                        ExecutionType = SignalExecutionType.Market,
                        Instrument = Contracts.Domain.Instrument.EUR_USD,
                        LastPrice = rate.CloseFact,
                        LastRateIndex = rate.Index,
                        ProviderName = Name,
                        TakeProfit = 0.0,
                        StopLoss = direction == SignalDirection.Buy ? rate.Close - dslowVerySlowMa : rate.Close + dslowVerySlowMa,
                    };

                    DataModelObj.Tickets.Add(new TicketBillet
                    {
                        SignalId = newSignal.SignalId,
                        EntryPrice = newSignal.LastPrice,
                        TakeProfit = newSignal.TakeProfit,
                        Stoploss = newSignal.StopLoss,
                        Active = true
                    });

                    m_SignalPublisher.Publish(newSignal);
                }
            }

            //var 

           // m_SignalPublisher.Publish((SignalUpdated) new {MaVerySlow = DataModelObj.MaVerySlow.Last().Value});
        }

        private IndexedList<double> ma(int period, int? i = null)
        {
            return m_IndicatorProvider.Sma(new SmaIndicatorProviderContext
            {
                Rates = DataModelObj.Rates,
                TimeFrameValue = Convert.ToInt32(ParametersObj.Tf.Value),
                Digits = m_Spec.Digits,
                Period = period,
                I = i
            });
        }
    }
}
