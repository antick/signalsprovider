﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.SignalsProvider.Cqrs.WaveAlg;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers.Types;

namespace Antick.SignalsProvider.Providers
{
    public abstract class SignalProvider : ISignalProvider
    {
        public virtual int Id { get; }

        public virtual string Name { get; }

        public virtual InstrumentComplex Instrument { get; }

        public string Type => GetType().Name;

        public virtual bool Active => Enabled && IsReady;

        public dynamic Parameters { get; protected set; }

        public dynamic DataModel { get; protected set; }

        protected bool IsReady;
        protected bool Enabled;
        protected readonly SignalProviderModel Model;

        protected SignalProvider(SignalProviderModel model)
        {
            Id = model.Id;
            Name = model.Name;
            Instrument = model.Instrument;
            Enabled = model.Enabled;
            Model = model;
        }

        public void SetReady()
        {
            IsReady = true;
        }

        public virtual void Configure()
        {
        }


        public abstract TimeFrameComplex GetTimeFrame();

        public abstract TimeFrameState GetTimeframeState();

        public abstract void Consume(List<Rate> rates);
    }
}
