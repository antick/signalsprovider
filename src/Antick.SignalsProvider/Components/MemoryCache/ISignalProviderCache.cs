﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers;

namespace Antick.SignalsProvider.Components.MemoryCache
{
    public interface ISignalProviderCache
    {
        void AddOrUpdate(ISignalProvider provider);

        ISignalProvider Get(int signalProviderId);

        List<ISignalProvider> All();
    }
}
