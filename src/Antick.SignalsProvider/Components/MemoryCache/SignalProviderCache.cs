﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers;

namespace Antick.SignalsProvider.Components.MemoryCache
{
    public class SignalProviderCache : ISignalProviderCache
    {
        private readonly ConcurrentDictionary<int, ISignalProvider> m_Providers;

        public SignalProviderCache()
        {
            m_Providers = new ConcurrentDictionary<int, ISignalProvider>();
        }

        public void AddOrUpdate(ISignalProvider provider)
        {
            m_Providers.AddOrUpdate(provider.Id, provider, (i, oldProvider) => provider);
        }

        public ISignalProvider Get(int signalProviderId)
        {
            return m_Providers.ContainsKey(signalProviderId) ? m_Providers[signalProviderId] : null;
        }

        public List<ISignalProvider> All()
        {
            return m_Providers.Values.ToList();
        }
    }
}
