﻿using Antick.Contracts.Msgs.SignalsProvider;

namespace Antick.SignalsProvider.Components.SignalPublisher
{
    public interface ISignalPublisher
    {
        void Publish(SignalCreated signal);
        void Publish(SignalUpdated signal);
    }
}
