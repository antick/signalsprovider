﻿using System;
using Antick.Contracts.Msgs.SignalsProvider;

namespace Antick.SignalsProvider.Components.SignalPublisher
{
    public class TesterSignalPublisher : ISignalPublisher
    {
        private readonly Action<SignalCreated> m_CallBackCreated;
        private readonly Action<SignalUpdated> m_CallBackUpdated;

        public TesterSignalPublisher(Action<SignalCreated> callBackCreated = null, Action<SignalUpdated> callBackUpdated = null)
        {
            m_CallBackCreated = callBackCreated;
            m_CallBackUpdated = callBackUpdated;
        }

        public void Publish(SignalCreated signal)
        {
            m_CallBackCreated?.Invoke(signal);
        }

        public void Publish(SignalUpdated signal)
        {
            m_CallBackUpdated?.Invoke(signal);
        }
    }
}
