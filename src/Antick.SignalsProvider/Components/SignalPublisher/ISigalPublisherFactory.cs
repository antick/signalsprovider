﻿using System;
using Antick.Contracts.Msgs.SignalsProvider;

namespace Antick.SignalsProvider.Components.SignalPublisher
{
    public interface ISigalPublisherFactory
    {
        ISignalPublisher Create(Action<SignalCreated> callback = null, Action<SignalUpdated> callbackData = null);
        void Release(ISignalPublisher component);
    }
}
