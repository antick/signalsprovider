﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Msgs.RatesApi;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Cqrs.Application.Context;
using Castle.Core.Logging;
using EasyNetQ.AutoSubscribe;

namespace Antick.SignalsProvider.Components.Consumers
{
    public class NewRatesConsumer : IConsume<NewRates>
    {
        private readonly ILogger m_Logger;
        private readonly ICommandBuilder m_CommandBuilder;

        public NewRatesConsumer(ILogger logger, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_CommandBuilder = commandBuilder;
        }

        public void Consume(NewRates message)
        {
            m_Logger.DebugFormat("Recivied message : {0};{1}", message.Instrument, message.TimeFrame.ToString());
            m_CommandBuilder.Execute(
                new ConsumeByInstrumentFromBusContext
                {
                    Instrument = message.Instrument,
                    TimeFrame = message.TimeFrame,
                    Rates = message.Rates
                });
        }
    }
}
