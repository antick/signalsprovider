﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Providers.WaveProvider;

namespace Antick.SignalsProvider.Components
{
    public class StubWaveFileStorage : IWaveFileStorage
    {
        public async Task<bool> Save(WaveV1DataModel waveData, int signalProviderId)
        {
            return await Task.FromResult(true);
        }

        public async Task<WaveV1DataModel> Get(int signalProviderId)
        {
            return await Task.FromResult<WaveV1DataModel>(null);
        }
    }
}
