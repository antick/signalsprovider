﻿using System.Collections.Generic;

namespace Antick.SignalsProvider.Components.Blocks
{
    public interface IBlocker
    {
        void Init(IEnumerable<int> ids);

        void Add(int id);

        object Get(int id);
    }
}
