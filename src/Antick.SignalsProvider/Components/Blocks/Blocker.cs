﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.SignalsProvider.Components.Blocks
{
    public class Blocker : IBlocker
    {
        private readonly Dictionary<int, object> m_Blocks;

        public Blocker()
        {
            m_Blocks = new Dictionary<int, object>();
        }

        public void Init(IEnumerable<int> ids)
        {
            foreach (var i in ids)
            {
                m_Blocks[i] = new object();
            }
        }

        public void Add(int id)
        {
            m_Blocks[id] = new object();
        }

        public object Get(int id)
        {
            if (!m_Blocks.ContainsKey(id))
                throw new ArgumentException($"Element block with id {id} is not configured");

            return m_Blocks[id];
        }
    }
}
