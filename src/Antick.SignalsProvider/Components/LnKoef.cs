﻿using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Components
{
    public class LnKoef
    {
        private double _k;
        private double _b;

        public void Calculate(double y1, long x1, double y2, long x2)
        {
            _k = (y2 - y1) / (x2 - x1);
            _b = y1 - _k * x1;
        }

        public void Clear()
        {
            _k = 0;
            _b = 0;
        }

        public bool IsEmpty()
        {
            return _k == 0 && _b == 0;
        }

        public double Y(long x)
        {
            return _k * x + _b;
        }

        public void Calculate(Rate startBar, Rate endBar, DirectionType direction)
        {
            if (direction == DirectionType.Up)
                Calculate(startBar.High, startBar.Index, endBar.High, endBar.Index);
            else Calculate(startBar.Low, startBar.Index, endBar.Low, endBar.Index);
        }
    }
}
