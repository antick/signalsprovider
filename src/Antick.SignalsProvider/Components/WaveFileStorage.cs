﻿using System.IO;
using System.Threading.Tasks;
using Antick.Infractructure.Components.IO;
using Antick.SignalsProvider.Providers.WaveProvider;

namespace Antick.SignalsProvider.Components
{
    public class WaveFileStorage : IWaveFileStorage
    {
        private readonly string m_Folder;
        private readonly IPackage<WaveV1DataModel> m_Package;

        public WaveFileStorage(string folder, IPackage<WaveV1DataModel> package)
        {
            m_Folder = folder;
            m_Package = package;
        }

        private string computeFullPath(int signalProviderId)
        {
            var curDir = Directory.GetCurrentDirectory();
            var baseDir = $"{curDir}\\{m_Folder}";

            var dir = $"{baseDir}\\{signalProviderId}";

            return dir + "\\" + "model.json";
        }

        public async Task<bool> Save(WaveV1DataModel waveData, int signalProviderId)
        {
            var fullpath = computeFullPath(signalProviderId);

            if (!Directory.Exists(fullpath))
            {
                var dir = Path.GetDirectoryName(fullpath);
                Directory.CreateDirectory(dir);
            }

            m_Package.Write(waveData, fullpath);

            return await Task.FromResult(true);
        }

        public async Task<WaveV1DataModel> Get(int signalProviderId)
        {
            var fullpath = computeFullPath(signalProviderId);
            
            if (!File.Exists(fullpath))
                return null;

            var data = m_Package.Read(fullpath);
            return await Task.FromResult(data);
        }
    }
}
