﻿using System.Threading.Tasks;
using Antick.SignalsProvider.Providers.WaveProvider;

namespace Antick.SignalsProvider.Components
{
    /// <summary>
    /// Интерфейс сохранения всей модели 
    /// </summary>
    public interface IWaveFileStorage
    {
        /// <summary>
        /// Сохранить кэш полного дня
        /// </summary>
        Task<bool> Save(WaveV1DataModel waveData, int signalProviderId);

        /// <summary>
        /// Получить десериализованный данные за указанный день по указанному инструменту
        /// </summary>
        Task<WaveV1DataModel> Get(int signalProviderId);
    }
}
