﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Domain;
using Antick.SignalsProvider.Models;

namespace Antick.SignalsProvider.Db
{
    public static class SignalDtoExtensions
    {
        public static SignalProviderModel ToModel(this SignalDto signal)
        {
            var instrument = new InstrumentComplex
            {
                Name = signal.InstrumentName,
                Group = signal.InstrumentGroup,
                Provider = signal.InstrumentProvider
            };

            return new SignalProviderModel(signal.Id, signal.Name, signal.Type, instrument,
                signal.Parameters, signal.Enabled);
        }
    }
}
