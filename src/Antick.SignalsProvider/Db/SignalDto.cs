using Antick.Contracts.Domain;
using Antick.SignalsProvider.Models;

namespace Antick.SignalsProvider.Db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Signal")]
    public partial class SignalDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public string Type { get; set; }

        public bool Enabled { get; set; }

        [Required]
        [StringLength(5000)]
        public string Parameters { get; set; }

        [Required]
        [StringLength(50)]
        public string InstrumentName { get; set; }

        public InstrumentGroup InstrumentGroup { get; set; }

        public InstrumentProvider InstrumentProvider { get; set; }
        
        public string DynamicValues { get; set; }
    }
}
