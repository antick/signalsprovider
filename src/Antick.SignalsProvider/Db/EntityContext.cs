using Inceptum.AppServer.Configuration;

namespace Antick.SignalsProvider.Db
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EntityContext : DbContext
    {
        public EntityContext(ConnectionString connectionString)
            : base(connectionString)
        {
        }

        public virtual DbSet<SignalDto> Signal { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SignalDto>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SignalDto>()
                .Property(e => e.Parameters)
                .IsUnicode(false);

            modelBuilder.Entity<SignalDto>()
                .Property(e => e.InstrumentName)
                .IsUnicode(false);
        }
    }
}
