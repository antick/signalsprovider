﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider
{
    public class StochasticZoneIndicatorProviderContext
    {
        /// <summary>
        /// Рейты
        /// </summary>
        public  List<Rate> Rates { get; set; }
        /// <summary>
        /// Расчитанные данные стохастика
        /// </summary>
        public IndexedList<StochasticIndicatorData> Stochastic { get; set; }
        /// <summary>
        /// Параметры индикатора з предыдущий период
        /// </summary>
        public StochasticZoneDynamicValues StochasticParams { get; set; }
        /// <summary>
        /// Индекс бара внутри Rate На котором нужно выполнить расчет (если не задан - выполняется расчет на всех данных)
        /// </summary>
        public int? I { get; set; }
    }
}
