﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Providers.Types;

namespace Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider
{
    public interface IStochasticZoneIndicatorProvider
    {
        StochasticZoneResult Do(StochasticZoneIndicatorProviderContext context);
    }
}
