﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Providers.Types
{
    public class StochasticZoneResult
    {
        public IndexedList<StochasticZoneType> Values { get; set; }

        public StochasticZoneDynamicValues DynamicValues { get; set; }
    }
}
