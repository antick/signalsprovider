﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.Types;

namespace Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider
{
    public class StochasticZoneIndicatorProvider : IStochasticZoneIndicatorProvider
    {
        public StochasticZoneResult Do(StochasticZoneIndicatorProviderContext context)
        {
            List<IndexValue<StochasticZoneType>> stockZoneTypes = new List<IndexValue<StochasticZoneType>>();

            var stochasticZoneIndicator =
                new StochasticZoneIndicatorV2(new StochasticZoneContext
                {
                    Candles = context.Rates,
                    StochasticData = context.Stochastic
                }, context.StochasticParams);

            if (context.I == null)
            {

                for (int j = 0; j < context.Rates.Count; j++)
                {
                    var value = stochasticZoneIndicator.CalculateLogic(j);
                    stockZoneTypes.Add(
                        new IndexValue<StochasticZoneType> { Value = value, Index = context.Rates[j].Index });
                }
            }
            else
            {
                var j = context.I.Value;

                var val = stochasticZoneIndicator.CalculateLogic(j);

                var value = new IndexValue<StochasticZoneType>
                {
                    Index = context.Rates[j].Index,
                    Value = val
                };
                stockZoneTypes.Add(value);
            }

            return new StochasticZoneResult
            {
                Values = new IndexedList<StochasticZoneType>(stockZoneTypes),
                DynamicValues = stochasticZoneIndicator.GetDynamicValues()
            };
        }
    }
}
