﻿using Antick.SignalsProvider.IndicatorProviders.MovementProvider;
using Antick.SignalsProvider.IndicatorProviders.SmaProvider;
using Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.Types;

namespace Antick.SignalsProvider.IndicatorProviders
{
    public interface IIndicatorProvider
    {
        IndexedList<double> Sma(SmaIndicatorProviderContext context);

        IndexedList<StochasticIndicatorData> Stochastic(StochasticIndicatorProviderContext context);

        StochasticZoneResult StochasticZone(StochasticZoneIndicatorProviderContext context);

        MovementResult Movement(MovementIndicatorProviderContext context);

        MovementResultV2 MovementV2(MovementIndicatorProviderContextV2 context);
    }
}
