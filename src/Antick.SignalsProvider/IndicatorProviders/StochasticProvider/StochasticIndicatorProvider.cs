﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators
{
    public class StochasticIndicatorProvider : IStochasticIndicatorProvider
    {
        public IndexedList<StochasticIndicatorData> Do(StochasticIndicatorProviderContext context)
        {
            if (context.I == null)
            {

                return StochasticIndicator.Calculate(
                    new StochasticIndicatorContext
                    {
                        Rates = context.Rates,
                        Pk = context.Pk,
                        Sk = context.Sk,
                        Pd = context.Pd
                    });
            }
            else
            {
                var ii = context.I.Value;

                var item = StochasticIndicator.Calculate(new StochasticIndicatorContext
                {
                    Rates = context.Rates,
                    Pk = context.Pk,
                    Sk = context.Sk,
                    Pd = context.Pd
                }, ii);

                return new IndexedList<StochasticIndicatorData>(item);
            }
        }
    }
}
