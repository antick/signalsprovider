﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.Indicators
{
    public interface IStochasticIndicatorProvider
    {
        IndexedList<StochasticIndicatorData> Do(StochasticIndicatorProviderContext context);
    }
}
