﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;

namespace Antick.SignalsProvider.Indicators.Types
{
    public class StochasticIndicatorProviderContext
    {
        /// <summary>
        /// Рейты
        /// </summary>
        public  List<Rate> Rates { get; set; }
        /// <summary>
        /// Основной параметр стохастика
        /// </summary>
        public int Pk { get; set; }
        /// <summary>
        /// Основной параметр стохастика
        /// </summary>
        public int Sk { get; set; }
        /// <summary>
        /// Основной параметр стохастика
        /// </summary>
        public int Pd { get; set; }
        /// <summary>
        /// Индекс бара внутри Rates на котором нужно выполнить расчет(если не задан - будет выполнен на всех данных)
        /// </summary>
        public int? I { get; set; }
    }
}
