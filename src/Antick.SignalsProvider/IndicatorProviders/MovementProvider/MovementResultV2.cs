﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.MovementProvider
{
    public class MovementResultV2
    {
        public IndexedList<StochasticZoneType> PivotZones { get; set; }
        public MovementDynamicValuesV2 DynamicValues { get; set; }
    }
}
