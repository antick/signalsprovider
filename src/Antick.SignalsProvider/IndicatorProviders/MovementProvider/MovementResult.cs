﻿using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.MovementProvider
{
    public class MovementResult
    {
        public IndexedList<StochasticZoneType> PivotZones { get; set; }

        public MovementDynamicValues DynamicValues { get; set; }
    }
}
