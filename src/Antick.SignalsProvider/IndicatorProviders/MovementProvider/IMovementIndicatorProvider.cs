﻿namespace Antick.SignalsProvider.IndicatorProviders.MovementProvider
{
    public interface IMovementIndicatorProvider
    {
        MovementResult Do(MovementIndicatorProviderContext context);

        MovementResultV2 Do(MovementIndicatorProviderContextV2 context);
    }
}
