﻿using System;
using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.MovementProvider
{
    public class MovementIndicatorProviderContext
    {
        /// <summary>
        /// Котировки
        /// </summary>
        public  List<Rate> Rates { get; set; }
        
        /// <summary>
        /// Расчитенные данные SMA
        /// </summary>
        public IndexedList<double> MaValues { get; set; }

        /// <summary>
        /// Расчетнные переходы стохастика
        /// </summary>
        public IndexedList<StochasticZoneType> StochasticZoneValues { get; set; }

        /// <summary>
        /// Список с УЖЕ существующими движениями (сюда же они будут дописываться)
        /// </summary>
        public List<Movement> Movemements { get; set; }

        /// <summary>
        /// Настройки индикатора с предыдущего цикла обсчета
        /// </summary>
        public MovementDynamicValues MovemementParams { get; set; }

        /// <summary>
        /// Колбэк при добавлении нового движения
        /// </summary>
        public Action<MovementIndicatorAddedEventArgs> CallbackAdded { get; set; }

        /// <summary>
        /// Колбэк при обновлении движения
        /// </summary>
        public Action<MovementIndicatorUpdateEventArgs> CallBackUpdated { get; set; }

        /// <summary>
        /// Индекс рейта, на котором нужно выполнить обсчет ( если не задан -обсчет идет по всем данным )
        /// </summary>
        public long? Index { get; set; }
    }
}
