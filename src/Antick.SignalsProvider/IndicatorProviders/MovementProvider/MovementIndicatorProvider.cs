﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.MovementProvider
{
    public class MovementIndicatorProvider : IMovementIndicatorProvider
    {
        public MovementResult Do(MovementIndicatorProviderContext context)
        {
            List<IndexValue<StochasticZoneType>> stochZonesUsed = new List<IndexValue<StochasticZoneType>>();

            var movementIndicator = new MovementIndicator(new MovementContext
            {
                Candles = context.Rates,
                MaValues = context.MaValues,
                StochZones = context.StochasticZoneValues
            }, context.MovemementParams);

            movementIndicator.Added += (sender, args) =>
            {
                context.Movemements.Add(args.Movement);
                context.CallbackAdded?.Invoke(args);
            };

            movementIndicator.Updated += (sender, arg) =>
            {
                var updatedEntity = context.Movemements.FirstOrDefault(p => p.Index == arg.MovementIndex) ??
                                    throw new Exception(
                                        "Ошибка в алгоритме поиске обновления движения - обновляемое вдижене не найдено");

                updatedEntity.EndBarIndex = arg.MovementEndBarIndex;
                updatedEntity.EndPrice = arg.MovementEndPrice;

                context.CallBackUpdated?.Invoke(arg);
            };

            movementIndicator.StochasticPivot += (sender, args) =>
            {
                stochZonesUsed.Add(new IndexValue<StochasticZoneType> { Value = args.ZoneType, Index = args.Index });
            };

            if (context.Index == null)
            {
                for (int i = 0; i < context.Rates.Count; i++)
                {
                    movementIndicator.CalculateLogic(context.Rates[i].Index);
                }
            }
            else
            {
                movementIndicator.CalculateLogic(context.Index.Value);
            }

            return new MovementResult
            {
                PivotZones = new IndexedList<StochasticZoneType>(stochZonesUsed),
                DynamicValues = movementIndicator.GetDynamicValues()
            };
        }

        public MovementResultV2 Do(MovementIndicatorProviderContextV2 context)
        {
            List<IndexValue<StochasticZoneType>> stochZonesUsed = new List<IndexValue<StochasticZoneType>>();

            var movementIndicator = new MovementIndicatorV2(new MovementContextV2
            {
                Candles = context.Rates,
                MaFastValues = context.MaValues.ToList()
            }, context.MovemementParams);

            movementIndicator.Added += (sender, args) =>
            {
                context.Movemements.Add(args.Movement);
                context.CallbackAdded?.Invoke(args);
            };

            movementIndicator.Updated += (sender, arg) =>
            {
                var updatedEntity = context.Movemements.FirstOrDefault(p => p.Index == arg.MovementIndex) ??
                                    throw new Exception(
                                        "Ошибка в алгоритме поиске обновления движения - обновляемое вдижене не найдено");

                updatedEntity.EndBarIndex = arg.MovementEndBarIndex;
                updatedEntity.EndPrice = arg.MovementEndPrice;

                context.CallBackUpdated?.Invoke(arg);
            };

            movementIndicator.StochasticPivot += (sender, args) =>
            {
                stochZonesUsed.Add(new IndexValue<StochasticZoneType> { Value = args.ZoneType, Index = args.Index });
            };

            if (context.Index == null)
            {
                for (int i = 0; i < context.Rates.Count; i++)
                {
                    movementIndicator.CalculateLogic(context.Rates[i].Index);
                }
            }
            else
            {
                movementIndicator.CalculateLogic(context.Index.Value);
            }

            return new MovementResultV2
            {
                PivotZones = new IndexedList<StochasticZoneType>(stochZonesUsed),
                DynamicValues = movementIndicator.GetDynamicValues()
            };
        }
    }
}
