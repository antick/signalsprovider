﻿using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.SmaProvider
{
    public interface ISmaIndicatorProvider
    {
        IndexedList<double> Do(SmaIndicatorProviderContext context);
    }
}
