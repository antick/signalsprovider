﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;

namespace Antick.SignalsProvider.IndicatorProviders.SmaProvider
{
    public class SmaIndicatorProviderContext
    {
        /// <summary>
        /// Рейты
        /// </summary>
        public  List<Rate> Rates { get; set; }


        /// <summary>
        /// размер шага таймфрейма в мажорных пунктах
        /// </summary>
        public int TimeFrameValue { get; set; }

        /// <summary>
        /// кол-во мажорных знаков символа
        /// </summary>
        public int Digits { get; set; }

        /// <summary>
        /// Период усреднения SMA
        /// </summary>
        public int Period { get; set; }

        /// <summary>
        /// Индекс элемента внутри Rates - на котором нужно выполнить расчет (если не задан - выполняет полный расчет)
        /// </summary>
        public int? I { get; set; }
    }
}
