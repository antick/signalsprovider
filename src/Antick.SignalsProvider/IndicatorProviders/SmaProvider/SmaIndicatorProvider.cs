﻿using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;

namespace Antick.SignalsProvider.IndicatorProviders.SmaProvider
{
    public class SmaIndicatorProvider : ISmaIndicatorProvider
    {
        public IndexedList<double> Do(SmaIndicatorProviderContext context)
        {
            if (context.I == null)
            {

                var list = SmaIndicator.Calculate(context.Rates, context.Period, context.Digits, context.TimeFrameValue);
                return new IndexedList<double>(list);
            }
            else
            {
                var j = context.I.Value;

                var item = new IndexValue<double>
                {
                    Index = context.Rates[j].Index,
                    Value = SmaIndicator.Calculate(context.Rates, context.Period, j, context.Digits, context.TimeFrameValue)
                };

                return new IndexedList<double>(item);
            }
        }
    }
}
