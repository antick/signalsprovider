﻿using Antick.SignalsProvider.IndicatorProviders.MovementProvider;
using Antick.SignalsProvider.IndicatorProviders.SmaProvider;
using Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider;
using Antick.SignalsProvider.Indicators;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.Types;

namespace Antick.SignalsProvider.IndicatorProviders
{
    public class IndicatorProvider : IIndicatorProvider
    {
        private readonly IMovementIndicatorProvider m_MovementIndicatorProvider;
        private readonly ISmaIndicatorProvider m_SmaIndicatorProvider;
        private readonly IStochasticIndicatorProvider m_StochasticIndicatorProvider;
        private readonly IStochasticZoneIndicatorProvider m_StochasticZoneIndicatorProvider;

        public IndicatorProvider(IMovementIndicatorProvider movementIndicatorProvider, ISmaIndicatorProvider smaIndicatorProvider,
            IStochasticIndicatorProvider stochasticIndicatorProvider, IStochasticZoneIndicatorProvider stochasticZoneIndicatorProvider)
        {
            m_MovementIndicatorProvider = movementIndicatorProvider;
            m_SmaIndicatorProvider = smaIndicatorProvider;
            m_StochasticIndicatorProvider = stochasticIndicatorProvider;
            m_StochasticZoneIndicatorProvider = stochasticZoneIndicatorProvider;
        }

        public IndexedList<double> Sma(SmaIndicatorProviderContext context)
        {
            return m_SmaIndicatorProvider.Do(context);
        }

        public IndexedList<StochasticIndicatorData> Stochastic(StochasticIndicatorProviderContext context)
        {
            return m_StochasticIndicatorProvider.Do(context);
        }

        public StochasticZoneResult StochasticZone(StochasticZoneIndicatorProviderContext context)
        {
            return m_StochasticZoneIndicatorProvider.Do(context);
        }

        public MovementResult Movement(MovementIndicatorProviderContext context)
        {
            return m_MovementIndicatorProvider.Do(context);
        }

        public MovementResultV2 MovementV2(MovementIndicatorProviderContextV2 context)
        {
            return m_MovementIndicatorProvider.Do(context);
        }
    }
}
