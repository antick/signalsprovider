﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveGetPriceQuery : IQuery<WaveGetPrice, double>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveGetPriceQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public double Ask(WaveGetPrice criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;

            if (criterion.Seq == SeqType.Start)
            {
                var firstMovement = m_QueryBuilder.For<Movement>()
                    .With(new WaveGetChildMovement(ptf, wave, OrderType.First));

                return firstMovement.StartPrice;
            }
            else
            {
                var lastMovement = m_QueryBuilder.For<Movement>()
                    .With(new WaveGetChildMovement(ptf, wave, OrderType.Last));

                return lastMovement.EndPrice;
            }

        
        }
    }
}
