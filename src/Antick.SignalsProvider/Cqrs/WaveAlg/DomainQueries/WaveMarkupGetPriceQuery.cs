﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGetPriceQuery : IQuery<WaveMarkupGetPrice, double>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGetPriceQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public double Ask(WaveMarkupGetPrice criterion)
        {
            var ptf = criterion.Ptf;
            var waveMarkup = criterion.WaveMarkup;

            if (criterion.Seq == SeqType.Start)
            {
                var firstMovement = m_QueryBuilder.For<Movement>()
                    .With(new WaveMarkupGetChildMovement(ptf, waveMarkup, OrderType.First));

                return firstMovement.StartPrice;
            }
            else
            {
                var lastMovement = m_QueryBuilder.For<Movement>()
                    .With(new WaveMarkupGetChildMovement(ptf, waveMarkup, OrderType.Last));

                return lastMovement.EndPrice;
            }
        }
    }
}
