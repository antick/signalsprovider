﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGeteDirectionQuery : IQuery<WaveMarkupGetDirection, DirectionType>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGeteDirectionQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public DirectionType Ask(WaveMarkupGetDirection criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;

            var startPrice = m_QueryBuilder.For<double>()
                .With(new WaveMarkupGetPrice(ptf, wave, SeqType.Start));
            var endPrice = m_QueryBuilder.For<double>()
                .With(new WaveMarkupGetPrice(ptf, wave, SeqType.End));

            return startPrice < endPrice ? DirectionType.Up : DirectionType.Down;
        }
    }
}
