﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupIsSimpleQuery : IQuery<WaveMarkupIsSimple, bool>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupIsSimpleQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public bool Ask(WaveMarkupIsSimple criterion)
        {
            var ptf = criterion.Ptf;
            var waveMarkup = criterion.WaveMarkup;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            return wtf.WaveMarkups.All(p => p.ParentId != waveMarkup.Id) && waveMarkup.ParentId == null && waveMarkup.MovementId != null;
        }
    }
}
