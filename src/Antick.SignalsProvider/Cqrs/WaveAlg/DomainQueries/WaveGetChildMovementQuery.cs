﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveGetChildMovementQuery : IQuery<WaveGetChildMovement, Movement>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveGetChildMovementQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Movement Ask(WaveGetChildMovement criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;

            var hasChildren = m_QueryBuilder.For<bool>()
                .With(new WaveHasChildren(ptf, wave));

            if (hasChildren)
            {
                var child = m_QueryBuilder.For<Wave>()
                    .With(new WaveGetChild(ptf, wave, criterion.OrderType));

                var childMovement = m_QueryBuilder.For<Movement>()
                    .With(new WaveGetChildMovement(ptf, child, criterion.OrderType));

                return childMovement;
            }

            var movement = m_QueryBuilder.For<Movement>()
                .With(new WaveGetMovement(ptf, wave));

            return movement;
        }
    }
}
