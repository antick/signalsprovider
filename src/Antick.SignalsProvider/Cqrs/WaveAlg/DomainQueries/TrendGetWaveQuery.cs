﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class TrendGetWaveQuery : IQuery<TrendGetWave, Wave>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public TrendGetWaveQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Wave Ask(TrendGetWave criterion)
        {
            var ptf = criterion.Ptf;
            var trend = criterion.Trend;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            if (criterion.Seq == SeqType.Start)
            {
                var startWave = wtf.Waves.FirstOrDefault(p => p.Id == trend.EndWaveId);
                return startWave;
            }
            else
            {
                var endWave = wtf.Waves.FirstOrDefault(p => p.Id == trend.EndWaveId);

                return endWave;
            }
          
        }
    }
}
