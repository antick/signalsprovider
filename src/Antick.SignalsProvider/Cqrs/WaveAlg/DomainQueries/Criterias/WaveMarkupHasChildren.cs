﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupHasChildren : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup Wave { get; set; }

        public WaveMarkupHasChildren(Ptf ptf, WaveMarkup wave)
        {
            Ptf = ptf;
            Wave = wave;
        }
    }
}
