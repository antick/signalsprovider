﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupGetChild : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup WaveMarkup { get; set; }
        public OrderType Order { get; set; }

        public WaveMarkupGetChild(Ptf ptf, WaveMarkup waveMarkup, OrderType order)
        {
            Ptf = ptf;
            WaveMarkup = waveMarkup;
            Order = order;
        }
    }
}
