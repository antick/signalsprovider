﻿using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class RateGetMaSlow : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Rate Rate { get; set; }

        public RateGetMaSlow(Ptf ptf, Rate rate)
        {
            Ptf = ptf;
            Rate = rate;
        }
    }
}
