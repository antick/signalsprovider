﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveGetChild : ICriterion
    {
        public Ptf Ptf { get; set; }

        public Wave Wave { get; set; }

        public OrderType OrderType { get; set; }

        public WaveGetChild(Ptf ptf, Wave wave, OrderType orderType)
        {
            Ptf = ptf;
            Wave = wave;
            OrderType = orderType;
        }
    }
}
