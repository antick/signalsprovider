﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class TrendGetWave : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Trend Trend { get; set; }

        public SeqType Seq { get; set; }

        public TrendGetWave(Ptf ptf, Trend trend, SeqType seq)
        {
            Ptf = ptf;
            Trend = trend;
            Seq = seq;
        }
    }
}
