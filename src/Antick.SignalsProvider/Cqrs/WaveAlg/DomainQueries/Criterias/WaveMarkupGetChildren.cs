﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupGetChildren : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup Wave { get; set; }

        public WaveMarkupGetChildren(Ptf ptf, WaveMarkup wave)
        {
            Ptf = ptf;
            Wave = wave;
        }
    }
}
