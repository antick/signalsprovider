﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupGetChildMovement : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup WaveMarkup { get; set; }
        public OrderType Order { get; set; }

        public WaveMarkupGetChildMovement(Ptf ptf, WaveMarkup waveMarkup, OrderType orderType)
        {
            Ptf = ptf;
            WaveMarkup = waveMarkup;
            Order = orderType;
        }
    }
}
