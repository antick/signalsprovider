﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class MovementGetMaSlow : ICriterion 
    {
        public Ptf Ptf { get; set; }
        public Movement Movement { get; set; }
        public OrderType OrderType { get; set; }

        public MovementGetMaSlow(Ptf ptf, Movement movement, OrderType orderType)
        {
            Ptf = ptf;
            Movement = movement;
            OrderType = orderType;
        }
    }
}
