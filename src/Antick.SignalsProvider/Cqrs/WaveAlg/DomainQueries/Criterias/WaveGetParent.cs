﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveGetParent : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Wave Wave { get; set; }

        public WaveGetParent(Ptf ptf, Wave wave)
        {
            Ptf = ptf;
            Wave = wave;
        }
    }
}
