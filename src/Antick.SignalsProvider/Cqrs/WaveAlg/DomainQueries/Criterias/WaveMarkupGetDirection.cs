﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupGetDirection : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup Wave { get; set; }

        public WaveMarkupGetDirection(Ptf ptf, WaveMarkup wave)
        {
            Ptf = ptf;
            Wave = wave;
        }

    }
}
