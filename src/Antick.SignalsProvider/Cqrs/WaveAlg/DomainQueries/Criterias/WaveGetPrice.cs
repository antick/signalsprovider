﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveGetPrice : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Wave Wave { get; set; }

        public SeqType Seq { get; set; }

        public WaveGetPrice(Ptf ptf, Wave wave, SeqType seq)
        {
            Ptf = ptf;
            Wave = wave;
            Seq = seq;
        }
    }
}
