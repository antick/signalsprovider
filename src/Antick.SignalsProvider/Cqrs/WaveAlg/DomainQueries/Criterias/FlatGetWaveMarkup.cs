﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class FlatGetWaveMarkup : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Flat Flat { get; set; }

        public FlatGetWaveMarkup(Ptf ptf, Flat flat)
        {
            Ptf = ptf;
            Flat = flat;
        }
    }
}
