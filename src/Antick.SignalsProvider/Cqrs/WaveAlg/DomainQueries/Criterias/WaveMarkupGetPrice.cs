﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupGetPrice : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup WaveMarkup { get; set; }
        public SeqType Seq { get; set; }

        public WaveMarkupGetPrice(Ptf ptf, WaveMarkup waveMarkup, SeqType seq)
        {
            Ptf = ptf;
            WaveMarkup = waveMarkup;
            Seq = seq;
        }
    }
}
