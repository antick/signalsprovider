﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class MovementIsWholeMaSlow : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Movement Movement { get; set; }
        public DirectionType DirectionType { get; set; }

        public MovementIsWholeMaSlow(Ptf ptf, Movement movement, DirectionType directionType)
        {
            Ptf = ptf;
            Movement = movement;
            DirectionType = directionType;
        }
    }
}
