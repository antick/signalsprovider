﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class FlatGetWaveMarkupPrice : ICriterion
    {
        public Ptf Ptf { get; set; }
        public Flat Flat { get; set; }
        public SeqType Seq { get; set; }

        public FlatGetWaveMarkupPrice(Ptf ptf, Flat flat, SeqType seq)
        {
            Ptf = ptf;
            Flat = flat;
            Seq = seq;
        }
    }
}
