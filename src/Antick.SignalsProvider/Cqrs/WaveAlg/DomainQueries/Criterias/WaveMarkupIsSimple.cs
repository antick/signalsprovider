﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias
{
    public class WaveMarkupIsSimple : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup WaveMarkup { get; set; }

        public WaveMarkupIsSimple(Ptf ptf, WaveMarkup waveMarkup)
        {
            Ptf = ptf;
            WaveMarkup = waveMarkup;
        }
    }
}
