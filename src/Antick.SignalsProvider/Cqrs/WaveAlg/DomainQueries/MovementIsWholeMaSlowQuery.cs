﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    /// <summary>
    /// Определяет полностью ли выше или ниже движение относительно медленной скользящей средней
    /// </summary>
    public class MovementIsWholeMaSlowQuery : IQuery<MovementIsWholeMaSlow, bool>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public MovementIsWholeMaSlowQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public bool Ask(MovementIsWholeMaSlow criterion)
        {
            var ptf = criterion.Ptf;
            var movement = criterion.Movement;

            var movementStartMaSlow =
                m_QueryBuilder.For<double>().With(new MovementGetMaSlow(ptf, movement, OrderType.First));
            var movementEndMaSlow =
                m_QueryBuilder.For<double>().With(new MovementGetMaSlow(ptf, movement, OrderType.Last));

            if (criterion.DirectionType == DirectionType.Up)
            {
                var condition = movement.StartPrice >= movementStartMaSlow && movement.EndPrice > movementEndMaSlow;
                return condition;
            }
            else
            {
                var condition = movement.StartPrice <= movementStartMaSlow && movement.EndPrice < movementEndMaSlow;
                return condition;
            }
        }
    }
}
