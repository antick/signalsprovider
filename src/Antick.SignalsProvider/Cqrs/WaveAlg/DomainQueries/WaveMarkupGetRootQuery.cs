﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGetRootQuery : IQuery<WaveMarkupGetRoot, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGetRootQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(WaveMarkupGetRoot criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;

            if (wave.ParentId == null)
                return wave;

            var parent = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetParent(ptf, wave));
            var root = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetRoot(ptf, parent));
            return root;

        }
    }
}
