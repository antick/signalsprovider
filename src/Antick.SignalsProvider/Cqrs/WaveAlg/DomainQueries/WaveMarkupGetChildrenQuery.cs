﻿using System.Collections.Generic;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGetChildrenQuery : IQuery<WaveMarkupGetChildren, List<WaveMarkup>>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGetChildrenQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public List<WaveMarkup> Ask(WaveMarkupGetChildren criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var result = wtf.WaveMarkups.Where(p => p.ParentId == wave.ParentId).OrderBy(p => p.Index).ToList();

            return result;
        }
    }
}
