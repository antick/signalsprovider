﻿using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    /// <summary>
    /// Возвращает значение скользящей средней на баре старта или конца движения
    /// </summary>
    public class MovementGetMaSlowQuery : IQuery<MovementGetMaSlow, double>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public MovementGetMaSlowQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public double Ask(MovementGetMaSlow criterion)
        {
            var ptf = criterion.Ptf;
            var movement = criterion.Movement;
            var orderType = criterion.OrderType;

            var rate = m_QueryBuilder.For<Rate>().With(new MovementGetRate(ptf, movement, orderType));

            var maSlow = m_QueryBuilder.For<double>().With(new RateGetMaSlow(ptf, rate));

            return maSlow;
        }
    }
}
