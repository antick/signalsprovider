﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGetChildQuery : IQuery<WaveMarkupGetChild, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGetChildQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }
        public WaveMarkup Ask(WaveMarkupGetChild criterion)
        {
            var ptf = criterion.Ptf;
            var waveMarkup = criterion.WaveMarkup;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            if (criterion.Order == OrderType.First)
            {
                var firstChild = wtf.WaveMarkups.Where(p => p.ParentId == waveMarkup.Id).OrderBy(p => p.Index).FirstOrDefault();
                return firstChild;
            }
            else
            {
                var lastChild = wtf.WaveMarkups.Where(p => p.ParentId == waveMarkup.Id).OrderByDescending(p => p.Index).FirstOrDefault();
                return lastChild;
            }
        }
    }
}
