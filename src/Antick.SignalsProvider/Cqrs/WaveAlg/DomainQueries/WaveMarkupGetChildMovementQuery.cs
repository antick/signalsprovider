﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGetChildMovementQuery : IQuery<WaveMarkupGetChildMovement, Movement>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGetChildMovementQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Movement Ask(WaveMarkupGetChildMovement criterion)
        {
            var ptf = criterion.Ptf;
            var waveMarkup = criterion.WaveMarkup;
            var order = criterion.Order;

            var hasChildren = m_QueryBuilder.For<bool>()
                .With(new WaveMarkupHasChildren(ptf, waveMarkup));

            if (hasChildren)
            {
                var childWave = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetChild(ptf, waveMarkup, order));

                var childMovement = m_QueryBuilder.For<Movement>()
                    .With(new WaveMarkupGetChildMovement(ptf, childWave, order));

                return childMovement;
            }

            var movement = m_QueryBuilder.For<Movement>()
                .With(new WaveMarkupGetMovement(ptf, waveMarkup));

            return movement;

        }
    }
}
