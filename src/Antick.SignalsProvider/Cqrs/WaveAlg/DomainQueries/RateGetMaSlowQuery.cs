﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class RateGetMaSlowQuery : IQuery<RateGetMaSlow, double>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public RateGetMaSlowQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public double Ask(RateGetMaSlow criterion)
        {
            var ptf = criterion.Ptf;
            var rate = criterion.Rate;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var maSlow = wtf.MaSlow.FirstOrDefault(p => p.Index == rate.Index);

            return maSlow.Value;
        }
    }
}
