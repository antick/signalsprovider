﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveGetChildQuery : IQuery<WaveGetChild, Wave>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveGetChildQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Wave Ask(WaveGetChild criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            if (criterion.OrderType == OrderType.First)
            {
                var firstChild = wtf.Waves.Where(p => p.ParentId == wave.Id).OrderBy(p => p.Index).FirstOrDefault();

                return firstChild;
            }
            else
            {
                var lastChild = wtf.Waves.Where(p => p.ParentId == wave.Id).OrderByDescending(p => p.Index).FirstOrDefault();

                return lastChild;
            }
        }
    }
}
