﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class TrendGetPriceQuery : IQuery<TrendGetPrice, double>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public TrendGetPriceQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public double Ask(TrendGetPrice criterion)
        {
            var ptf = criterion.Ptf;
            var trend = criterion.Trend;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            if (criterion.Seq == SeqType.Start)
            {
                var startWave = wtf.Waves.FirstOrDefault(p => p.Id == trend.StartWaveId);
                var startPrice = m_QueryBuilder.For<double>().With(new WaveGetPrice(ptf, startWave, SeqType.Start));
                return startPrice;
            }
            else
            {
                var endWave = wtf.Waves.FirstOrDefault(p => p.Id == trend.EndWaveId);
                var endPrice = m_QueryBuilder.For<double>().With(new WaveGetPrice(ptf, endWave, SeqType.End));
                return endPrice;
            }
        }
    }
}
