﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveGetRootQuery : IQuery<WaveGetRoot, Wave>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveGetRootQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Wave Ask(WaveGetRoot criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;

            if (wave.ParentId == null)
                return wave;

            var parent = m_QueryBuilder.For<Wave>().With(new WaveGetParent(ptf, wave));
            var root = m_QueryBuilder.For<Wave>().With(new WaveGetRoot(ptf, parent));
            return root;

        }
    }
}
