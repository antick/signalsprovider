﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    public class WaveMarkupGetNextWaveQuery : IQuery<WaveMarkupGetNextWave, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveMarkupGetNextWaveQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(WaveMarkupGetNextWave criterion)
        {
            var ptf = criterion.Ptf;
            var wave = criterion.Wave;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var nextWave = wtf.WaveMarkups.FirstOrDefault(p => 
                !p.IsCompleted &&
                p.TrendType == wave.TrendType && 
                p.ParentId == wave.ParentId &&
                p.Index == wave.Index + 1);

            return nextWave;
        }
    }
}
