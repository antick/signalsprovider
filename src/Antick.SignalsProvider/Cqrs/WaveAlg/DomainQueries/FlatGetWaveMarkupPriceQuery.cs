﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    /// <summary>
    /// Возвращает цену старта или конца, разметочной волны, по которой образовался Флэт.
    /// </summary>
    public class FlatGetWaveMarkupPriceQuery : IQuery<FlatGetWaveMarkupPrice, double>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public FlatGetWaveMarkupPriceQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public double Ask(FlatGetWaveMarkupPrice criterion)
        {
            var ptf = criterion.Ptf;
            var flat = criterion.Flat;
            var seq = criterion.Seq;

            var waveMarkup = m_QueryBuilder.For<WaveMarkup>().With(new FlatGetWaveMarkup(ptf, flat));
            var price = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(ptf, waveMarkup, seq));
            return price;

        }
    }
}
