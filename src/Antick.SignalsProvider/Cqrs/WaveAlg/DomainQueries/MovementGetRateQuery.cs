﻿using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    /// <summary>
    /// Возвращает бар старта или конца движения
    /// </summary>
    public class MovementGetRateQuery : IQuery<MovementGetRate, Rate>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public MovementGetRateQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Rate Ask(MovementGetRate criterion)
        {
            var ptf = criterion.Ptf;
            var movement = criterion.Movement;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            if (criterion.OrderType == OrderType.First)
            {
                var rate = wtf.Rates.FirstOrDefault(p => p.Index == movement.StartBarIndex);
                return rate;
            }
            else
            {
                var rate = wtf.Rates.FirstOrDefault(p => p.Index == movement.EndBarIndex);
                return rate;
            }
        }
    }
}
