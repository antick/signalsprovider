﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries
{
    /// <summary>
    /// Возвращает разметочную волну, по которой был построен флэт
    /// </summary>
    public class FlatGetWaveMarkupQuery : IQuery<FlatGetWaveMarkup, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public FlatGetWaveMarkupQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(FlatGetWaveMarkup criterion)
        {
            var ptf = criterion.Ptf;
            var flat = criterion.Flat;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var waveMarkup = wtf.WaveMarkups.FirstOrDefault(p => p.Id == flat.WaveMarkupId);
            return waveMarkup;
        }
    }
}
