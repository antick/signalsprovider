﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg
{
    public class GetWaveParametersV1 : ICriterion
    {
        public Ptf Ptf;

        public GetWaveParametersV1(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
