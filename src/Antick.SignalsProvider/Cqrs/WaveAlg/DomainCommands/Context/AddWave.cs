﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context
{
    public class AddWave : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public Wave Wave { get; set; }

        public AddWave(Ptf ptf, Wave wave)
        {
            Ptf = ptf;
            Wave = wave;
        }
    }
}
