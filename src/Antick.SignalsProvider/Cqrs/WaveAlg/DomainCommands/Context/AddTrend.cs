﻿using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context
{
    public class AddTrend : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public Trend Trend { get; set; }


        public AddTrend(Ptf ptf, Trend trend)
        {
            Ptf = ptf;
            Trend = trend;
        }
    }
}
