﻿using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context
{
    public class AddWaveMarkup : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public WaveMarkup WaveMarkup { get; set; }

        public AddWaveMarkup(Ptf ptf, WaveMarkup waveMarkup)
        {
            Ptf = ptf;
            WaveMarkup = waveMarkup;
        }
    }
}
