﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Contracts.Specs;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core;
using Antick.SignalsProvider.IndicatorProviders;
using Antick.SignalsProvider.IndicatorProviders.MovementProvider;
using Antick.SignalsProvider.IndicatorProviders.SmaProvider;
using Antick.SignalsProvider.IndicatorProviders.StochasticZoneProvider;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers;
using Antick.SignalsProvider.Providers.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg
{
    public class ProccessingWaveV1Command : ICommand<ProccessingWaveV1Context>
    {
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IIndicatorProvider m_IndicatorProvider;

        private WaveV1DataModel m_DataModel;
        private WaveV1Parameters m_Parameters;
        private Ptf m_Ptf;
        private bool m_UseWaveAlg;
        private InstrumentSpecItem m_Spec;

        public ProccessingWaveV1Command(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, IIndicatorProvider indicatorProvider)
        {
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
            m_IndicatorProvider = indicatorProvider;
        }

        public void Execute(ProccessingWaveV1Context criterion)
        {
            m_Ptf = criterion.Ptf;
            m_DataModel = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));
            m_Parameters = m_QueryBuilder.For<WaveV1Parameters>().With(new GetWaveParametersV1(m_Ptf));
            m_UseWaveAlg = criterion.UseWaveAlg;

            Instrument instrument = (Instrument)Enum.Parse(typeof(Instrument), m_Ptf.Instrument.Name);
            m_Spec = InstrumentSpec.Get(instrument);

            exec(criterion.Rates);
        }

        private void exec(List<Rate> rates)
        {
            if (m_DataModel.TfLastRateIndex == null)
            {
                execAllData(rates); // вычисление "скопом"  - сразу всех данных
            }
            else
            { 
                execPerData(rates); // вычисление "по элементно" - вычисляет по каждому записи баров
            }
        }

        private void execAllData(List<Rate> rates)
        {
            m_DataModel.Rates = rates;
            m_DataModel.MaFast = ma(m_Parameters.MaFastPeriod);
            m_DataModel.MaSlow = ma(m_Parameters.MaSlowPeriod);
            m_DataModel.MaVerySlow = ma(m_Parameters.MaVerySlowPeriod);
            m_DataModel.Stochastic = stochastic();

            var stochasticZoneResult = stochasticZone();
            m_DataModel.StochasticZone = stochasticZoneResult.Values;
            m_DataModel.StochasticZoneDynamicValues = stochasticZoneResult.DynamicValues;

            var movementsResult = movement();
            m_DataModel.PivotZones = movementsResult.PivotZones;
            m_DataModel.MovementDynamicValues = movementsResult.DynamicValues;
        }

        private void execPerData(List<Rate> rates)
        {
            var nextRateIndex = m_DataModel.Rates.Count;

            m_DataModel.Rates.AddRange(rates);

            for (int k = 0; k < rates.Count; k++)
            {
                var i = nextRateIndex + k;
                var rate = m_DataModel.Rates[i];

                execWaveAnalysis(WaveEventType.NewRate, rate.Index);

                m_DataModel.MaFast.AddRange(ma(m_Parameters.MaFastPeriod, i));
                m_DataModel.MaSlow.AddRange(ma(m_Parameters.MaSlowPeriod, i));
                m_DataModel.MaVerySlow.AddRange(ma(m_Parameters.MaVerySlowPeriod, i));
                m_DataModel.Stochastic.AddRange(stochastic(i));

                var stochasticZoneResult = stochasticZone(i);
                m_DataModel.StochasticZone.AddRange(stochasticZoneResult.Values);
                m_DataModel.StochasticZoneDynamicValues = stochasticZoneResult.DynamicValues;

                var movementResul = movement(rate.Index);
                m_DataModel.PivotZones.AddRange(movementResul.PivotZones);
                m_DataModel.MovementDynamicValues = movementResul.DynamicValues;
            }
        }

        private IndexedList<double> ma(int period, int? i = null)
        {
            return m_IndicatorProvider.Sma(new SmaIndicatorProviderContext
            {
                Rates = m_DataModel.Rates,
                TimeFrameValue = Convert.ToInt32(m_Ptf.TimeFrame.Value),
                Digits = m_Spec.Digits,
                Period = period,
                I = i
            });
        }

        private StochasticZoneResult stochasticZone(int? i = null)
        {
            return m_IndicatorProvider.StochasticZone(new StochasticZoneIndicatorProviderContext
            {
                Rates = m_DataModel.Rates,
                Stochastic = m_DataModel.Stochastic,
                StochasticParams = m_DataModel.StochasticZoneDynamicValues,
                I = i
            });
        }

        private IndexedList<StochasticIndicatorData> stochastic(int? i = null)
        {
            return m_IndicatorProvider.Stochastic(new StochasticIndicatorProviderContext
            {
                Rates = m_DataModel.Rates,
                Pk = m_Parameters.StochasticPk,
                Sk = m_Parameters.StochasticSk,
                Pd = m_Parameters.StochasticPd,
                I = i
            });
        }

        private MovementResult movement(long? index = null)
        {
            var context = new MovementIndicatorProviderContext
            {
                Rates = m_DataModel.Rates,
                MaValues = m_DataModel.MaFast,
                StochasticZoneValues = m_DataModel.StochasticZone,
                Movemements = m_DataModel.Movements,
                MovemementParams = m_DataModel.MovementDynamicValues,
                CallbackAdded = args => { execWaveAnalysis(WaveEventType.AddMovement, args.Index); },
                CallBackUpdated = args => { execWaveAnalysis(WaveEventType.UpdateMovement, args.Index); },
                Index = index
            };

            var result = m_IndicatorProvider.Movement(context);

            return result;
        }

        private void execWaveAnalysis(WaveEventType eventType, long index)
        {
            if (!m_UseWaveAlg)
                return;

            m_CommandBuilder.Execute(new WaveAnalysisContext(m_Ptf, eventType, index));
        }

    }
}

