﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetReverseWaveBContext : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public long WaveBId { get; set; }

        public SetReverseWaveBContext(Ptf ptf, long waveBId)
        {
            Ptf = ptf;
            WaveBId = waveBId;
        }
    }
}
