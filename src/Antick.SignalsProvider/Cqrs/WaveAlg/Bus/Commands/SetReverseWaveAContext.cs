﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetReverseWaveAContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public long MarkupWaveId { get; set; }

        public bool CrossMaSlow { get; set; }

        public SetReverseWaveAContext(Ptf ptf, long markupWaveId, bool crossMaSlow = true)
        {
            Ptf = ptf;
            MarkupWaveId = markupWaveId;
            CrossMaSlow = crossMaSlow;
        }
    }
}
