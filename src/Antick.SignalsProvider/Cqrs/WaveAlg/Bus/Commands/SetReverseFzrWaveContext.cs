﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetReverseFzrWaveContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public long FzrWaveId { get; set; }

        public SetReverseFzrWaveContext(Ptf ptf, long fzrWaveId)
        {
            Ptf = ptf;
            FzrWaveId = fzrWaveId;
        }
    }
}
