﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class ClearPatternDataContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public ClearPatternDataContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
