﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetReverseWaveBCommand : ICommand<SetReverseWaveBContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public SetReverseWaveBCommand(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(SetReverseWaveBContext commandContext)
        {
            var ptf = commandContext.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            wtf.Bus.Reverse.CorrectionId = commandContext.WaveBId;
        }
    }
}
