﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetPatternContext : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public DirectionType Markup { get; set; }
        public PatternType Pattern { get; set; }

        public SetPatternContext(Ptf ptf, DirectionType markup,
            PatternType pattern)
        {
            Ptf = ptf;
            Markup = markup;
            Pattern = pattern;
        }
    }
}
