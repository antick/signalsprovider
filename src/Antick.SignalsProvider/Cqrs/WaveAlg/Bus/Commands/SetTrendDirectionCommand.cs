﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetTrendDirectionCommand : ICommand<SetTrendDirectionContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        public SetTrendDirectionCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(SetTrendDirectionContext commandContext)
        {
            var ptf = commandContext.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            wtf.Bus.Direction = commandContext.Direction;
        }
    }
}
