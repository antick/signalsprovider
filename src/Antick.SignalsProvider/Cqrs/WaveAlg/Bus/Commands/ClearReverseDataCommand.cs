﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class ClearReverseDataCommand : ICommand<ClearReverseDataContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public ClearReverseDataCommand(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(ClearReverseDataContext commandContext)
        {
            var ptf = commandContext.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var reverseData = wtf.Bus.Reverse;
            reverseData.CrossMaSlow = false;
            reverseData.HasFzr = false;
            reverseData.SarBar = -1;
            reverseData.ImpulseId = -1;
            reverseData.CorrectionId = -1;
            reverseData.FzrWaveId = -1;
        }
    }
}
