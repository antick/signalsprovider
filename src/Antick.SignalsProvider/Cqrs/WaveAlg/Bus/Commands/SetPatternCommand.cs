﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetPatternCommand : ICommand<SetPatternContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public SetPatternCommand(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(SetPatternContext commandContext)
        {
            var ptf = commandContext.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            switch (commandContext.Markup)
            {
                case DirectionType.Up:
                    wtf.Bus.Pattern.Up = commandContext.Pattern;
                    break;
                case DirectionType.Down:
                    wtf.Bus.Pattern.Down = commandContext.Pattern;
                    break;
            }
        }
    }
}
