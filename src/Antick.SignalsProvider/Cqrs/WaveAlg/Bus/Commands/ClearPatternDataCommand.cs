﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class ClearPatternDataCommand: ICommand<ClearPatternDataContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public ClearPatternDataCommand(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(ClearPatternDataContext commandContext)
        {
            var ptf = commandContext.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var bus = wtf.Bus;

            bus.Pattern.Up = PatternType.None;
            bus.Pattern.Down = PatternType.None;
        }
    }
}
