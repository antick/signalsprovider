﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands
{
    public class SetTrendDirectionContext : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public DirectionType Direction { get; set; }

        public SetTrendDirectionContext(Ptf ptf, DirectionType direction)
        {
            Ptf = ptf;
            Direction = direction;
        }
    }
}
