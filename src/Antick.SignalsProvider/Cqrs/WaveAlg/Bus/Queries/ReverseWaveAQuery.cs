﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries
{
    public class ReverseWaveAQuery : IQuery<ReverseWaveACriterion, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public ReverseWaveAQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(ReverseWaveACriterion criterion)
        {
            var ptf = criterion.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var impulseId = wtf.Bus.Reverse.ImpulseId;

            if (impulseId == -1)
                return null;

            var waveMarkup = wtf.WaveMarkups.FirstOrDefault(p => p.Id == impulseId);

            return waveMarkup;
        }
    }
}
