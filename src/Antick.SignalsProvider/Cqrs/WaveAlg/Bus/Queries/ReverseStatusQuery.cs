﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries
{
    public class ReverseStatusQuery : IQuery<ReverseStatusCriterion, ReverseStatus>
    {
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ILogger m_Logger;

        public ReverseStatusQuery(IQueryBuilder queryBuilder, ILogger logger)
        {
            m_QueryBuilder = queryBuilder;
            m_Logger = logger;
        }

        public ReverseStatus Ask(ReverseStatusCriterion criterion)
        {
            var ptf = criterion.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));
            
            return Status(wtf.Bus);
        }

        /// <summary>
        /// Статус разворота по типу 1
        /// </summary>
        public ReverseStatus Status(Providers.WaveProvider.Types.Bus bus)
        {
            var obj = bus.Reverse;

            if (obj.CrossMaSlow)
            {
                if (obj.CorrectionId != -1 && obj.SarBar != -1 && obj.HasFzr)
                    return ReverseStatus.Stage4;

                if (obj.CorrectionId != -1 && obj.SarBar == -1 && !obj.HasFzr)
                {
                    return ReverseStatus.Stage2;
                }
                if (obj.SarBar != -1 && !obj.HasFzr)
                {
                    return ReverseStatus.Stage3A;
                }
                else if (obj.HasFzr)
                {
                    return ReverseStatus.Stage3B;
                }

                return ReverseStatus.Stage1;
            }
            return ReverseStatus.None;
        }
    }
}
