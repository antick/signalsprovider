﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries
{
    public class ReverseStatusCriterion : ICriterion
    {
        public Ptf Ptf { get; set; }

        public ReverseStatusCriterion(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
