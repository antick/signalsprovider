﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries
{
    public class PatternQuery : IQuery<PatternCriterion, PatternType>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public PatternQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public PatternType Ask(PatternCriterion criterion)
        {
            var ptf = criterion.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            return criterion.Markup == DirectionType.Up ? wtf.Bus.Pattern.Up : wtf.Bus.Pattern.Down;
        }
    }
}
