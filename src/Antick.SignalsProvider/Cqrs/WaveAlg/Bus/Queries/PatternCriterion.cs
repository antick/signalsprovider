﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries
{
    public class PatternCriterion : ICriterion
    {
        public Ptf Ptf { get; set; }
        public DirectionType Markup { get; set; }

        public PatternCriterion(Ptf ptf, DirectionType markup)
        {
            Ptf = ptf;
            Markup = markup;
        }
    }
}
