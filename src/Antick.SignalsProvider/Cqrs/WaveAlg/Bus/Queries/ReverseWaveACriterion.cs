﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries
{
    public class ReverseWaveACriterion : ICriterion
    {
        public Ptf Ptf { get; set; }

        public ReverseWaveACriterion(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
