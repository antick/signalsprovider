﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg
{
    public class GetWaveDataModelV1 : ICriterion
    {
        public Ptf Ptf;

        public GetWaveDataModelV1(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
