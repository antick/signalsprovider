﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core
{
    public class TryUpdateTrendLogicCommand : ICommand<TryUpdateTrendLogicContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Providers.WaveProvider.Types.Bus m_Bus;
        private Ptf m_Ptf { get; set; }

        public TryUpdateTrendLogicCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(TryUpdateTrendLogicContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));
            m_Bus = m_Wtf.Bus;
          

            logic();
        }

        // Проверяет планы на то что нынешний тренд был продолжен(тогда применяет план по тренду, отменяет другой план)
        private void logic()
        {
            var planWave = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, m_Bus.Direction, OrderType.Last));

            if (planWave != null && canUpdateTrend(planWave))
            {
                applyMarkup(m_Bus.Direction);
                updateTrend();
            }
        }

        /// <summary>
        /// Определяет может ли волна обновить существующий тренд по плану
        /// </summary>
        /// <param name="waveMarkup"></param>
        private bool canUpdateTrend(WaveMarkup waveMarkup)
        {
            var lastTrend = m_Wtf.Trends.Last();

            var waveMarkupEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waveMarkup, SeqType.End));

            var lasttrendEndPrice = m_QueryBuilder.For<double>().With(new TrendGetPrice(m_Ptf, lastTrend, SeqType.End));

            if (waveMarkup.TrendType == DirectionType.Up && waveMarkup.WaveType == WaveType.Impulse &&
                lastTrend.Type == DirectionType.Up)
            {
                return waveMarkupEndPrice > lasttrendEndPrice;
            }
            else if (waveMarkup.TrendType == DirectionType.Down && waveMarkup.WaveType == WaveType.Impulse &&
                     lastTrend.Type == DirectionType.Down)
                return waveMarkupEndPrice < lasttrendEndPrice;

            return false;
        }

        /// <summary>
        /// Обновляет последний тренд - ищет последний импульс в списке факт. волн -> конец тренда
        /// </summary>
        private void updateTrend()
        {
            var lastTrend = m_Wtf.Trends.Last();

            var lastWave = m_QueryBuilder.For<Wave>()
                .With(new WaveSearch(m_Ptf, lastTrend.Type, OrderType.Last){WaveType = WaveType.Impulse});

            lastTrend.EndWaveId = lastWave.Id;

            m_CommandBuilder.Execute(new ClearReverseDataContext(m_Ptf));
        }

        private void applyMarkup(DirectionType markup)
        {
            m_CommandBuilder.Execute(new ApplyMarkupContext(m_Ptf, markup));
        }
    }
}
