﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Handlers;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core
{
    public class WaveAnalysisCommand : ICommand<WaveAnalysisContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IHandlerBuilder m_HandlerBuilder;

        private WaveEventType m_EventType;
        private long m_Index;
        private Ptf m_ptf;
        private WaveV1DataModel m_Wtf;

        public WaveAnalysisCommand(ILogger logger, IQueryBuilder queryProvider, ICommandBuilder commandBuilder, IHandlerBuilder handlerBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryProvider;
            m_CommandBuilder = commandBuilder;
            m_HandlerBuilder = handlerBuilder;
        }

        public void Execute(WaveAnalysisContext commandContext)
        {
            m_ptf = commandContext.Ptf;
            m_EventType = commandContext.EventType;
            m_Index = commandContext.Index;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_ptf));

            logic();
        }

        private void logic()
        {
            // Алгоритм обновления НК в тренде
            if (m_EventType == WaveEventType.NewRate)
            {
                m_CommandBuilder.Execute(new UpdateFlatNkContext(m_ptf));
                return;
            }

            // Алгоритм флэта - вход и выход в обоих разметках
            m_CommandBuilder.Execute(new FlatLogicContext(m_ptf, m_EventType, m_Index));
            
            if (!m_Wtf.Trends.Any())
            {
                if (m_EventType == WaveEventType.AddMovement || m_EventType == WaveEventType.UpdateMovement)
                {
                    // алгоритм создания первого тренда
                    m_CommandBuilder.Execute(new InitTrendContext(m_ptf));
                }
                return;
            }
            else if (m_EventType == WaveEventType.AddMovement)
            {
                // Алгоритм оновленя разметочных волн
                m_CommandBuilder.Execute(new MarkupLogicContext(m_ptf));
            }

            // Алгоритм обновления по текущему тренду
            if (m_EventType == WaveEventType.AddMovement || m_EventType == WaveEventType.UpdateMovement)
            {
                m_CommandBuilder.Execute(new TryUpdateTrendLogicContext(m_ptf, m_EventType, m_Index));
            }

            // Алгоритм разворота активного тренда, сбор признаков и выполнения разворота
            m_CommandBuilder.Execute(new ReversalLogicContext(m_ptf, m_EventType, m_Index));
          
            if (m_EventType == WaveEventType.AddMovement || m_EventType == WaveEventType.UpdateMovement)
            {
                m_CommandBuilder.Execute(new PatternCatchingContext(m_ptf));
            }
        }
    }
}
