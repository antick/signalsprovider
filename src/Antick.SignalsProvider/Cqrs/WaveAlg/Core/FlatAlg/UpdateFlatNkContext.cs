﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg
{
    public class UpdateFlatNkContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public UpdateFlatNkContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
