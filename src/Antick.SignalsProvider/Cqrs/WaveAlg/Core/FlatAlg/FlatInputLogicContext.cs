﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg
{
    public class FlatInputLogicContext : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public WaveEventType EventType { get; set; }
        public long Index { get; set; }
        public DirectionType Markup { get; set; }

        public FlatInputLogicContext(Ptf ptf, WaveEventType eventType, long index, DirectionType markup)
        {
            Ptf = ptf;
            EventType = eventType;
            Index = index;
            Markup = markup;
        }
    }
}
