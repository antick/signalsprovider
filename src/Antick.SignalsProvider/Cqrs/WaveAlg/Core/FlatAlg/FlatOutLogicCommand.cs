﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg
{
    public class FlatOutLogicCommand : ICommand<FlatOutLogicContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;
        private long m_Index;

        public FlatOutLogicCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(FlatOutLogicContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            logic(commandContext.Markup);
        }

        /* Условия для пробития верхней границы флэта, 
            * при Разметке вверх - это отмена флэта и переход обратно во флэт
            * при разметке вниз - это переход в УЗ общий, либо возможен вариант в УЗ к ИПУЗУ(как частный случай)  
        */
        private bool breakUpperFlat(DirectionType markup, WaveMarkup lastWave, Flat flat)
        {
            var lastWaveEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, lastWave, SeqType.End));
            var flatMarkupStartPrice =
                m_QueryBuilder.For<double>().With(new FlatGetWaveMarkupPrice(m_Ptf, flat, SeqType.Start));
            var flatMarkupEndPrice =
                m_QueryBuilder.For<double>().With(new FlatGetWaveMarkupPrice(m_Ptf, flat, SeqType.End));

            return markup == DirectionType.Up
                ? lastWaveEndPrice > flatMarkupStartPrice
                : lastWaveEndPrice > flatMarkupEndPrice;
        }

        /* Условия для пробития верхней границы флэта, 
          * при Разметке вверх - это переход в УЗ общий, либо возможен вариант в УЗ к ИПУЗУ(как частный случай)  
          * при разметке вниз - это отмена флэта и переход обратно во флэт
         */
        private bool breakLowerFlat(DirectionType markup, WaveMarkup lastWave, Flat flat)
        {
            var lastWaveEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, lastWave, SeqType.End));
            var flatMarkupStartPrice =
                m_QueryBuilder.For<double>().With(new FlatGetWaveMarkupPrice(m_Ptf, flat, SeqType.Start));
            var flatMarkupEndPrice =
                m_QueryBuilder.For<double>().With(new FlatGetWaveMarkupPrice(m_Ptf, flat, SeqType.End));

            return markup == DirectionType.Up
                ? lastWaveEndPrice < flatMarkupEndPrice
                : lastWaveEndPrice < flatMarkupStartPrice;
        }

        /* Функция поиска лучшей конечной волны будущейволны Б
               * (это либо сама волна Б или составная часть, последняя часть волны Б)
               */
        private bool betterWavebEnd(DirectionType markup, WaveMarkup waveCandidate, WaveMarkup waveCur)
        {
            if (waveCur == null)
                return true;

            var waveCandidateEndPrice =
                m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waveCandidate, SeqType.End));
            var waveCurEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waveCur, SeqType.End));

            return markup == DirectionType.Up
                ? waveCandidateEndPrice > waveCurEndPrice
                : waveCandidateEndPrice < waveCurEndPrice;
        }

        // Функция действия при выхода флэта в УЗ
        private void ExitToUz(DirectionType markup, Flat flat)
        {
            // Последняя часть будущей волны Б(либо это и есть волна Б в простом случае)
            WaveMarkup lastPartBigWaveB = null;

            var flatMarkup = m_QueryBuilder.For<WaveMarkup>().With(new FlatGetWaveMarkup(m_Ptf, flat));

            foreach (var waveCanditate in markupsSearchRootRange(flatMarkup.Index + 1, markup))
            {
                if (betterWavebEnd(markup,waveCanditate, lastPartBigWaveB))
                {
                    lastPartBigWaveB = waveCanditate;
                }

            }

            var firstPartBigWaveB = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupGetNextWave(m_Ptf, flatMarkup));
            var firstPartBigC = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupGetNextWave(m_Ptf, lastPartBigWaveB));

            //Построение большой Б волны 
            IncreaseWaveLevel(markup, firstPartBigWaveB, lastPartBigWaveB);

            //Построение большой С волны
            IncreaseWaveLevel(markup, firstPartBigC, null);

            flat.IsCompleted = true;
            //LogWalgEvent.CatchFlatOutputUz(LogParams, markup);

            m_CommandBuilder.Execute(new CatchUzContext(m_Ptf, markup));

            //OnNgChaned(new NgChangedEventArgs { Direction = markup });
        }

        // Функция дествия при выходе флэта по тренда
        private void ExitToTrend(DirectionType markup, Flat flat)
        {
            var flatMarkup = m_QueryBuilder.For<WaveMarkup>().With(new FlatGetWaveMarkup(m_Ptf, flat));

            if (flat.FlatType != FlatType.AfterIpuz)
            {
                var nextWave = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupGetNextWave(m_Ptf, flatMarkup));

                IncreaseWaveLevel(markup, nextWave, null, PatternType.ComplexImpulse);
                flat.IsCompleted = true;
                //LogWalgEvent.CatchFlatOutputToTrend(LogParams, markup);
            }
            else
            {
                var nextWave = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupGetNextWave(m_Ptf, flatMarkup));

                IncreaseWaveLevel(markup, nextWave, null, PatternType.Vipuz);
                flat.IsCompleted = true;
                //LogWalgEvent.CatchFlatType2OutputToVipuz(LogParams, markup);
            }
            //OnNgChaned(new NgChangedEventArgs { Direction = markup });
        }

        private void logic(DirectionType markup)
        {
            var flat = m_Wtf.Flats.SingleOrDefault(p => p.TrendType == markup && !p.IsCompleted);
            if (flat == null)
                return;

            var lastWave = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, markup, OrderType.Last));
           
            switch (markup)
            {
                // Разметка вверх
                case DirectionType.Up:
                    // Пробитие верхней границы флэта
                    if (breakUpperFlat(markup, lastWave,flat))
                    {
                        ExitToTrend(markup,flat);
                    }
                    // Пробитие нижней границы флэта
                    else if (breakLowerFlat(markup, lastWave, flat))
                    {
                        ExitToUz(markup, flat);
                    }
                    break;

                case DirectionType.Down:
                    // Пробитие верхней границы флэта
                    if (breakUpperFlat(markup, lastWave, flat))
                    {
                        ExitToUz(markup, flat);
                    }
                    // Пробитие нижней границы флэта
                    else if (breakLowerFlat(markup, lastWave, flat))
                    {
                        ExitToTrend(markup,flat);
                    }
                    break;
            }
        }

        /// <summary>
        /// Увеличение уровня волны c указанной по конечную.
        /// </summary>
        /// <param name="plan">План разметки,где расматриваем</param>
        /// <param name="waveStart">Начальная волна,входящая в будущую большую волну</param>
        /// <param name="waveEnd">Кончная волна, входящая в будущую большую волну</param>
        /// <param name="pattern">Паттерн, которым помечаем новую большую волну</param>
        private void IncreaseWaveLevel(DirectionType plan,
            WaveMarkup waveStart, WaveMarkup waveEnd, PatternType pattern = PatternType.None)
        {
            // Вариант когда ищем волну Б после того как флэт закончился, а это волна представлена  ввиде одной волны
            if (waveEnd != null && waveStart.Id == waveEnd.Id)
            {
                return;
            }
            if (waveEnd == null)
            {
                // Вариант когда ищем волну С после того как флэт закончился, а это волна представлена ввиде одной волны
                var count = markupsSearchRootRangeCount(waveStart.Index, waveStart.TrendType);
                if (count <= 1)
                {
                    return;
                }
                // ------
            }

            var rootWave = new WaveMarkup
            {
                TrendType = waveStart.TrendType,
                WaveType = waveStart.WaveType,
                Pattern = pattern,
                Depth = waveStart.Depth,
                IsCompleted = false,
            };

            m_Wtf.WaveMarkups.Add(rootWave);


            var query = waveEnd == null
                ? markupsSearchRootRange(waveStart.Index, waveStart.TrendType)
                : markupsSearchRootRange(waveStart.Index, waveEnd.Index, waveStart.TrendType);

            foreach (var waveMarkup in query)
            {
                waveMarkup.ParentId = rootWave.Id;
                waveMarkup.Depth++;
            }

            rootWave.Index = m_QueryBuilder.For<int>()
                .With(new WaveEntityGetNextIndex(m_Ptf, WaveGroupType.WaveMarkup, plan));
            
        }

        private int markupsSearchRootRangeCount(int index, DirectionType direction)
        {
            return m_Wtf.WaveMarkups.Where(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null)
                .Count(p => p.Index >= index);
        }


        /// <summary>
        /// Список волн по поиску среди активных рутовых волн с указанного индекса и до конца
        /// </summary>
        private List<WaveMarkup> markupsSearchRootRange(int index, DirectionType direction)
        {
            return m_Wtf.WaveMarkups.Where(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null)
                .Where(p => p.Index >= index).OrderBy(p => p.Index).ToList();
        }

        /// <summary>
        /// Список волн по поиск среди активных рутовых волн с указанного индекса и до указанного индекса
        /// </summary>
        private List<WaveMarkup> markupsSearchRootRange(int startIndex, int endIndex, DirectionType direction)
        {
            return
                m_Wtf.WaveMarkups.Where(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null)
                    .Where(p => p.Index >= startIndex && p.Index <= endIndex)
                    .OrderBy(p => p.Index)
                    .ToList();
        }

       
    }

}
