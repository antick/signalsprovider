﻿using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg
{
    public class FlatLogicCommand : ICommand<FlatLogicContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;

        public FlatLogicCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(FlatLogicContext context)
        {
            var ptf = context.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));
            var eventType = context.EventType;
            var index = context.Index;

            // Создание флэта только через последний признак - сарка
            if (eventType == WaveEventType.NewSarBar || eventType == WaveEventType.AddMovement)
            {
                if (!m_Wtf.Flats.Any(p => p.TrendType == DirectionType.Up && !p.IsCompleted))
                {
                    m_CommandBuilder.Execute(new FlatInputLogicContext(ptf, eventType, index, DirectionType.Up));
                }
                if (!m_Wtf.Flats.Any(p => p.TrendType == DirectionType.Down && !p.IsCompleted))
                {
                    m_CommandBuilder.Execute(new FlatInputLogicContext(ptf, eventType, index, DirectionType.Down));
                }
            }

            // Выход из флэта
            if (eventType == WaveEventType.AddMovement || eventType == WaveEventType.UpdateMovement)
            {
                m_CommandBuilder.Execute(new FlatOutLogicContext(ptf, DirectionType.Up));
                m_CommandBuilder.Execute(new FlatOutLogicContext(ptf, DirectionType.Down));
            }
        }
    }
}
