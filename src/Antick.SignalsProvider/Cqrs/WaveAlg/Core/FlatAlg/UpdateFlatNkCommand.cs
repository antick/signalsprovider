﻿using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Components;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg
{
    public class UpdateFlatNkCommand : ICommand<UpdateFlatNkContext>
    {
        private readonly IQueryBuilder m_QueryBuilder;

        private WaveV1DataModel m_Wtf;

        public UpdateFlatNkCommand(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(UpdateFlatNkContext commandContext)
        {
            var ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            foreach (var flat in m_Wtf.Flats.Where(p => !p.IsCompleted))
            {
                var updated = updateNkOnPrev(flat);
                if (updated)
                {
                    //NgChanged.Raise(this, new NgChangedEventArgs { Direction = waveMarkup.TrendTypeId });
                }
            }

        }

        private bool updateNkOnPrev(Flat flat)
        {
            var barStart = getRate(flat.BarStart);
            var barEnd = getRate(flat.BarNKEnd);

            var koef = new LnKoef();
            koef.Calculate(barStart, barEnd, flat.TrendType);

            var prevBar = m_Wtf.Rates.Last();
            var y = koef.Y(prevBar.Index);

            if (hkCondition(flat.TrendType, prevBar.Index, y))
            {
                flat.BarNKEnd = prevBar.Index;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Условия обновления НК флэта
        /// </summary>
        /// <param name="direction">направление разметки, к которому относится флэт</param>
        /// <param name="index">индекс бара, который расматривает как кандидат на пробитие НК</param>
        /// <param name="yNk">значение текущего НК на этом баре</param>
        private bool hkCondition(DirectionType direction, long index, double yNk)
        {
            return direction == DirectionType.Up
                ? m_Wtf.Rates.First(p => p.Index == index).High > yNk
                : m_Wtf.Rates.First(p => p.Index == index).Low < yNk;
        }

        private Rate getRate(long index)
        {
            return m_Wtf.Rates.FirstOrDefault(p => p.Index == index);
        }
    }
}
