﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.FlatAlg
{
    public class FlatInputLogicCommand : ICommand<FlatInputLogicContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;
        private long m_EventIndex;
        private WaveEventType m_EventType;

        public FlatInputLogicCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(FlatInputLogicContext context)
        {
            m_Ptf = context.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));
            m_EventIndex = context.Index;
            m_EventType = context.EventType;

            alg(context.Markup);
        }

        private void alg(DirectionType plan)
        {
            var wave1 = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetFirstCorrection(m_Ptf, plan));

            //TODO: переписать на выборка всех следуюущий волн сразу
            var wave2 = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, wave1));
            var wave3 = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, wave2));
            var wave4 = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, wave3));
            var wave5 = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, wave4));

            // Как минимум должно быть 3 волны
            if (wave1 == null || wave2 == null || wave3 == null)
                return;

            if (m_EventType == WaveEventType.AddMovement)
            {
                /* Флэт после отмены ВИПУЗа */
                if (wave1.Pattern == PatternType.Uz && wave2.Pattern == PatternType.Ipuz && wave4 != null &&
                    wave4.Pattern == PatternType.Vipuz && wave5 != null)
                {
                    /* Пояснения для разметки ВВЕРХ:
                     * wave1 - УЗ, 
                     * wave2 - ИПУЗ,
                     * wave3 - откат,
                     * wave4 - ВИПУЗ, который не пробил хай последнего импульса,
                     * wave5 - волна вниз, cамо ее наличие говорит о том что флэт начался(модель ИПУЗ-ВИПУЗ не выполнена)
                     */

                    var wave4EndPrice = m_QueryBuilder.For<double>()
                        .With(new WaveMarkupGetPrice(m_Ptf, wave4, SeqType.End));
                    var wave1StartPrice = m_QueryBuilder.For<double>()
                        .With(new WaveMarkupGetPrice(m_Ptf, wave1, SeqType.End));
                    var wave5EndPrice = m_QueryBuilder.For<double>()
                        .With(new WaveMarkupGetPrice(m_Ptf, wave5, SeqType.End));
                    var wave1EndPrice = m_QueryBuilder.For<double>()
                        .With(new WaveMarkupGetPrice(m_Ptf, wave1, SeqType.End));

                   var  condition = plan == DirectionType.Up
                        ? wave4EndPrice < wave1StartPrice && wave5EndPrice > wave1EndPrice
                        : wave4EndPrice > wave1StartPrice && wave5EndPrice < wave1EndPrice;
                    if (condition)
                    {
                        CancelIpuz(wave2);
                        cancelVipuz(wave4);
                        initFlat(wave1, m_EventIndex, FlatType.AfterCanceledVipuz);
                        m_CommandBuilder.Execute(new SetPatternContext(m_Ptf, plan, PatternType.FlatAfterCanceledVipuz));
                    }
                }
            }

        }

        /// <summary>
        /// Анализ условий для входа во флэт, в указанной разметке
        /// </summary>
        /// <param name="args"></param>
        /// <param name="plan"></param>
        private void obsolete(DirectionType plan)
        {
            //    var wave1 = markupFirstCorrection(plan);
            //    var wave2 = m_QueryBuilder.For<WaveMarkup>()
            //            .With(new MarkupNextWaveCriterion(m_ProviderId, m_Timeframe, wave1));
            //    var wave3 = m_QueryBuilder.For<WaveMarkup>()
            //            .With(new MarkupNextWaveCriterion(m_ProviderId, m_Timeframe, wave2));
            //    var wave4 = m_QueryBuilder.For<WaveMarkup>()
            //            .With(new MarkupNextWaveCriterion(m_ProviderId, m_Timeframe, wave3));
            //    var wave5 = m_QueryBuilder.For<WaveMarkup>()
            //            .With(new MarkupNextWaveCriterion(m_ProviderId, m_Timeframe, wave4));

            //    // Как минимум должно быть 3 волны
            //    if (wave1 == null || wave2 == null || wave3 == null)
            //        return;

            //    /*Для флэтов 2ух типов нужна соотвестующая сарка. При плане вверх - сарка на Бай*/
            //    //var exceptedSar = plan.ToSar();

            //    var condition = false;

            //    var sarka = SarType.None; // бралась сарка по индексу бара, на котором было событие : Pool.Bars[args.Index].Sar();

            //    // Нужная сарка на этом текущем баре
            //    var sarkaCondition = false; // сломано sarka == exceptedSar;

            //    /* Простой флэт */
            //    // Волна 1 не должна быть УЗ, ибо тогда это либо паттерны УЗ-ИПУЗ-ВИПУЗ либо более сложный флэт
            //    if (wave1.PatternId != PatternTypeEnum.Uz && (sarkaCondition))
            //    {
            //        /*  Пояснение при разметке ВВЕРХ:
            //        *  wave1 - коррекционная волна, НЕ УЗ(потому что в этом случае за ним будет ИПУЗ),                        
            //        *  wave2 - волна вверх, не пробивает коррекционную волну(точка конца последнего импульса)
            //        *  wave3 - волна вниз, не пробивает точку старта волны вверх(если бы пробил - это уже был бы УЗ)  
            //        */
            //        condition = plan == TrendTypeEnum.Up
            //            ? (wave2.EndPrice(m_Wtf) < wave1.StartPrice(m_Wtf) && wave3.EndPrice(m_Wtf) > wave1.EndPrice(m_Wtf))
            //            : (wave2.EndPrice(m_Wtf) > wave1.StartPrice(m_Wtf) && wave3.EndPrice(m_Wtf) < wave1.EndPrice(m_Wtf));
            //        if (condition)
            //        {
            //            initFlat(wave1, m_EventIndex, FlatTypeEnum.Simple);
            //            m_CommandBuilder.Execute(new SetPatternContext(m_ProviderId, m_Timeframe, plan, PatternTypeEnum.FlatSimple));
            //            //LogWalgEvent.CatchPattern(LogParams, PatternTypeEnum.FlatSimple, plan);
            //        }
            //    }
            //    //-----------------

            //    /* Флэт к ИПУЗу */
            //    if (wave1.PatternId == PatternTypeEnum.Uz &&
            //        wave2.PatternId == PatternTypeEnum.Ipuz &&
            //        wave4 != null &&
            //        wave4.PatternId != PatternTypeEnum.Vipuz &&
            //        wave5 != null &&
            //        !condition &&
            //        (sarkaCondition))
            //    {
            //        /* Пояснения при Разметке ВВЕРХ:
            //            * wave1 - УЗ, 
            //            * wave2 - ИПУЗ,
            //            * wave3 - откат,
            //            * wave4 - волна вверх, не пробивает ИПУЗ
            //            * wave5 - волна вниз, не пробивает точку старта волны вверх(конеч волны wave3)*/

            //        condition = plan == TrendTypeEnum.Up
            //            ? (wave4.EndPrice(m_Wtf) < wave3.StartPrice(m_Wtf) && wave5.EndPrice(m_Wtf) > wave3.EndPrice(m_Wtf))
            //            : (wave4.EndPrice(m_Wtf) > wave3.StartPrice(m_Wtf) && wave5.EndPrice(m_Wtf) < wave3.EndPrice(m_Wtf));
            //        if (condition)
            //        {
            //            cancelVipuz(wave4);
            //            initFlat(wave3, m_EventIndex, FlatTypeEnum.AfterIpuz);
            //            m_CommandBuilder.Execute(new SetPatternContext(m_ProviderId, m_Timeframe, plan, PatternTypeEnum.FlatAfterIpuz));
            //            //LogWalgEvent.CatchPattern(LogParams, PatternTypeEnum.FlatAfterIpuz, plan);
            //        }
            //    }
            //    //------------------


            //    if (m_EventType == WaveEventType.AddMovement && !condition)
            //    {
            //        /* Флэт после отмены ВИПУЗа */
            //        if (wave1.PatternId == PatternTypeEnum.Uz && wave2.PatternId == PatternTypeEnum.Ipuz && wave4 != null &&
            //            wave4.PatternId == PatternTypeEnum.Vipuz && wave5 != null)
            //        {
            //            /* Пояснения для разметки ВВЕРХ:
            //             * wave1 - УЗ, 
            //             * wave2 - ИПУЗ,
            //             * wave3 - откат,
            //             * wave4 - ВИПУЗ, который не пробил хай последнего импульса,
            //             * wave5 - волна вниз, cамо ее наличие говорит о том что флэт начался(модель ИПУЗ-ВИПУЗ не выполнена)
            //             */

            //            condition = plan == TrendTypeEnum.Up
            //                ? (wave4.EndPrice(m_Wtf) < wave1.StartPrice(m_Wtf) && wave5.EndPrice(m_Wtf) > wave1.EndPrice(m_Wtf))
            //                : (wave4.EndPrice(m_Wtf) > wave1.StartPrice(m_Wtf) && wave5.EndPrice(m_Wtf) < wave1.EndPrice(m_Wtf));
            //            if (condition)
            //            {
            //                CancelIpuz(wave2);
            //                cancelVipuz(wave4);
            //                initFlat(wave1, m_EventIndex, FlatTypeEnum.AfterCanceledVipuz);
            //                m_CommandBuilder.Execute(new SetPatternContext(m_ProviderId, m_Timeframe, plan, PatternTypeEnum.FlatAfterCanceledVipuz));
            //                //LogWalgEvent.CatchPattern(LogParams, PatternTypeEnum.FlatAfterCanceledVipuz, plan);
            //            }
            //        }
            //    }

            //    //if (condition)
            //    //    OnNgChaned(new NgChangedEventArgs { Direction = plan });
         }

            private void cancelVipuz(WaveMarkup waveMarkup)
            {
                if (waveMarkup.Pattern == PatternType.Vipuz)
                {
                    var childrend = m_QueryBuilder.For<List<WaveMarkup>>()
                        .With(new WaveMarkupGetChildren(m_Ptf,waveMarkup));

                    if (childrend.Any())
                    {
                        var index = waveMarkup.Index;
                        foreach (var child in childrend)
                        {
                            child.ParentId = null;
                            child.Depth = waveMarkup.Depth;
                            child.Index = index++;
                        }
                        waveMarkup.Pattern = PatternType.None;
                        waveMarkup.IsCompleted = true;
                    }
                    else
                    {
                        waveMarkup.Pattern = PatternType.None;
                    }
                }
            }

        /// <summary>
        /// Отмена ИПУЗа
        /// </summary>
        /// <param name="waveMarkup"></param>
        private void CancelIpuz(WaveMarkup waveMarkup)
        {
            if (waveMarkup.Pattern == PatternType.Ipuz)
            {
                waveMarkup.Pattern = PatternType.None;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="waveMarkup">Коррекционная волна по которой строится флэт</param>
        /// <param name="barStartIndex"></param>
        /// <param name="flatType"></param>
        private void initFlat(WaveMarkup waveMarkup, long barStartIndex, FlatType flatType)
        {
            var firstMovement =
                m_QueryBuilder.For<Movement>().With(new WaveMarkupGetChildMovement(m_Ptf, waveMarkup, OrderType.First));

            var firstMovementStartBar =
                m_QueryBuilder.For<Rate>().With(new MovementGetRate(m_Ptf, firstMovement, OrderType.First));
            
            var flat = new Flat
            {
                BarStart = firstMovementStartBar.Index,
                BarInit = barStartIndex,
                BarNKEnd = barStartIndex,
                WaveMarkupId = waveMarkup.Id,
                TrendType = waveMarkup.TrendType,
                FlatType = flatType
            };
            m_Wtf.Flats.Add(flat);

            m_CommandBuilder.Execute(new UpdateFlatNkContext(m_Ptf));
        }
    }
}
