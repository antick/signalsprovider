﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core
{
    public class WaveAnalysisContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public WaveEventType EventType { get; set; }
        public long Index { get; set; }

        public WaveAnalysisContext(Ptf ptf, WaveEventType eventType, long index)
        {
            Ptf = ptf;
            EventType = eventType;
            Index = index;
        }
    }
}
