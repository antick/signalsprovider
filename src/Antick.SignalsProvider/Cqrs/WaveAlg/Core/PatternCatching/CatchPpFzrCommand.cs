﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching
{
    public class CatchPpFzrCommand : ICommand<CatchPpFzrContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;

        public CatchPpFzrCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchPpFzrContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            catchPpFzr(commandContext.Markup);
        }

        private void catchPpFzr(DirectionType direction)
        {
            if (m_Wtf.Flats.Any(p => p.TrendType == direction && !p.IsCompleted))
                return;

            var pattern = m_QueryBuilder.For<PatternType>()
                .With(new PatternCriterion(m_Ptf, direction));


            // Нельзя дважды применить паттерн
            if (pattern == PatternType.Ppfzr)
                return;

            var apply = false;

            var planWave = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, direction, OrderType.Last));

            if (planWave != null)
            {
                // ПО ТС : ФЗР это ОДНО движение против тренда, которое пробивает точку старта последнего импульса
                // ПП ФЗР - это простая волна, еще не помеченна как ПП ФЗР, пробивает точку старта посл. импульса и не пробивает
                // точку старта последнего импульса (актуально для разворота но не для ПП ФЗР)
                // Кроме того, это единственная на данный момент разметочная волна

                var isSimple = m_QueryBuilder.For<bool>()
                    .With(new WaveMarkupIsSimple(m_Ptf, planWave));

                if (isSimple && 
                    planWave.Pattern != PatternType.Ppfzr &&
                    breakLastImpulse(planWave) &&
                    !breakLastTrend(planWave) &&
                    countWavesRootActive(direction) == 1)
                {
                    planWave.Pattern = PatternType.Ppfzr;
                    apply = true;
                }
            }

            if (apply)
            {
                //LogWalgEvent.CatchPattern(LogParams, PatternTypeEnum.Ppfzr, direction);
                m_CommandBuilder.Execute(new SetPatternContext(m_Ptf, direction, PatternType.Ppfzr));
            }
        }

        /// <summary>
        /// Определяет пробила ли волна точку старта импульса
        /// </summary>
        private bool breakLastImpulse(WaveMarkup waveMarkup)
        {
            var lastImpulse = m_QueryBuilder.For<Wave>()
                .With(new WaveSearch(m_Ptf, waveMarkup.TrendType, OrderType.Last){WaveType = WaveType.Impulse});

            var waveMarkupEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waveMarkup, SeqType.End));

            var waveStartPrice = m_QueryBuilder.For<double>().With(new WaveGetPrice(m_Ptf, lastImpulse, SeqType.Start));

            if (waveMarkup.TrendType == DirectionType.Up)
            {
                // Планируемая волна пробивает точку старта последнего импульса
                return waveMarkupEndPrice < waveStartPrice;
            }
            else return waveMarkupEndPrice > waveStartPrice;
        }

        /// <summary>
        /// Пробила ли волна точку старта предыдущего тренда
        /// </summary>
        private bool breakLastTrend(WaveMarkup waveMarkup)
        {
            var trendDirection = m_QueryBuilder.For<DirectionType>()
                .With(new TrendDirectionCriterion(m_Ptf));

            var waveMarkupEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waveMarkup, SeqType.End));

            var lastTrend = m_Wtf.Trends.Last();

            var lastTrendStartPrice = m_QueryBuilder.For<double>().With(new TrendGetPrice(m_Ptf, lastTrend, SeqType.End));

            if (waveMarkup.TrendType == DirectionType.Up && trendDirection == DirectionType.Up)
            {
                return waveMarkupEndPrice < lastTrendStartPrice;
            }
            else if (waveMarkup.TrendType == DirectionType.Down && trendDirection == DirectionType.Down)
            {
                return waveMarkupEndPrice > lastTrendStartPrice;
            }
            return false;
        }

        /// <summary>
        /// Кол-во активных рутовых волн
        /// </summary>
        private int countWavesRootActive(DirectionType direction)
        {
            return m_Wtf.WaveMarkups.Count(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null);
        }

    }
}
