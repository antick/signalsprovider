﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching
{
    public class PatternCatchingContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public PatternCatchingContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
