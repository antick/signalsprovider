﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching
{
    public class CatchIpuzCommand : ICommand<CatchIpuzContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;


        public CatchIpuzCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchIpuzContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            catchIpuz(commandContext.Markup);
        }

        private void catchIpuz(DirectionType direction)
        {
            if (m_Wtf.Flats.Any(p => p.TrendType == direction && !p.IsCompleted))
                return;

            var apply = false;

            var waveUz = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, direction, OrderType.Last){Pattern = PatternType.Uz});

            if (waveUz != null)
            {
                var ipuz = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupSearch(m_Ptf, direction, OrderType.Last));

                var isSimple = m_QueryBuilder.For<bool>()
                    .With(new WaveMarkupIsSimple(m_Ptf, ipuz));

                // еще не помечена, простая, и следующая после УЗ
                if (ipuz.Pattern != PatternType.Ipuz && isSimple && waveUz.Index + 1 == ipuz.Index)
                {
                    ipuz.Pattern = PatternType.Ipuz;
                    apply = true;
                }
            }

            if (apply)
            {
                //LogWalgEvent.CatchPattern(LogParams, PatternTypeEnum.Ipuz, direction);
                m_CommandBuilder.Execute(new SetPatternContext(m_Ptf, direction, PatternType.Ipuz));
            }
        }
    }
}
