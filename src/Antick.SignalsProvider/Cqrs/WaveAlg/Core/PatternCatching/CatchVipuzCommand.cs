﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching
{
    public class CatchVipuzCommand : ICommand<CatchVipuzContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public CatchVipuzCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchVipuzContext commandContext)
        {
             m_Ptf = commandContext.Ptf;
             m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            catchVipuz(commandContext.Markup);
        }

        private void catchVipuz(DirectionType direction)
        {
            if (m_Wtf.Flats.Any(p => p.TrendType == direction && !p.IsCompleted))
                return;

            var apply = false;

            var waveUz = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, direction, OrderType.Last){Pattern = PatternType.Uz});

            var waveIpuz = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupGetNextWave(m_Ptf, waveUz));

            if (waveUz != null && waveUz.Pattern == PatternType.Uz && waveIpuz != null &&
                waveIpuz.Pattern == PatternType.Ipuz)
            {
                // Коррекционная  волна после ИПУЗа
                var wave3 = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, waveIpuz));

                var waveVipuz = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, wave3));


                /* После ИПУЗа откат не пробивает точку старта волны ИПУЗа => имеем значит тут Может быть ВИПУЗ */
                if (wave3 != null && waveVipuz != null && waveVipuz.Pattern != PatternType.Vipuz)
                {
                    var wave3EndPrice = m_QueryBuilder.For<double>()
                        .With(new WaveMarkupGetPrice(m_Ptf, wave3, SeqType.End));

                    var waveIpuzStartPrice = m_QueryBuilder.For<double>()
                        .With(new WaveMarkupGetPrice(m_Ptf, waveIpuz, SeqType.Start));

                    switch (direction)
                    {
                        case DirectionType.Up:
                            if (wave3EndPrice > waveIpuzStartPrice)
                            {
                                waveVipuz.Pattern = PatternType.Vipuz;
                                apply = true;
                            }
                            break;
                        case DirectionType.Down:
                            if (wave3EndPrice < waveIpuzStartPrice)
                            {
                                waveVipuz.Pattern = PatternType.Vipuz;
                                apply = true;
                            }
                            break;
                    }

                }
            }

            if (apply)
            {
                //LogWalgEvent.CatchPattern(LogParams, PatternTypeEnum.Vipuz, direction);
                m_CommandBuilder.Execute(new SetPatternContext(m_Ptf, direction, PatternType.Vipuz));
            }
        }
    }
}
