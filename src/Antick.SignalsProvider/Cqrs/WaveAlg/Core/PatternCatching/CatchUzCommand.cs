﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching
{
    public class CatchUzCommand : ICommand<CatchUzContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public CatchUzCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchUzContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            var direction = commandContext.Markup;

            if (m_Wtf.Flats.Any(p => p.TrendType == direction && !p.IsCompleted))
                return;

            var apply = false;

            var waveMarkups = getLastRootWaves(direction, 3);

            if (waveMarkups.Count == 3)
            {
                var wave1 = waveMarkups[2];
                var wave2 = waveMarkups[1];
                var wave3 = waveMarkups[0];

                var isFzr = m_QueryBuilder.For<bool>()
                    .With(new FzrCriterion(m_Ptf, direction.Reverse(), new List<WaveMarkup> {wave1, wave2, wave3}));
              
                // УЗ это фзр в обратную сторону
                if (isFzr)
                {
                    var root = increaseWaveLevelForUz(direction, waveMarkups);
                    apply = true;

                    // Обновление импульса - кандидата на волну А при развороте
                    //if (Pool.AlgParams.TrendReverseStatus.ImpulseId != Constants.Undefined &&
                    //    root.CrossMa208())
                    //{
                    //    var waveMarkup = Pool.WaveMarkups.FirstOrDefault(p => !p.IsCompleted &&
                    //                                              p.Parent == root &&
                    //                                              p.Index == 2);
                    //    if (waveMarkup != null && waveMarkup.AlternativeWave != null)
                    //    {
                    //        if (Pool.AlgParams.TrendReverseStatus.ImpulseId != waveMarkup.AlternativeWave.Id)
                    //        {
                    //            Pool.AlgParams.TrendReverseStatus.ImpulseId = waveMarkup.AlternativeWave.Id;
                    //            LogEvent.Patterns.UpdateReversalImpulse(_logParams, Bus.Direction);
                    //        }
                    //    }

                    //}

                }
            }

            if (apply)
            {
                m_CommandBuilder.Execute(new SetPatternContext(m_Ptf, direction,PatternType.Uz));
            }
        }

        /// <summary>
        /// Повышение ВУ волн, которые составляют Паттерн УЗ или УЗ к ИПУЗу
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="waveMarkups"></param>
        private WaveMarkup increaseWaveLevelForUz(DirectionType plan,
            List<WaveMarkup> waveMarkups)
        {
            var wave1 = waveMarkups[2];
            var wave2 = waveMarkups[1];
            var wave3 = waveMarkups[0];

            var depth = wave1.Depth;

            wave1.ParentId = null;
            wave1.Depth++;
            wave2.ParentId = null;
            wave2.Depth++;
            wave3.ParentId = null;
            wave3.Depth++;

            var nextIndex = m_QueryBuilder.For<int>()
                .With(new WaveEntityGetNextIndex(m_Ptf, WaveGroupType.WaveMarkup, plan));

            var root = new WaveMarkup
            {
                TrendType = wave1.TrendType,
                WaveType = WaveType.Correction,
                Pattern = PatternType.Uz,
                ParentId = null,
                Index = nextIndex,
                Depth = depth,
                IsCompleted = wave1.IsCompleted,
            };

            m_Wtf.WaveMarkups.Add(root);

            wave1.ParentId = root.Id;
            wave2.ParentId = root.Id;
            wave3.ParentId = root.Id;

            return root;
        }

        /// <summary>
        /// Список последний активных Рут волн, упорядоченных с КОНЦА!
        /// </summary>
        private List<WaveMarkup> getLastRootWaves(DirectionType direction, int countWaves)
        {
            return m_Wtf.WaveMarkups.Where(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null)
                .OrderByDescending(p => p.Index).Take(countWaves).ToList();
        }
    }
}
