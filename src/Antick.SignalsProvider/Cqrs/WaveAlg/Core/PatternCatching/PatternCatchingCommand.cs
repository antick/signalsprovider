﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.PatternCatching
{
    public class PatternCatchingCommand : ICommand<PatternCatchingContext>
    {
        private readonly ILogger m_Logger;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IQueryBuilder m_QueryBuilder;

        private Ptf m_Ptf;

        public PatternCatchingCommand(ILogger logger, ICommandBuilder commandBuilder, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_CommandBuilder = commandBuilder;
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(PatternCatchingContext commandContext)
        {
            m_Ptf = commandContext.Ptf;


            logic();
        }

        /// <summary>
        /// Поиск паттернов для обоих планов движения
        /// </summary>
        private void logic()
        {
            var trendDirection = m_QueryBuilder.For<DirectionType>()
                .With(new TrendDirectionCriterion(m_Ptf));

            // ПП ФЗР для текущего тренда
            m_CommandBuilder.Execute(new CatchPpFzrContext(m_Ptf, trendDirection));

            // Ищем паттерн УЗ для обоих вариантов движения
            m_CommandBuilder.Execute(new CatchUzContext(m_Ptf, DirectionType.Up));
            m_CommandBuilder.Execute(new CatchUzContext(m_Ptf, DirectionType.Down));

            // Поиск импуза для обоих вариантов движения
            m_CommandBuilder.Execute(new CatchIpuzContext(m_Ptf, DirectionType.Up));
            m_CommandBuilder.Execute(new CatchIpuzContext(m_Ptf, DirectionType.Down));
            
            // Поиск простых ВИПУЗов
            m_CommandBuilder.Execute(new CatchVipuzContext(m_Ptf, DirectionType.Up));
            m_CommandBuilder.Execute(new CatchVipuzContext(m_Ptf, DirectionType.Down));
        }
    }
}
