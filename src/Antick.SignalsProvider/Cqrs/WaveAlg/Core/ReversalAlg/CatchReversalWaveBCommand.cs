﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class CatchReversalWaveBCommand : ICommand<CatchReversalWaveBContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;

        public CatchReversalWaveBCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchReversalWaveBContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            signsType1Correction();
        }

        /// <summary>
        /// Поиск разворотной волны для типа 1
        /// </summary>
        private void signsType1Correction()
        {
            var reverseStatus = m_QueryBuilder.For<ReverseStatus>().With(new ReverseStatusCriterion(m_Ptf));

            if (reverseStatus != ReverseStatus.Stage1)
                return;

            var trendType = m_QueryBuilder.For<DirectionType>().With(new TrendDirectionCriterion(m_Ptf));
            var newDirection = trendType.Reverse();

            var waveA = m_QueryBuilder.For<WaveMarkup>().With(new ReverseWaveACriterion(m_Ptf));

            var correction = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, newDirection, OrderType.First)
                {
                    WaveIndex = waveA.Index,
                    WaveType = WaveType.Correction
                });

            if (correction != null)
            {
                m_CommandBuilder.Execute(new SetReverseWaveBContext(m_Ptf, correction.Id));
            }
        }
    }
}
