﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class CatchReversalFzrCommand : ICommand<CatchReversalFzrContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;

        public CatchReversalFzrCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchReversalFzrContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            logic();
        }

        private void logic()
        {
            var reverseStatus = m_QueryBuilder.For<ReverseStatus>().With(new ReverseStatusCriterion(m_Ptf));

            if (reverseStatus != ReverseStatus.Stage2)
                return;

            var trendType = m_QueryBuilder.For<DirectionType>().With(new TrendDirectionCriterion(m_Ptf));
            var newDirection = trendType.Reverse();

            if (hasFzr(newDirection))
            {
                var waveFzr = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetTrueImpulse(m_Ptf, newDirection));
                if (waveFzr != null)
                {
                    m_CommandBuilder.Execute(new SetReverseFzrWaveContext(m_Ptf, waveFzr.Id));
                }
                else
                {
                    throw new Exception("При развороте не найдена 3ая импульсная волна");
                }
            }
        }

        /// <summary>
        /// Проверка Фзр для типа по данным текущим данным
        /// </summary>
        /// <returns></returns>
        private bool hasFzr(DirectionType newDirection)
        {
            var firstImpulse = m_QueryBuilder.For<WaveMarkup>().With(new ReverseWaveACriterion(m_Ptf));
            var waveMarkup = m_QueryBuilder.For<WaveMarkup>().With(new ReverseWaveBCriterion(m_Ptf));
            var corection = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetRoot(m_Ptf, waveMarkup));
            var lastImpulse = markupTrueImpulse(newDirection);

            var waves = new List<WaveMarkup> { firstImpulse, corection, lastImpulse };

            var isFzr = m_QueryBuilder.For<bool>()
                .With(new FzrCriterion(m_Ptf, newDirection, waves, false));

            return isFzr;
        }

        private WaveMarkup markupTrueImpulse(DirectionType markup)
        {
            return m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetTrueImpulse(m_Ptf, markup));
        }
    }
}
