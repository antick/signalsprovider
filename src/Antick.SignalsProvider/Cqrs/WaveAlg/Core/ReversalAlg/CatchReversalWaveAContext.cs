﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    /// <summary>
    /// Поиск пробоя первой разворотной волны Медленной скользящей средней
    /// </summary>
    public class CatchReversalWaveAContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public CatchReversalWaveAContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
