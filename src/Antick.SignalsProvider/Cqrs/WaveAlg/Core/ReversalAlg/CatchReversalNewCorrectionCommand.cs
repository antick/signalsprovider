﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class CatchReversalNewCorrectionCommand : ICommand<CatchReversalNewCorrectionContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;


        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;

        public CatchReversalNewCorrectionCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchReversalNewCorrectionContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            signsType1NewCorrection();
        }

        private void signsType1NewCorrection()
        {
            // Когда по правилам разворота ФЗР у нас уже состоялся, но Разворотной Сар не было
            // Но, новая коррекция пробила точку

            var reverseStatus = m_QueryBuilder.For<ReverseStatus>().With(new ReverseStatusCriterion(m_Ptf));

            if (reverseStatus != ReverseStatus.Stage3B)
                return;
            

            var trendType = m_QueryBuilder.For<DirectionType>().With(new TrendDirectionCriterion(m_Ptf));
            var newDirection = trendType.Reverse();


            var lastWave = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, newDirection, OrderType.Last){WaveType = WaveType.Correction});

            if (lastWave != null)
            {
                var waveMarkup = m_QueryBuilder.For<WaveMarkup>().With(new ReverseWaveBCriterion(m_Ptf));
                var correction = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetRoot(m_Ptf, waveMarkup));

                var lastWaveEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, lastWave, SeqType.End));
                var correctionEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, correction, SeqType.End));

                var apply = false;
                if (newDirection == DirectionType.Up)
                {
                    apply = lastWaveEndPrice < correctionEndPrice;
                }
                else apply = lastWaveEndPrice > correctionEndPrice;


                if (apply)
                {
                    m_CommandBuilder.Execute(new SetReverseWaveBContext(m_Ptf, lastWave.Id));
                }
            }
        }
    }
}
