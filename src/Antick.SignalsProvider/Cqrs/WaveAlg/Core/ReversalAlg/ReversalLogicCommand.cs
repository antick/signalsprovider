﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class ReversalLogicCommand : ICommand<ReversalLogicContext>
    {
        private readonly ILogger m_Logger;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IQueryBuilder m_QueryBuilder;

        private WaveV1DataModel m_Wtf;
        private Providers.WaveProvider.Types.Bus m_Bus;
        private WaveEventType m_EventType;
        private long m_Index;
        private Ptf m_Ptf;


        public ReversalLogicCommand(ILogger logger, ICommandBuilder commandBuilder, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_CommandBuilder = commandBuilder;
            m_QueryBuilder = queryBuilder;
        }

        public void Execute(ReversalLogicContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));
            m_Bus = m_Wtf.Bus;
            m_EventType = commandContext.EventType;
            m_Index = commandContext.Index;
           

            logic();
        }

        private void logic()
        {
            // Поиск признаков тренда
            if (m_EventType == WaveEventType.AddMovement || m_EventType == WaveEventType.UpdateMovement)
            {
                m_CommandBuilder.Execute(new CatchReversalWaveAContext(m_Ptf));
            }

            if (m_EventType == WaveEventType.AddMovement)
            {
                m_CommandBuilder.Execute(new CatchReversalWaveBContext(m_Ptf));
            }
           

            if (m_EventType == WaveEventType.AddMovement || m_EventType == WaveEventType.UpdateMovement)
            {
                m_CommandBuilder.Execute(new CatchReversalFzrContext(m_Ptf));
                m_CommandBuilder.Execute(new CatchReversalNewCorrectionContext(m_Ptf));
            }

            // Попытка применить разворот
            applyTrendChange();
        }
        
        /// <summary>
        /// Проверяет на разворот тренда( через фзр или пробитие пред. тренда) и применяет разворот.
        /// </summary>
        private void applyTrendChange()
        {
            // Разворота тренда НЕ МОЖЕТ быть пока, мы находимся во флэте
            if (m_Wtf.Flats.Any(p => p.TrendType == m_Bus.Direction && !p.IsCompleted))
                return;

            var _newDirection = m_Bus.Direction.Reverse();

            bool wasChangedTrend = catchClassic();
            if (!wasChangedTrend)
                wasChangedTrend = CatchType2();

            if (wasChangedTrend)
            {
                applyMarkup(_newDirection);
                addTrend(_newDirection);
                //LogWalgEvent.AppliedReverseTrend(LogParams, Bus.Direction);
                clearReverseData();
                m_Bus.Direction = _newDirection;
            }
        }

        /// <summary>
        /// Проверка Фзр для типа по данным текущим данным
        /// </summary>
        /// <returns></returns>
        private bool hasFzr()
        {
            var _newDirection = m_Bus.Direction.Reverse();

            var firstImpulse = getMarkup(m_Bus.Reverse.ImpulseId);
            var waveMarkup = getMarkup(m_Bus.Reverse.CorrectionId);
            var corection = m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetRoot(m_Ptf, waveMarkup));
            var lastImpulse = markupTrueImpulse(_newDirection);

            var waves = new List<WaveMarkup> {firstImpulse, corection, lastImpulse};

            var isFzr = m_QueryBuilder.For<bool>()
                .With(new FzrCriterion(m_Ptf, _newDirection, waves, false));
            
            return isFzr;
        }

        /// <summary>
        /// Поиск первого типа разворота
        /// </summary>
        /// <returns>true если условия для разворота выполнены</returns>
        private bool catchClassic()
        {
            /*Вариант 1: Импульс + Коррекция + Разворотная САРка + ФЗР = Новый тренд */
            if (getReverseStatus() == ReverseStatus.Stage3A ||
                getReverseStatus() == ReverseStatus.Stage4)
            {
                if (m_Bus.Reverse.HasFzr || hasFzr())
                {
                    // Логировать только если уже не было залогировано
                    if (!m_Bus.Reverse.HasFzr)
                    {
                        //LogWalgEvent.CatchedReverseFzr(LogParams, Bus.Direction);
                    }
                    //LogWalgEvent.CatchedReverseFirst(LogParams, Bus.Direction);

                    return true;
                }
            }
            return false;
        }

        private bool CatchType2()
        {
            var _newDirection = m_Bus.Direction.Reverse();

            var lastTrend = m_Wtf.Trends.Last();

            /*Вариант 2 : Новый тренд начинается тогда, когда точка старта предыдущего была пробита */
            var lastImpulse = markupGetLastRoot(_newDirection, WaveType.Impulse);
            if (lastImpulse != null)
            {
                Func<bool> fBreakTrend = () =>
                {
                    var endPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, lastImpulse, SeqType.End));
                    var startPrice = m_QueryBuilder.For<double>().With(new TrendGetPrice(m_Ptf, lastTrend, SeqType.Start));

                    if (m_Bus.Direction == DirectionType.Up)
                        return endPrice < startPrice;
                    else return endPrice > startPrice;
                };

                if (fBreakTrend())
                {
                    //LogWalgEvent.CatchedReverseSecond(LogParams, bus.Direction);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Добавляет новый тренд, начальная волна - конец предыдущего тренда, конечная - последня волна(должна уже сущестовать!!!)
        /// </summary>
        private void addTrend(DirectionType directionType)
        {
            var lastTrend = m_Wtf.Trends.Last();
            var lastTrendEndWave = m_QueryBuilder.For<Wave>().With(new TrendGetWave(m_Ptf, lastTrend, SeqType.End));
            
            // Первая волна в новом тренде(импульсная и рутовая)
            var startWave = getFirstRootImpluse(lastTrendEndWave.Index, directionType);
            // Последняя рутовая импульсная волна по заданному направлениею тренда

            var endWave = m_QueryBuilder.For<Wave>()
                .With(new WaveSearch(m_Ptf,directionType, OrderType.Last){WaveType = WaveType.Impulse});

            var index = m_QueryBuilder.For<int>().With(new WaveEntityGetNextIndex(m_Ptf, WaveGroupType.Trend));

            var trend = new Trend
            {
                StartWaveId = startWave.Id,
                EndWaveId = endWave.Id,
                Type = directionType,
                Index = index,
                IsCompleted = false
            };
            m_CommandBuilder.Execute(new AddTrend(m_Ptf, trend));
        }

        private WaveMarkup getMarkup(long id)
        {
            return m_Wtf.WaveMarkups.FirstOrDefault(p => p.Id == id);
        }

        private Wave getFirstRootImpluse(int index, DirectionType direction)
        {
            return m_Wtf.Waves.Where(p => p.Index > index && p.WaveType == WaveType.Impulse &&
                                        p.TrendType == direction &&
                                        p.ParentId == null)
                .OrderBy(p => p.Index)
                .First();
        }
    

        private WaveMarkup markupGetLastRoot(DirectionType direction, WaveType waveType)
        {
            return m_Wtf.WaveMarkups.Where(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null)
                .Where(p => p.WaveType == waveType)
                .OrderByDescending(p => p.Index)
                .FirstOrDefault();
        }

        private ReverseStatus getReverseStatus()
        {
            return m_QueryBuilder.For<ReverseStatus>().With(new ReverseStatusCriterion(m_Ptf));
        }

        private void clearReverseData()
        {
            m_CommandBuilder.Execute(new ClearReverseDataContext(m_Ptf));
        }

        private void applyMarkup(DirectionType markup)
        {
            m_CommandBuilder.Execute(new ApplyMarkupContext(m_Ptf, markup));
        }

        private WaveMarkup markupTrueImpulse(DirectionType markup)
        {
            return m_QueryBuilder.For<WaveMarkup>().With(new WaveMarkupGetTrueImpulse(m_Ptf,markup));
        }

      
    }
}
