﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class CatchReversalWaveACommand : ICommand<CatchReversalWaveAContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;

        public CatchReversalWaveACommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CatchReversalWaveAContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            logic();
        }

        private void logic()
        {
            var reverseStatus = m_QueryBuilder.For<ReverseStatus>().With(new ReverseStatusCriterion(m_Ptf));

            // Поиск только когда пересечения еще не было с МА208
            if (reverseStatus != ReverseStatus.None)
                return;

            var trendType = m_QueryBuilder.For<DirectionType>().With(new TrendDirectionCriterion(m_Ptf));
            var newDirection = trendType.Reverse();

            WaveMarkup waveMarkup = m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupSearch(m_Ptf, newDirection, OrderType.Last){WaveType = WaveType.Impulse});
                
            if (waveMarkup != null && breakMa208(waveMarkup, newDirection))
            {
                m_CommandBuilder.Execute(new SetReverseWaveAContext(m_Ptf, waveMarkup.Id));
            }
        }

        // Волна пробивает МА208
        private bool breakMa208(WaveMarkup mWave, DirectionType newDirection)
        {
            // TODO: отрефакторить метод

            Movement firstMovement =
                m_QueryBuilder.For<Movement>().With(new WaveMarkupGetChildMovement(m_Ptf, mWave, OrderType.First));
            Movement lastMovement =
                m_QueryBuilder.For<Movement>().With(new WaveMarkupGetChildMovement(m_Ptf, mWave, OrderType.Last));

            var firstMovementStartMaSlow = m_QueryBuilder.For<double>()
                .With(new MovementGetMaSlow(m_Ptf, firstMovement, OrderType.First));

            var lastMovementEndMaSlow = m_QueryBuilder.For<double>()
                .With(new MovementGetMaSlow(m_Ptf, lastMovement, OrderType.Last));

            return newDirection == DirectionType.Down
                ? firstMovement.StartPrice > firstMovementStartMaSlow && lastMovement.EndPrice < lastMovementEndMaSlow
                : firstMovement.StartPrice < firstMovementStartMaSlow && lastMovement.EndPrice > lastMovementEndMaSlow;
        }
    }
}
