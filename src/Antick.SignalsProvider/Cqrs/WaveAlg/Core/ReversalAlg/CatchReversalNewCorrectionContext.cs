﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class CatchReversalNewCorrectionContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public CatchReversalNewCorrectionContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
