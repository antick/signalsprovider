﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.ReversalAlg
{
    public class ReversalLogicContext : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public WaveEventType EventType { get; set; }
        public long Index { get; set; }

        public ReversalLogicContext(Ptf ptf, WaveEventType eventType, long index)
        {
            Ptf = ptf;
            EventType = eventType;
            Index = index;
        }
    }
}
