﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg
{
    public class InitTrendCommand : ICommand<InitTrendContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public InitTrendCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(InitTrendContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            initTrend();
        }
        
        /// <summary>
        /// Создает самый первый тренд из движения
        /// </summary>
        private void initTrend()
        {
            // Добавляем первый тренд и первую фактическую волну, тренд = волна
            /* Алгоритм: Движение вверх над МА208 или изпод МА208 снизу вверх => Тренд вверх
             *           Движение вниз под МА208 или под МА208 сверху вниз => Тренд вниз
             *           Остальные движения игнорируются
             */

            var lastMovement = m_Wtf.Movements.Last();

            Func<DirectionType, Movement, bool> condition = (t, m) =>
            {
                var direction = m.Direction();
                var startMaSlow = m_QueryBuilder.For<double>().With(new MovementGetMaSlow(m_Ptf, m, OrderType.First));
                var endMaSlow = m_QueryBuilder.For<double>().With(new MovementGetMaSlow(m_Ptf, m, OrderType.Last));

                var movementWholeUpperMaSlow =
                    m_QueryBuilder.For<bool>().With(new MovementIsWholeMaSlow(m_Ptf, m, DirectionType.Up));

                var movementWholeLowerMaSlow =
                    m_QueryBuilder.For<bool>().With(new MovementIsWholeMaSlow(m_Ptf, m, DirectionType.Down));

                if (direction == t && t == DirectionType.Up)
                    return (m.StartPrice < startMaSlow && m.EndPrice > endMaSlow) || movementWholeUpperMaSlow;
                else if (direction == t && t == DirectionType.Down)
                    return (m.StartPrice >= startMaSlow && m.EndPrice < endMaSlow) || movementWholeLowerMaSlow;
                return false;
            };

            var trend = DirectionType.None;
            if (condition(DirectionType.Up, lastMovement))
                trend = DirectionType.Up;
            else if (condition(DirectionType.Down, lastMovement))
                trend = DirectionType.Down;

            if (trend != DirectionType.None)
            {
                var wave = addWave(lastMovement, trend, WaveType.Impulse);
                addTrend(wave);
                //LogWalgEvent.NewTrendStarted(LogParams, trend);
                m_CommandBuilder.Execute(new SetTrendDirectionContext(m_Ptf, trend));
            }
        }

        /// <summary>
        /// Создает новый тренд из одной волны
        /// </summary>
        /// <param name="wave"></param>
        private void addTrend(Wave wave)
        {
            var index = m_QueryBuilder.For<int>()
                .With(new WaveEntityGetNextIndex(m_Ptf, WaveGroupType.Trend));

            var trend = new Trend
            {
                StartWaveId = wave.Id,
                EndWaveId = wave.Id,
                Type = wave.TrendType,
                Index = index,
                IsCompleted = false,
            };
            m_CommandBuilder.Execute(new AddTrend(m_Ptf, trend));
        }

        /// <summary>
        /// Добавить движение как новую волну указав ее тип и тренд.
        /// </summary>
        private Wave addWave(Movement movement, DirectionType directionType, WaveType waveType)
        {
            var wave = new Wave
            {
                MovementId = movement.Id,
                TrendType = directionType,
                WaveType = waveType,
                Pattern = PatternType.None,
                Index = 0,
                Depth = 0,
                IsCompleted = false,
            };
            m_CommandBuilder.Execute(new AddWave(m_Ptf, wave));
            return wave;
        }

    }
}
