﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg
{
    public class MarkupLogicCommand : ICommand<MarkupLogicContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public MarkupLogicCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(MarkupLogicContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            updateWaveMarkup();
        }

        /// <summary>
        /// Добавляет новые волны в каждую разметку, при новом движении
        /// </summary>
        private void updateWaveMarkup()
        {
            var movementDirection = m_Wtf.Movements.Last().Direction();
            var lastMovement = m_Wtf.Movements.Last();
            WaveMarkup waveUp, waveDown;
            switch (movementDirection)
            {
                // Последнее движение идет вверх
                case DirectionType.Up:
                    // Вариант Вверх: Это движение - импульс вверх
                    waveUp = addWaveMarkup(lastMovement, DirectionType.Up, WaveType.Impulse);
                    // Вариант Вниз : Это движение - коррекция вверх
                    waveDown = addWaveMarkup(lastMovement, DirectionType.Down, WaveType.Correction);

                    waveUp.AlterativeWaveId = waveDown.Id;
                    waveDown.AlterativeWaveId = waveUp.Id;
                    break;
                // Последнее движение идет вниз
                case DirectionType.Down:
                    // Вариант Вверх : Это движение коррекция вниз
                    waveUp = addWaveMarkup(lastMovement, DirectionType.Up, WaveType.Correction);
                    // Вариант Вниз : это движение импульс вниз
                    waveDown = addWaveMarkup(lastMovement, DirectionType.Down, WaveType.Impulse);

                    waveUp.AlterativeWaveId = waveDown.Id;
                    waveDown.AlterativeWaveId = waveUp.Id;
                    break;
            }
        }

       
        /// <summary>
        /// Создает разметочную волну из указанного движения с указанными параметрами
        /// </summary>
        /// <param name="movement">Движение, которое является основой разметочной волны</param>
        /// <param name="markupDirection">В какую разметку добавляем</param>
        /// <param name="waveType">Тип волны(импульсная или коррекционная)</param>
        private WaveMarkup addWaveMarkup(Movement movement, DirectionType markupDirection,
            WaveType waveType)
        {
            var nextIndex = m_QueryBuilder.For<int>()
                .With(new WaveEntityGetNextIndex(m_Ptf, WaveGroupType.WaveMarkup, markupDirection ));

            var wave = new WaveMarkup
            {
                MovementId = movement.Id,
                TrendType = markupDirection,
                WaveType = waveType,
                Pattern = PatternType.None,
                Index = nextIndex,
                Depth = 0,
                IsCompleted = false,
            };
            m_CommandBuilder.Execute(new AddWaveMarkup(m_Ptf, wave));

            return wave;
        }
    }
}
