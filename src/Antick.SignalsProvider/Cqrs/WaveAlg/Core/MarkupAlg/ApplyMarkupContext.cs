﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg
{
    public class ApplyMarkupContext : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public DirectionType Markup { get; set; }

        public ApplyMarkupContext(Ptf ptf, DirectionType markup)
        {
            Ptf = ptf;
            Markup = markup;
        }
    }
}
