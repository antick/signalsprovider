﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg
{
    public class MarkupLogicContext: ICommandContext
    {
        public Ptf Ptf { get; set; }

        public MarkupLogicContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
