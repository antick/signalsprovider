﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.Bus.Commands;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainCommands.Context;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg
{
    public class ApplyMarkupCommand : ICommand<ApplyMarkupContext>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public ApplyMarkupCommand(ILogger logger, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(ApplyMarkupContext commandContext)
        {
            m_Ptf = commandContext.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            ApplyPlan(commandContext.Markup);
        }

        /// <summary>
        /// Применяет указанный план разметки, отменяет другой план
        /// </summary>
        /// <param name="plan"></param>
        private void ApplyPlan(DirectionType plan)
        {
            // список активных рут волн
            var rootList =  m_Wtf.WaveMarkups.Where(p => p.TrendType == plan && !p.IsCompleted && p.ParentId == null)
                .OrderBy(p => p.Index).ToList();

            // Примение плана - обновление текущего тренда
            foreach (var planWave in rootList)
            {
                var waveId = moveMarkupToActive(planWave);
                copyChildrenWaves(planWave.Id, waveId);
            }

            cancelPlans();

            //Здесь писать о том что разметка применена, альтернативная отменена
            //LogWalgEvent.СanceledMarkup(LogParams, plan);
        }

        private void cancelPlans()
        {
            foreach (var waveMarkup in m_Wtf.WaveMarkups.Where(p => !p.IsCompleted))
            {
                waveMarkup.IsCompleted = true;
            }

            foreach (var flat in m_Wtf.Flats.Where(p => !p.IsCompleted))
            {
                flat.IsCompleted = true;
            }

            m_CommandBuilder.Execute(new ClearPatternDataContext(m_Ptf));
        }

        /// <summary>
        /// Копирует дочерние планируемые волны в фактические
        /// </summary>
        /// <param name="planWaveId">Айди родительской волны в списке планируюемых</param>
        /// <param name="waveId">Айди родительской волны в фактических</param>
        private void copyChildrenWaves(long planWaveId, long waveId)
        {
            foreach (var planWave in m_Wtf.WaveMarkups.Where(p => p.ParentId == planWaveId).OrderBy(p => p.Index).ToList())
            {
                var id = moveMarkupToActive(planWave, waveId);
                copyChildrenWaves(planWave.Id, id);
            }
        }

        /// <summary>
        /// Переносит указанную волну из планируюмых в фактическую(ParentId не копируется)
        /// </summary>
        /// <param name="waveMarkup"></param>
        /// <param name="parentWaveId">Айди родительской волны в списке фактических</param>
        /// <returns>Возвращяет айди доступа до новой волны</returns>
        private long moveMarkupToActive(WaveMarkup waveMarkup, long? parentWaveId = null)
        {
            Wave parentWave = null;

            var index = waveMarkup.Index;

            if (parentWaveId == null)
            {
                index = m_QueryBuilder.For<int>()
                    .With(new WaveEntityGetNextIndex(m_Ptf, WaveGroupType.Wave));
            }

            var movement = m_QueryBuilder.For<Movement>().With(new WaveMarkupGetMovement(m_Ptf, waveMarkup));

            var wave = new Wave
            {
                MovementId = movement.Id,
                TrendType = waveMarkup.TrendType,
                WaveType = waveMarkup.WaveType,
                Pattern = waveMarkup.Pattern,
                ParentId = parentWaveId.Value,
                Index = index,
                Depth = waveMarkup.Depth,
                IsCompleted = waveMarkup.IsCompleted,
            };
            m_CommandBuilder.Execute(new AddWave(m_Ptf, wave));

            return wave.Id;
        }

      
    }
}
