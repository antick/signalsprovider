﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.Core.MarkupAlg
{
    public class InitTrendContext : ICommandContext
    {
        public Ptf Ptf { get; set; }

        public InitTrendContext(Ptf ptf)
        {
            Ptf = ptf;
        }
    }
}
