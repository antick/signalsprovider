﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Providers;
using Antick.SignalsProvider.Providers.WaveProvider;

namespace Antick.SignalsProvider.Cqrs.WaveAlg
{
    public  class GetWaveParametersV1Query : IQuery<GetWaveParametersV1, WaveV1Parameters>
    {
        private readonly ISignalProviderCache m_Cache;

        public GetWaveParametersV1Query(ISignalProviderCache cache)
        {
            m_Cache = cache;
        }

        public WaveV1Parameters Ask(GetWaveParametersV1 criterion)
        {
            var provider = m_Cache.Get(criterion.Ptf.ProviderId);

            if (provider.Type == "WaveV1")
            {
                return (WaveV1Parameters)provider.Parameters;
            }
            else throw new NotSupportedException($"Provider is not {provider.Type} type");
        }
    }
}
