﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg
{
    public class ProccessingWaveV1Context : ICommandContext
    {
        public Ptf Ptf { get; set; }
        public List<Rate> Rates { get; set; }
        
        public bool UseWaveAlg = true;

        public ProccessingWaveV1Context(Ptf ptf, List<Rate> rates)
        {
            Ptf = ptf;
            Rates = rates;
        }
    }
}
