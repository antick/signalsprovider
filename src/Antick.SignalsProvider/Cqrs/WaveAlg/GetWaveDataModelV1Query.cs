﻿using System;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Providers;
using Antick.SignalsProvider.Providers.WaveProvider;

namespace Antick.SignalsProvider.Cqrs.WaveAlg
{
    /// <summary>
    /// Достать конкретную по TimeFrame модель волновых данных
    /// </summary>
    public class GetWaveDataModelV1Query : IQuery<GetWaveDataModelV1,WaveV1DataModel>
    {
        private readonly ISignalProviderCache m_Cache;

        public GetWaveDataModelV1Query(ISignalProviderCache cache)
        {
            m_Cache = cache;
        }

        public WaveV1DataModel Ask(GetWaveDataModelV1 criterion)
        {
            var provider = m_Cache.Get(criterion.Ptf.ProviderId);

            if (provider.Type == "WaveV1")
            {
                return (WaveV1DataModel)provider.DataModel;
            }
            else throw  new NotSupportedException($"Provider is not {provider.Type} type");
        }
    }
}
