﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveMarkupSearchQuery : IQuery<WaveMarkupSearch, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;
        
        public WaveMarkupSearchQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(WaveMarkupSearch criterion)
        {
            var ptf = criterion.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var enumerable = wtf.WaveMarkups.Where(p =>
                p.TrendType == criterion.Markup && !p.IsCompleted && p.ParentId == criterion.ParentId);

            if (criterion.WaveType != null)
                enumerable = enumerable.Where(p => p.WaveType == criterion.WaveType.Value);

            if (criterion.Pattern != null)
                enumerable = enumerable.Where(p => p.Pattern == criterion.Pattern.Value);

            if (criterion.WaveIndex != null)
                enumerable = enumerable.Where(p => p.Index > criterion.WaveIndex.Value);

            if (criterion.OrderType == OrderType.First)
            {
                return enumerable
                    .OrderBy(p => p.Index).FirstOrDefault();
            }
            else
            {
                return enumerable
                    .OrderByDescending(p => p.Index).FirstOrDefault();
            }
        }

         
    }
}
