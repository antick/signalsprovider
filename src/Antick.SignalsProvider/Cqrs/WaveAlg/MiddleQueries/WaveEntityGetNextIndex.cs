﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveEntityGetNextIndex : ICriterion
    {
        public Ptf Ptf { get; set; }
        public WaveGroupType WaveGroupType { get; set; }
        public DirectionType? Markup { get; set; }

        public WaveEntityGetNextIndex(Ptf ptf, WaveGroupType groupType, DirectionType? markup = null)
        {
            Ptf = ptf;
            WaveGroupType = groupType;
            Markup = markup;
        }
    }
}
