﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveSearchQuery : IQuery<WaveSearch, Wave>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveSearchQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public Wave Ask(WaveSearch criterion)
        {
            var ptf = criterion.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            var enumerable =  
                wtf.Waves.Where(p => p.ParentId == criterion.ParentId && p.TrendType == criterion.Direction);

            if (criterion.WaveType != null)
                enumerable = enumerable.Where(p => p.WaveType == criterion.WaveType.Value);
            
            if (criterion.OrderType == OrderType.First)
            {
                return enumerable.OrderBy(p => p.Index).FirstOrDefault();
            }
            else
            {
                return enumerable.OrderByDescending(p => p.Index).FirstOrDefault();
            }
                
        }
    }
}
