﻿using System.Collections.Generic;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class FzrCriterion : ICriterion
    {
        public Ptf Ptf { get; set; }

        public List<WaveMarkup> Waves { get; set; }
        public DirectionType Markup { get; set; }
        public bool Strong = true;

        public FzrCriterion(Ptf ptf, DirectionType markup, List<WaveMarkup> waves,
            bool strong = true)
        {
            Ptf = ptf;
            Waves = waves;
            Strong = strong;
        }
    }
}
