﻿using System;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveEntityGetNextIndexQuery : IQuery<WaveEntityGetNextIndex, int>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        public WaveEntityGetNextIndexQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public int Ask(WaveEntityGetNextIndex criterion)
        {
            var ptf = criterion.Ptf;
            var wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(ptf));

            if (criterion.WaveGroupType == WaveGroupType.WaveMarkup)
            {
                if (!criterion.Markup.HasValue)
                    throw new ArgumentNullException();

                var waveMarkup = wtf.WaveMarkups
                    .Where(p => p.TrendType == criterion.Markup.Value && !p.IsCompleted && p.ParentId == null)
                    .OrderByDescending(p => p.Index).FirstOrDefault();

                if (waveMarkup != null)
                {
                    return waveMarkup.Index + 1;
                }
                return 0;
            }

            if (criterion.WaveGroupType == WaveGroupType.Wave)
            {
                var wave = wtf.Waves.Where(p => p.ParentId == null).OrderByDescending(p => p.Index).FirstOrDefault();

                if (wave != null)
                {
                    return wave.Index + 1;
                }
                return 0;
            }

            if (criterion.WaveGroupType == WaveGroupType.Trend)
            {
                if (wtf.Trends.Any())
                {
                    return wtf.Trends.OrderByDescending(p => p.Index).First().Index + 1;
                }
                return 0;
            }

            throw  new ArgumentException("waveType");
        }
    }
}
