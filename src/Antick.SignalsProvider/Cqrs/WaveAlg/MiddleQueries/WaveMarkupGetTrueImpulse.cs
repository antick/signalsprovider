﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveMarkupGetTrueImpulse : ICriterion
    {
        public Ptf Ptf { get; set; }
        public DirectionType Markup { get; set; }

        public WaveMarkupGetTrueImpulse(Ptf ptf, DirectionType markup)
        {
            Ptf = ptf;
            Markup = markup;
        }
    }
}
