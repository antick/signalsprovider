﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveMarkupGetTrueImpulseQuery : IQuery<WaveMarkupGetTrueImpulse, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        private Ptf m_Ptf;
        private WaveV1DataModel m_Wtf;
        
        
        public WaveMarkupGetTrueImpulseQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(WaveMarkupGetTrueImpulse criterion)
        {
            m_Ptf = criterion.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            return MarkupTrueImpulse(criterion.Markup);
        }

        /// <summary>
        /// Достает истинный импульс в рамках разметки.
        /// ( Истинный импульс - волна, не просто помеченная как импульсная, но и пробивающая пик 1ой волны.
        /// Это либо 3ая волна, либо конец-подволна 3ей волны )
        /// </summary>
        public WaveMarkup MarkupTrueImpulse(DirectionType direction)
        {
            // Список последний активных рут волн типа импульса
            var impulseList = m_Wtf.WaveMarkups
                .Where(p => p.TrendType == direction && !p.IsCompleted && p.ParentId == null &&
                            p.WaveType == WaveType.Impulse).OrderBy(p => p.Index).ToList();

            WaveMarkup impluse = null;

            foreach (var impluseCandidate in impulseList)
            {
                if (betterImpulse(impluse, impluseCandidate, direction))
                    impluse = impluseCandidate;
            }
            return impluse;
        }

        private bool betterImpulse(WaveMarkup im, WaveMarkup imCandidate, DirectionType d)
        {
            var imCandidateEndPrice =
                m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, imCandidate, SeqType.End));
            var imEndPrice = m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, im, SeqType.End));


            return im == null || (d == DirectionType.Up
                       ? imCandidateEndPrice > imEndPrice
                       : imCandidateEndPrice < imEndPrice);
        }
    }
}
