﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveMarkupGetFirstCorrectionQuery : IQuery<WaveMarkupGetFirstCorrection, WaveMarkup>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public WaveMarkupGetFirstCorrectionQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public WaveMarkup Ask(WaveMarkupGetFirstCorrection criterion)
        {
            m_Ptf = criterion.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            return markupFirstCorrectionAfterTrend(criterion.Markup);
        }

        /// <summary>
        /// Первая активная рутовая разметочная волна типа коррекции
        /// </summary>
        private WaveMarkup markupGetFirstRootCorrection(DirectionType direction)
        {
            return m_Wtf.WaveMarkups.Where(p =>
                    p.TrendType == direction && !p.IsCompleted && p.ParentId == null &&
                    p.WaveType == WaveType.Correction)
                .OrderBy(p => p.Index)
                .FirstOrDefault();
        }

        /// <summary>
        /// Для активного плана возвращает первую коррекцию, для альтернативной разметки - сначала пытается из разметочных волн достать коррекцию после последнего разметочного импульса, если не получается достает как обычно.
        /// </summary>
        private WaveMarkup markupFirstCorrectionAfterTrend(DirectionType direction)
        {
            var firstMarkupCorrection = markupGetFirstRootCorrection(direction);

            var activeTrendDirection = m_Wtf.Trends.Last().Type;

            if (activeTrendDirection == direction)
                return firstMarkupCorrection;
            else
            {
                var markupImpulse = markupTrueImpulse(direction); // Истинный импульс что создал ФЗР

                var markupCorrection = m_QueryBuilder.For<WaveMarkup>()
                    .With(new WaveMarkupGetNextWave(m_Ptf, markupImpulse));

                return markupCorrection != null && activeTrendDirection != direction
                    ? markupCorrection
                    : firstMarkupCorrection;
            }
        }

        private WaveMarkup markupTrueImpulse(DirectionType markup)
        {
            return m_QueryBuilder.For<WaveMarkup>()
                .With(new WaveMarkupGetTrueImpulse(m_Ptf, markup)); 
        }
    }
}
