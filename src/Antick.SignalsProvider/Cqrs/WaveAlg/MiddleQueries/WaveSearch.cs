﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveSearch : ICriterion
    {
        public Ptf Ptf { get; set; }

        public DirectionType Direction { get; set; }

        public OrderType OrderType { get; set; }

        public WaveType? WaveType { get; set; }
        public int? ParentId { get; set; }


        public WaveSearch(Ptf ptf,DirectionType direction, OrderType orderType)
        {
            Ptf = ptf;
            Direction = direction;
            OrderType = orderType;
        }
     
    }
}
