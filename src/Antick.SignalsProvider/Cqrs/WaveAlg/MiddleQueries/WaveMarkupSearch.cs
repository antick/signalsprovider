﻿using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider.Types;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class WaveMarkupSearch : ICriterion
    {
        public Ptf Ptf { get; set; }

        public DirectionType Markup { get; set; }

        public OrderType OrderType { get; set; }

        public int? ParentId { get; set; }
        public PatternType? Pattern { get; set; }
        public WaveType? WaveType { get; set; }

        /// <summary>
        /// Индекс искомой волны должен быть строго больше этого айди
        /// </summary>
        public long? WaveIndex { get; set; }

        public WaveMarkupSearch(Ptf ptf, DirectionType markup, OrderType orderType)
        {
            Ptf = ptf;
            Markup = markup;
            OrderType = orderType;
        }
    }
}
