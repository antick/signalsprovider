﻿using System.Collections.Generic;
using Antick.Cqrs.Queries;
using Antick.SignalsProvider.Cqrs.WaveAlg.DomainQueries.Criterias;
using Antick.SignalsProvider.Indicators.Types;
using Antick.SignalsProvider.Providers.WaveProvider;
using Antick.SignalsProvider.Providers.WaveProvider.Types;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.WaveAlg.MiddleQueries
{
    public class FzrQuery : IQuery<FzrCriterion, bool>
    {
        private readonly ILogger m_Logger;
        private readonly IQueryBuilder m_QueryBuilder;

        private WaveV1DataModel m_Wtf;
        private Ptf m_Ptf;

        public FzrQuery(ILogger logger, IQueryBuilder queryBuilder)
        {
            m_Logger = logger;
            m_QueryBuilder = queryBuilder;
        }

        public bool Ask(FzrCriterion criterion)
        {
            m_Ptf = criterion.Ptf;
            m_Wtf = m_QueryBuilder.For<WaveV1DataModel>().With(new GetWaveDataModelV1(m_Ptf));

            return isFzr(criterion.Waves, criterion.Markup, criterion.Strong);
        }

        /// <summary>
        /// Определяет есть ли ФЗР в указанной трехволновке по указанному направлению
        /// </summary>
        private bool isFzr(List<WaveMarkup> waves, DirectionType direction, bool strong)
        {
            // Строгий ФЗР - волна 2 не может пробивать точку старта волны 1
            // Не строгий - это правило опущено


            var wave0Direction =
                m_QueryBuilder.For<DirectionType>().With(new WaveMarkupGetDirection(m_Ptf, waves[0]));
            var wave1Direction =
                m_QueryBuilder.For<DirectionType>().With(new WaveMarkupGetDirection(m_Ptf, waves[1]));
            var wave2Direction =
                m_QueryBuilder.For<DirectionType>().With(new WaveMarkupGetDirection(m_Ptf, waves[2]));

            var wave0StartPrice =
                m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waves[0], SeqType.Start));
            var wave0EndPrice =
                m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waves[0], SeqType.End));

            var wave1EndPrice =
                m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waves[1], SeqType.End));
            var wave2EndPrice =
                m_QueryBuilder.For<double>().With(new WaveMarkupGetPrice(m_Ptf, waves[2], SeqType.End));

            if (direction == DirectionType.Up)
            {
                return wave0Direction == DirectionType.Up &&
                       wave1Direction == DirectionType.Down &&
                       wave2Direction == DirectionType.Up &&
                       (wave1EndPrice >= wave0StartPrice || !strong) &&
                       wave2EndPrice > wave0EndPrice;

            }
            if (direction == DirectionType.Down)
            {
                return wave0Direction == DirectionType.Down &&
                       wave1Direction == DirectionType.Up &&
                       wave2Direction == DirectionType.Down &&
                       (wave1EndPrice <= wave0StartPrice || !strong) &&
                       wave2EndPrice < wave0EndPrice;
            }
            return false;
        }
    }
}
