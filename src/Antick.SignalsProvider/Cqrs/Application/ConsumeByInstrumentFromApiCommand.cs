﻿using System;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Components.Misc;
using Antick.Infractructure.Extensions;
using Antick.RateManager.Client;
using Antick.SignalsProvider.Components.Blocks;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Cqrs.Application.Context;
using Antick.SignalsProvider.Providers;
using Castle.Core.Internal;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.Application
{
    public class ConsumeByInstrumentFromApiCommand : ICommand<ConsumeByInstrumentFromApiContex>
    {
        private readonly ILogger m_Logger;
        private readonly IBlocker m_Blocker;
        private readonly ISignalProviderCache m_SignalProviderCache;
        private readonly IRateManagerClient m_RateManagerClient;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IClock m_Clock;

        public ConsumeByInstrumentFromApiCommand(ILogger logger, IBlocker blocker, ISignalProviderCache signalProviderCache, IRateManagerClient rateManagerClient, ICommandBuilder commandBuilder, IClock clock)
        {
            m_Logger = logger;
            m_Blocker = blocker;
            m_SignalProviderCache = signalProviderCache;
            m_RateManagerClient = rateManagerClient;
            m_CommandBuilder = commandBuilder;
            m_Clock = clock;
        }

        public void Execute(ConsumeByInstrumentFromApiContex commandContext)
        {
            lock (m_Blocker.Get(commandContext.SignalProviderId))
            {
                var signalProvider = m_SignalProviderCache.Get(commandContext.SignalProviderId);

                logic(signalProvider);

                if (commandContext.IsStarting)
                {
                    signalProvider.SetReady();
                    m_Logger.DebugFormat("SignalProvider {0} is Ready", signalProvider.ToString());
                }
            }
        }

        private void logic(ISignalProvider signalProvider)
        {
            var timeFramesStates = signalProvider.GetTimeframeState();

            var tfState = timeFramesStates;
            {
                var lastRateData = tfState.LastRateDate;

                var day = tfState.LastRateDate;

                // Если не было данных о последней Quote, то берем из ее первой доступной Quote 
                if (day == null)
                {
                    var firstQuote = m_RateManagerClient.GetFirstRate(signalProvider.Instrument.Name,tfState.TimeFrame.Type, tfState.TimeFrame.Value).GetSynchronousResult();
                    if (firstQuote == null)
                    {
                        m_Logger.DebugFormat("Provider [{0}] no history Quotes", signalProvider.ToString());
                        return;
                    }
                    day = firstQuote.Time;
                }

                while (true)
                {
                    try
                    {
                        var d = day.Value;
                        var startDate = new DateTime(d.Year,d.Month, d.Day,0,0,0, DateTimeKind.Utc);
                        if (lastRateData != null && lastRateData > startDate)
                            startDate = lastRateData.Value;

                        var endDate = new DateTime(d.Year,d.Month,d.Day,23,59,59, DateTimeKind.Utc);

                        var rates = m_RateManagerClient.GetRates(signalProvider.Instrument.Name, tfState.TimeFrame.Type,
                            tfState.TimeFrame.Value, startDate, endDate, true).GetSynchronousResult();
                        
                        // Обычный день, в которорм есть quotes
                        if (!rates.IsNullOrEmpty())
                        {
                            m_Logger.DebugFormat("Provider [{0}] reading day {1}", signalProvider,
                                day.Value.ToString("yyyy/MM/dd"));

                            // Кидаем в алгоритм, фильруем, если данные уже были
                            m_CommandBuilder.Execute(new ConsumeContext
                            {
                                SignalProviderId = signalProvider.Id,
                                Rates = rates,
                                TimeFrameType = tfState.TimeFrame.Type,
                                TimeFrameValue = tfState.TimeFrame.Value
                            });
                        }
                        // в этом дне нет записей
                        else
                        {
                            m_Logger.DebugFormat("SignalProvider [{0}] no data in day {1}. Break", signalProvider,
                                day.Value.ToString("yyyy/MM/dd"));

                            break;
                        }

                        day = day.Value.AddDays(1);
                    }
                    catch (Exception ex)
                    {
                        m_Logger.Error(string.Format("Error on consuming alg {0}. Continue...", signalProvider.ToString()), ex);
                    }
                }
            }
        }
    }
}
