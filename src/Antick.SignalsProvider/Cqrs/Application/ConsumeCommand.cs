﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Cqrs.Application.Context;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.Application
{
    public class ConsumeCommand : ICommand<ConsumeContext>
    {
        private readonly ILogger m_Logger;
        private readonly ISignalProviderCache m_SignalProviderCache;

        public ConsumeCommand(ILogger logger, ISignalProviderCache signalProviderCache)
        {
            m_Logger = logger;
            m_SignalProviderCache = signalProviderCache;
        }

        public void Execute(ConsumeContext commandContext)
        {
            var provider = m_SignalProviderCache.Get(commandContext.SignalProviderId);
            provider.Consume(commandContext.Rates);
        }
    }
}
