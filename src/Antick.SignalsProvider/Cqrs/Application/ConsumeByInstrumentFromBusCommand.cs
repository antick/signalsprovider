﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Components.Blocks;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Cqrs.Application.Context;
using Antick.SignalsProvider.Providers;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.Application
{
    public class ConsumeByInstrumentFromBusCommand : ICommand<ConsumeByInstrumentFromBusContext>
    {
        private readonly ILogger m_Logger;
        private readonly IBlocker m_Blocker;
        private readonly ISignalProviderCache m_SignalProviderCache;
        private readonly ICommandBuilder m_CommandBuilder;

        public ConsumeByInstrumentFromBusCommand(ILogger logger, IBlocker blocker, ISignalProviderCache signalProviderCache, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_Blocker = blocker;
            m_SignalProviderCache = signalProviderCache;
            m_CommandBuilder = commandBuilder;
        }


        public void Execute(ConsumeByInstrumentFromBusContext commandContext)
        {
            var providers = m_SignalProviderCache.All()
                .Where(p => p.Active && p.Instrument == commandContext.Instrument).ToList();

            if (!providers.Any())
            {
                m_Logger.DebugFormat("No active providers found {0};{1}", commandContext.Instrument, commandContext.TimeFrame.ToString());
                return;
            }
            
            var innerDatas = providers
                .Select(p => new InnerData {SignalProvider = p, TimeFrames = p.GetTimeFrame()}).ToList();

            var filteredProviders = innerDatas;

            if (!filteredProviders.Any())
            {
                m_Logger.DebugFormat("No active providers found {0};{1}", commandContext.Instrument, commandContext.TimeFrame.ToString());
                return;
            }
            
            foreach (var provider in filteredProviders.Select(p => p.SignalProvider).ToList())
            {
                consume(provider, commandContext.TimeFrame, commandContext.Rates);
            }
        }

        private void consume(ISignalProvider signalProvider, TimeFrameComplex timeFrame, List<Rate> newRates)
        {
            var provider = m_SignalProviderCache.Get(signalProvider.Id);

            var tfState = provider.GetTimeframeState();

            // Синхронизация установлена по индексу бара
            if (tfState.LastRateIndex + 1 == newRates.First().Index)
            {
                m_Logger.DebugFormat("Provider {0};{1};{2} is sync", signalProvider.Name, signalProvider.Instrument, tfState.TimeFrame);

                lock (m_Blocker.Get(signalProvider.Id))
                {
                    m_CommandBuilder.Execute(new ConsumeContext
                    {
                        SignalProviderId = signalProvider.Id,
                        Rates = newRates,
                        TimeFrameType = timeFrame.Type,
                        TimeFrameValue = timeFrame.Value
                    });
                }
            }
            else
            {
                m_Logger.DebugFormat("Provider {0};{1};{2} is OUT OF sync. Exec webApi", signalProvider.Name, signalProvider.Instrument, tfState.TimeFrame);
                m_CommandBuilder.Execute(new ConsumeByInstrumentFromApiContex {SignalProviderId = signalProvider.Id});
            }
        }

        private class InnerData
        {
            public ISignalProvider SignalProvider { get; set; }
            public TimeFrameComplex TimeFrames { get; set; }
        }
    }
}
