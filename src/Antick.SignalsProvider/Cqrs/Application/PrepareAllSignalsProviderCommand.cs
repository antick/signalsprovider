﻿using System.Collections.Generic;
using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Db;
using Antick.SignalsProvider.Components.Blocks;
using Antick.SignalsProvider.Components.MemoryCache;
using Antick.SignalsProvider.Components.SignalPublisher;
using Antick.SignalsProvider.Cqrs.Application.Context;
using Antick.SignalsProvider.Db;
using Antick.SignalsProvider.Models;
using Antick.SignalsProvider.Providers;
using Castle.Core.Logging;

namespace Antick.SignalsProvider.Cqrs.Application
{
    public class PrepareAllSignalsProviderCommand : ICommand<PrepareAllSignalsProviderContext>
    {
        private readonly ILogger m_Logger;
        private readonly ISessionBuilder<EntityContext> m_Session;
        private readonly IBlocker m_Blocker;
        private readonly ISignalProviderCache m_SignalProviderCache;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly ISignalProviderFactory m_SignalProviderFactory;
        private readonly ISignalPublisher m_SignalPublisher;

        public PrepareAllSignalsProviderCommand(ILogger logger, ISessionBuilder<EntityContext> session, IBlocker blocker, ISignalProviderCache signalProviderCache, ICommandBuilder commandBuilder, ISignalProviderFactory signalProviderFactory, ISignalPublisher signalPublisher)
        {
            m_Logger = logger;
            m_Session = session;
            m_Blocker = blocker;
            m_SignalProviderCache = signalProviderCache;
            m_CommandBuilder = commandBuilder;
            m_SignalProviderFactory = signalProviderFactory;
            m_SignalPublisher = signalPublisher;
        }

        public void Execute(PrepareAllSignalsProviderContext context)
        {
            var providerModels = m_Session.Execute(session =>
            {
                // Db запрос с материализацией
                var dbListeners = session.Query<SignalDto>().Where(p => p.Enabled).ToList();

                // Памминг
                List<SignalProviderModel> result = new List<SignalProviderModel>();
                result.AddRange(dbListeners.Select(x => x.ToModel()));
                return result;
            });

            m_Blocker.Init(providerModels.Select(p => p.Id));

            foreach (var model in providerModels)
            {
                var signalProvider = m_SignalProviderFactory.Create(model, m_SignalPublisher);
                m_SignalProviderCache.AddOrUpdate(signalProvider);
                
                // Синхронизация этого SignalProvider
                m_CommandBuilder.Execute(
                    new ConsumeByInstrumentFromApiContex
                    {
                        SignalProviderId = model.Id,
                        IsStarting = true
                    });
            }
        }
    }
}
