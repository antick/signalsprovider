﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;

namespace Antick.SignalsProvider.Cqrs.Application.Context
{
    public class ConsumeContext : ICommandContext
    {
        public int SignalProviderId { get; set; }

        public TimeFrameType TimeFrameType { get; set; }
        public string TimeFrameValue { get; set; }


        public List<Rate> Rates { get; set; }
    }
}
