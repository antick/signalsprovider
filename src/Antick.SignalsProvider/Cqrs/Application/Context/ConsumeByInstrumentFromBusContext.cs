﻿using System.Collections.Generic;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;

namespace Antick.SignalsProvider.Cqrs.Application.Context
{
    public class ConsumeByInstrumentFromBusContext : ICommandContext
    {
        public InstrumentComplex Instrument { get; set; }
        public TimeFrameComplex TimeFrame { get; set; }
        public List<Rate> Rates { get; set; }
    }
}
