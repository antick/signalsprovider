﻿using Antick.Cqrs.Commands;

namespace Antick.SignalsProvider.Cqrs.Application.Context
{
    public class ConsumeByInstrumentFromApiContex : ICommandContext
    {
        public int SignalProviderId { get; set; }

        public bool IsStarting { get; set; }
    }
}
