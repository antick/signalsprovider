﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.ApiHosting.WebApi;
using Antick.ApiHosting.WebApi.Configuration;
using Antick.Cqrs.Commands;
using Antick.SignalsProvider.Cqrs.Application.Context;
using Castle.Core.Logging;
using EasyNetQ.AutoSubscribe;
using Inceptum.AppServer;

namespace Antick.SignalsProvider
{
    public class HostedApplication : IHostedApplication
    {
        private readonly ILogger m_Logger;
        private readonly ApiHost m_ApiHost;
        private readonly NetworkConfig m_NetworkConfig;
        private readonly AutoSubscriber m_Subscriber;
        private readonly ICommandBuilder m_CommandBuilder;

        public HostedApplication(ILogger logger, ApiHost apiHost, NetworkConfig networkConfig,
            AutoSubscriber subscriber, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_ApiHost = apiHost;
            m_NetworkConfig = networkConfig;
            m_Subscriber = subscriber;
            m_CommandBuilder = commandBuilder;
        }

        public void Start()
        {
            m_Logger.DebugFormat("Starting application...");

            AppDomain.CurrentDomain.UnhandledException += onUnhandledException;
            TaskScheduler.UnobservedTaskException +=
                (sender, args) => m_Logger.Error("Unobserved task exception", args.Exception);

            m_Logger.DebugFormat("Starting web api hosts...");
            m_NetworkConfig.SetupNetwork();
            m_ApiHost.Start();
            m_Logger.InfoFormat("Web api hosts started");

            m_Logger.DebugFormat("Subscribe to Rabbit exchange...");
            m_Subscriber.Subscribe(GetType().Assembly);
            m_Subscriber.SubscribeAsync(GetType().Assembly);
            m_Logger.InfoFormat("Subscribe successfull");


            //m_Logger.WarnFormat("Debug mode. Delay 15 secs");
            //Thread.Sleep(15000);


            m_Logger.DebugFormat("Starting ConsumerStarter...");
            m_CommandBuilder.ExecuteAsync(new PrepareAllSignalsProviderContext());
            m_Logger.InfoFormat("ConsumerStarter started successfull");

            m_Logger.InfoFormat("Application started");
        }

        void onUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            if (ex != null)
            {
                m_Logger.ErrorFormat(ex, "Unhandled exception occured");
            }
            else
            {
                m_Logger.Error("Unhandled exception occured. No extra information can be provided.");
            }
        }
    }
}
