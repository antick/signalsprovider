﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.RateManager.Client;
using Antick.SignalsProvider.Indicators.Imp;
using Antick.SignalsProvider.Indicators.Types;
using NUnit.Framework;

namespace Antick.SignalsProvider.Indicator.Tests
{
    public class StochasticZoneIntegrationTests
    {
        private string rateManagetrClientUrl = "http://localhost:2002";

        [Test]
        public void Test()
        {
            var rates = getRates(Instrument.EUR_USD, TimeFrameType.Renko, "40", new DateTime(2016, 1, 1), DateTime.Now);


            var stochastic = StochasticIndicator.Calculate(
                new StochasticIndicatorContext
                {
                    Rates = rates,
                    Pk = 20,
                    Sk = 20,
                    Pd = 20
                });

            var stochasticZoneIndicator =
                new StochasticZoneIndicatorV2(new StochasticZoneContext { Candles = rates, StochasticData = stochastic });
            List<IndexValue<StochasticZoneType>> stockZoneTypes = new List<IndexValue<StochasticZoneType>>();
            for (int i = 0; i < rates.Count; i++)
            {
                var value = stochasticZoneIndicator.CalculateLogic(i);
                stockZoneTypes.Add(new IndexValue<StochasticZoneType> { Value = value, Index = rates[i].Index });
            }

            var hasAnyType = false;
            foreach (var zoneType in stockZoneTypes)
            {
                if (zoneType.Value != StochasticZoneType.None)
                    hasAnyType = true;
            }

            Assert.IsTrue(hasAnyType);
        }

        private List<Rate> getRates(Instrument instrument, TimeFrameType timeFrameType, string timeFrameValue, DateTime sTime, DateTime eTime)
        {
            var starttime = new DateTime(sTime.Year, sTime.Month, sTime.Day, 0, 0, 0, DateTimeKind.Local);
            var endTime = new DateTime(eTime.Year, eTime.Month, eTime.Day, 23, 59, 59, DateTimeKind.Local);

            var candles = new RateManagerClient(rateManagetrClientUrl)
                .GetRates(instrument.ToString(), timeFrameType, timeFrameValue, starttime, endTime).Result;

            return candles;
        }
    }
}
